import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OutboundService } from '../../../services/outbound/outbound.service';
import { NgxGalleryOptions, NgxGalleryImage } from 'ngx-gallery';
import { MatDialog } from '@angular/material/dialog';
import { LocationsComponent } from '../locations/locations.component';
import { FormComponent } from '../form/form.component';

import { CookieService } from 'ngx-cookie-service';
import { GlobalService } from '../../../services/global/global.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  providers: [OutboundService]
})
export class LandingComponent implements OnInit {
  public destinations: Array<Object> = [];
  public packages: Array<Object> = [];
  public locations: object[] = [];
  public days: Array<any> = [];

  // Customize itinerary
  public userLocations: string[] = [];
  private userSelections: object[] = [];
  public customLocations: object;

  public destinationFilter = 'All';
  public locationFilter = 'All';
  public daysFilter = 'All';
  public orderFilter = 'asc';

  public galleryOptions: NgxGalleryOptions[];
  public galleryImages: NgxGalleryImage[];

  private isLoggedIn: boolean;
  private type = 'outbound';
  public favorites: Array<object>;
  public userId: number;
  public favList = [];
  public favCount: number;

  constructor(
    private outboundservice: OutboundService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private cookieService: CookieService,
    private global: GlobalService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getDestinations();
    this.getPackages();
    this.getLocations();

    this.activeRoute.params.subscribe(param => {
      this.destinationFilter = param['destination'];
    });
    this.galleryOptions = [
      {
        width: '814px',
        height: '407px',
        imageAutoPlay: true,
        imageAutoPlayPauseOnHover: true,
        previewAutoPlay: true,
        previewAutoPlayPauseOnHover: true,
        thumbnailsMoveSize: 5,
        thumbnailsColumns: 5,
        thumbnailMargin: 10,
        previewFullscreen: true,
        previewKeyboardNavigation: true,
        previewCloseOnClick: true,
        previewCloseOnEsc: true
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];
    this.filterDestinations('All');

    /* Handle favorites */
    this.isLoggedIn = this.cookieService.check('user');
    this.fetchUser();
    this.fetchFavorites(this.userId); // Hopefully the id will exist before this method is called
    this.favCount = localStorage.length;

    /* Subscribe to user logged in status change event */
    this.global.loginStatusChange.subscribe(res => {
      this.fetchUser();
      this.fetchFavorites(this.userId);
    });
    this.updateCount(this.favCount);
  }

  private getDestinations() {
    return this.outboundservice.fetchDestinations().subscribe(res => {
      this.destinations = res.data;
    });
  }

  private getPackages() {
    return this.outboundservice.fetchPackages().then(res => {
      this.packages = res.data;
      this.favorites = this.transformPackages(res.data);
      this.getDays();
    });
  }

  private getLocations() {
    return this.outboundservice.fetchLocations().subscribe(res => {
      this.locations = res.data;
    });
  }

  public getLocationsImages(location: object) {
    return this.outboundservice.fetchLocationImages(location[0]).subscribe(res => {
      const galleryImages = res.data.map(item => {
        return {
          small: item[2],
          medium: item[2], /*.substr(0, item[3].length - 7) + '_md.png'*/
          big: item[2], /* .substr(0, item[3].length - 7) + '_bg.png' */
          description: ''
        };
      });

      this.openLocationsDialog(location, galleryImages);
    });
  }

  /* Open the dialog for the individual location */
  private openLocationsDialog(location: object, images: NgxGalleryImage[]) {
    const dialogRef = this.dialog.open(LocationsComponent, {
      width: '700px',
      height: '100%',
      data: {
        location: location,
        galleryOptions: this.galleryOptions,
        locationImages: images
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  public openFormDialog() {
    const dialogRef = this.dialog.open(FormComponent, {
      width: '500px',
      data: {
        selection: this.customLocations
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.openSnackBar('Your selection has been saved. We\'l be in touch soon.', '');
      }
    });
  }

  /* Customize locations */
  public toggleLocation(name: string, country: string) {
    if (this.userLocations.indexOf(name) === -1) {
      this.userLocations.push(name);
      this.userSelections.push({ location: name, country: country });
    } else {
      this.userLocations.splice(this.userLocations.indexOf(name), 1);
      this.userSelections = this.userSelections.filter((item) => {
        return item['location'] !== name;
      });
    }
    this.customLocations = this.groupLocations(this.userSelections, 'country');
  }

  private groupLocations(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = []; }
      result[item[prop]].push(item['location']);
      return result;
    }, {});
  }

  public isSelected(location: string): boolean {
    return this.userLocations.indexOf(location) > -1;
  }

  /* Get number of days in the packages */
  private getDays() {
    const unique = this.packages.map(pkg => pkg[6]).filter((value, index, self) => self.indexOf(value) === index);
    this.days = unique.sort((a, b) => a - b);
  }

  public filterDestinations(dest) {
    this.destinationFilter = dest;
  }

  public filterDays(days) {
    this.daysFilter = days;
  }

  public sortFilter(orderby) {
    this.orderFilter = orderby;
  }

  public filterLocations(loc) {
    this.locationFilter = loc;
  }

  public getPackageDetails(destination, packageId) {
    const url = `outbound/${destination}`;
    this.router.navigate([url, packageId]);
  }

  /* Favorites methods */
  private fetchUser() {
    if (this.isLoggedIn) {
      this.userId = JSON.parse(this.cookieService.get('user'))[0];
    }
  }

  private fetchFavorites(userId: number) {
    if (this.isLoggedIn) {
      return this.global.getFavorites(userId).subscribe(res => {
        if (res) {
          this.favList = this.transformFavorites(res.data);
        }
      });
    } else {
      this.fetchFavList();
    }
  }

  private fetchFavList() {
    if (localStorage.length > 0) {
      return Object.entries(localStorage).filter(arr => {
        if (arr[1] === this.type) {
          this.favList.push(arr[0].substring(4));
        }
      });
    }
  }

  /* Transform the array of objects { id, pacageType } - filter by type then extract just the id */
  private transformFavorites(arr) {
    return arr.filter(obj => obj[1] === this.type).reduce((result, item) => {
      result.push(item[0]);
      return result;
    }, []);
  }

  private transformPackages(arr: Array<object>) {
    return arr.map(item => {
      return {
        packageId: item[0],
        packageType: this.type,
        title: item[1],
        image: item[2],
        description: item[3],
        url: `seasonal/All/${item[0]}`,
        duration: `${item[6]}-days`,
        price: item[4]
      };
    });
  }

  /* Check if favorite is in favList */
  public isSaved(id) {
    if (this.favList.length >= 1) {
      return this.favList.indexOf(id) > -1;
    } else {
      return false;
    }
  }

  /* Update the favorites count */
  private updateCount(count) {
    this.global.favCountEvent.emit(count);
  }

  // toggleSave

  public toggleFavorite(id: number, pkg) {
    if (!this.isLoggedIn) {
      if (localStorage.getItem(`ofav${id}`) === null) {
        localStorage.setItem(`ofav${id}`, this.type);
        this.favList.push(id);
        this.openSnackBar('Added to favorites', '');
        this.updateCount(this.favCount += 1);
      } else {
        localStorage.removeItem(`ofav${id}`);
        this.favList = this.favList.filter(item => item !== id);
        this.updateCount(this.favCount -= 1);
        this.openSnackBar('Removed from favorites', 'Undo');
      }
    } else { // Check fav list, if true, delete but if doesn't exist delete it from db
      if (this.isSaved(id)) {
        this.global.removeFavorite(this.userId, pkg).subscribe(res => {
          this.favList = this.favList.filter(item => item !== id);
          this.updateCount(this.favCount -= 1);
          this.openSnackBar('Removed from favorites', 'Undo');
        });
      } else {
        this.global.postToFavorites(this.userId, pkg).subscribe(res => {
          this.favList.push(id);
          this.updateCount(this.favCount += 1);
          this.openSnackBar('Added to favorites', 'Undo');
        });
      }
    }
  }

  public openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
