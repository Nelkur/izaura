import { Component, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import * as _moment from 'moment';
const moment = _moment;

export interface DialogData {
  package: Object;
  pkg: Object;
  excursions?: object[];
  isPackage: Boolean;
  flights?: Object[];
}

@Component({
  selector: 'app-summary-dialog',
  templateUrl: './summary-dialog.component.html',
  styleUrls: ['./summary-dialog.component.scss']
})
export class SummaryDialogComponent {
  date = new FormControl(moment());
  public counter: Array<number> = [];
  public doubleRooms: Array<any> = [];
  private condensedPkg: Object;
  public departureType = 'group';
  public tickets = 0;
  public selectedExcursions: { [key: string]: number } = {};
  public selectedFlights: { [key: string]: number } = {};
  public selectedAccommodation: { [key: string]: any } = {};
  public userSelection: Object = {};
  private excursions = [];
  private excursionsTotal = 0;
  @ViewChild('form', { static: true }) form;

  constructor(public dialogRef: MatDialogRef<SummaryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private router: Router) {
    this.populateArray();

    this.condensedPkg = {
      id: this.data.package[0],
      title: this.data.package[1],
      byline: this.data.package[5],
      image: this.data.package[2],
      days: this.data.package[6],
      nights: parseFloat(this.data.package[7])
    };
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  private populateArray() {
    for (let i = 0; i <= 10; i++) {
      this.counter.push(i);
    }
    this.doubleRooms = this.counter.filter(room => {
      return room % 2 === 0;
    });
  }

  // Excursion handling
  public toggleExcursion(ex: object, isDropdown: boolean = false) {
    if (isDropdown) {
      this.selectedExcursions = this.data.excursions.filter(x => !!x['pax']).map(i => ({
        [`${i[2]}-${i['pax']}-${i[3]}`]: i[3] * (i['pax'] || 0)
      })).reduce((a, b) => ({ ...a, ...b }), {});
    }
    if (this.excursions.indexOf(ex[2]) === -1) {
      this.excursions.push(ex[2]);
      this.excursionsTotal += parseInt(ex[3], 10);
    } else {
      this.excursions.splice(this.excursions.indexOf(ex[2]), 1);
      this.excursionsTotal -= parseInt(ex[3], 10);
    }
  }

  get excursionTotal() {
    const ttl = this.data.excursions.map(ex => Number.parseFloat(ex[3]) * (ex['pax'] || 0)).reduce((total, price) => total + price, 0);
    this.excursionsTotal = ttl;
    return ttl;
  }

  get total() {
    return (this.data.pkg['flPrice'] || 0) * (this.data.pkg['pax'] || 0);
  }

  public onPaxChange() {
    // console.log(this.data.flights);
    if (this.data.flights) {
      this.selectedFlights = this.data.flights.filter(f => !!f['pax']).map(f => ({
        [`${f['flightName']}-${f['flightClass']}-${f['pax']}-${f['flightPrice']}`]: f['flightPrice'] * (f['pax'] || 0)
      })).reduce((a, b) => ({ ...a, ...b }), {});
      // console.log(this.selectedFlights);
      // console.log(this.checkPaxGuests);
    } else {
      this.selectedFlights = {
        // tslint:disable-next-line: max-line-length
        [`${this.data.pkg['flight']}-${this.data.pkg['flClass']}-${this.data.pkg['pax']}-${this.data.pkg['flPrice']}`]: this.data.pkg['flPrice'] * (this.data.pkg['pax'] || 0)
      };
      // console.log(this.selectedFlights);
      // console.log(this.checkPaxGuests);
    }
  }

  public onAcPaxChange() {
    /* If user selects bundle */
    if (!!this.data.pkg['paxSingle'] && !!this.data.pkg['paxDouble']) {
      if (this.data.isPackage) {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotel'],
          [`single room-${this.data.pkg['paxSingle']}-${this.data.pkg['singleRoom']}`]:
            this.data.pkg['singleRoom'] * (this.data.package[7] * (this.data.pkg['paxSingle'] || 0)),

          [`double room-${this.data.pkg['paxDouble']}-${this.data.pkg['doubleRoom']}`]:
            this.data.pkg['doubleRoom'] * (this.data.package[7] * (this.data.pkg['paxDouble'] || 0))
        };
        // console.log(this.selectedAccommodation);
        // console.log(this.checkPaxGuests);
      } else {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotelName'],
          [`single room-${this.data.pkg['paxSingle']}-${this.data.pkg['singleRoomPrice']}`]:
            this.data.pkg['singleRoomPrice'] * (this.data.package[7] * (this.data.pkg['paxSingle'] || 0)),

          [`double room-${this.data.pkg['paxDouble']}-${this.data.pkg['doubleRoomPrice']}`]:
            this.data.pkg['doubleRoomPrice'] * (this.data.package[7] * (this.data.pkg['paxDouble'] || 0))
        };
        // console.log(this.selectedAccommodation);
        // console.log(this.checkPaxGuests);
      }
    } else if (!!this.data.pkg['paxSingle']) {
      if (this.data.isPackage) {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotel'],
          [`single room-${this.data.pkg['paxSingle']}-${this.data.pkg['singleRoom']}`]:
            this.data.pkg['singleRoom'] * (this.data.package[7] * (this.data.pkg['paxSingle'] || 0))
        };
        // console.log(this.selectedAccommodation);
        // console.log(this.checkPaxGuests);
      } else {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotelName'],
          [`single room-${this.data.pkg['paxSingle']}-${this.data.pkg['singleRoomPrice']}`]:
            this.data.pkg['singleRoomPrice'] * (this.data.package[7] * (this.data.pkg['paxSingle'] || 0))
        };
        // console.log(this.selectedAccommodation);
        // console.log(this.checkPaxGuests);
      }
    } else if (!!this.data.pkg['paxDouble']) {
      if (this.data.isPackage) {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotel'],
          [`double room-${this.data.pkg['paxDouble']}-${this.data.pkg['doubleRoom']}`]:
            this.data.pkg['doubleRoom'] * (this.data.package[7] * (this.data.pkg['paxDouble'] || 0))
        };
        // console.log(this.selectedAccommodation);
        // console.log(this.checkPaxGuests);
      } else {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotelName'],
          [`double room-${this.data.pkg['paxDouble']}-${this.data.pkg['doubleRoomPrice']}`]:
            this.data.pkg['doubleRoomPrice'] * (this.data.package[7] * (this.data.pkg['paxDouble'] || 0))
        };
        // console.log(this.selectedAccommodation);
        // console.log(this.checkPaxGuests);
      }
    }
    return this.selectedAccommodation;
  }

  /* Row Totals */
  get singleRoomTotal() {
    if (this.data.isPackage) {
      return ((this.data.pkg['paxSingle'] || 0) * this.data.package[7]) * this.data.pkg['singleRoom'];
    } else {
      return ((this.data.pkg['paxSingle'] || 0) * this.data.package[7]) * this.data.pkg['singleRoomPrice'];
    }
  }

  get doubleRoomTotal() {
    if (this.data.isPackage) {
      return ((this.data.pkg['paxDouble'] || 0) * this.data.package[7]) * this.data.pkg['doubleRoom'];
    } else {
      return ((this.data.pkg['paxDouble'] || 0) * this.data.package[7]) * this.data.pkg['doubleRoomPrice'];
    }
  }

  get transportTotal() {
    if ((this.data.pkg['flPrice']) !== undefined) {
      return this.total;
    } else {
      // tslint:disable-next-line:max-line-length
      return this.data.flights.map(f => Number.parseFloat(f['flightPrice']) * (f['pax'] || 0)).reduce((total, flightPrice) => total + flightPrice, 0);
    }
  }

  get accommodationTotal() {
    return this.singleRoomTotal + this.doubleRoomTotal;
  }

  get fullTotal() {
    return this.transportTotal + this.accommodationTotal + this.excursionsTotal;
  }

  // Confirm that the number of flight pax is equal to the number of guests in accommodation. Return true or false
  get checkPaxGuests() {
    let sum: number;
    let total: number;
    if (this.data.flights) {
      this.data.flights.filter(f => !!f['pax']).map(f => {
        sum += (f['pax'] || 0);
      });
    } else {
      sum = (this.data.pkg['pax'] || 0);
    }

    if (!!this.data.pkg['paxSingle'] || !!this.data.pkg['paxDouble']) {
      total = (this.data.pkg['paxSingle'] || 0) + (this.data.pkg['paxDouble'] || 0);
    }

    return sum === total;
  }

  public checkout() {
    this.userSelection = {
      ['package']: this.condensedPkg,
      ['packageType']: 'outbound',
      ['packageUrl']: `https://izaurasafaris.com/${this.router.url}`,
      // tslint:disable-next-line: max-line-length
      ['departureDate']: (this.form.value.departureType === 'group' ? this.data.package[10] : this.convertDate(this.form.value.departureDate._i)),
      ['transport']: this.selectedFlights,
      ['accommodation']: this.onAcPaxChange(),
      ['excursions']: Object.keys(this.selectedExcursions).length > 0 ? this.selectedExcursions : [],
      ['totalCharge']: this.fullTotal
    };
    this.dialogRef.close(this.userSelection);
  }

  private convertDate(dateObj) {
    return `${dateObj.year}-${dateObj.month + 1}-${dateObj.date}`;
  }
}
