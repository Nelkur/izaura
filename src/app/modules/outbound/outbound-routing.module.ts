import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { DetailsComponent } from './details/details.component';
import { LocationsComponent } from './locations/locations.component';
import { CheckoutComponent } from './checkout/checkout.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent
  },
  {
    path: ':destination',
    children: [
      {
        path: '',
        component: LandingComponent
      },
      {
        path: ':packageId',
        component: DetailsComponent
      },
      {
        path: 'locations/:id',
        component: LocationsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OutboundRoutingModule { }
