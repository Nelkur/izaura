import {
  Component,
  Input,
  Inject,
  ViewChild,
  OnChanges,
  OnInit,
  SimpleChanges,
  ElementRef,
  ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { AuthService } from '../../../services/auth/auth.service';
import { Router } from '@angular/router';
import { PaymentService } from '../../../services/payments/payment.service';
import { FlexconnectService } from '../../../services/flexconnect/flexconnect.service';
import { CookieService } from 'ngx-cookie-service';
import { GlobalService } from '../../../services/global/global.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as intlTelInput from 'intl-tel-input';

import { User, Booking, Excursion, Transport, Accommodation } from '../../../models/booking.model';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
  providers: [AuthService]
})
export class CheckoutComponent implements OnInit, OnChanges {
  public checkout: FormGroup;
  public pointsControl: FormControl;
  public walletControl: FormControl;
  @Input() selection: object;
  @ViewChild('loginForm', { static: false }) loginForm: any;
  @ViewChild('registerForm', { static: false }) registerForm: any;
  @ViewChild('guestForm', { static: false }) guestForm: any;
  public hotel: string;
  public transport: Transport[];
  public accommodation: Accommodation[];
  public excursions: Excursion[];
  public hide = true;
  private adminCookie;

  public points = 0;
  public wallet = 0;
  public userId: number;
  private user: User;
  public registrationStatus = false;
  private booking: Booking;
  private guestUser: object;
  public deferPayment = false;

  public state = 'home';
  public notifications = 'yes';
  private hashedPass: string;

  /* Phone input */
  private input;
  private inputGlobal;
  private inputInstance;
  private selectedCountry: object;
  public countries: object[];
  public disablePayLater = true;

  constructor(
    @Inject(DOCUMENT) private document: any,
    private auth: AuthService,
    private flex: FlexconnectService,
    private router: Router,
    private payment: PaymentService,
    private cookieService: CookieService,
    private global: GlobalService,
    private changeDetector: ChangeDetectorRef,
    private el: ElementRef,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.userSetup();
    this.countries = window.intlTelInputGlobals.getCountryData();

    this.global.loginStatusChange.subscribe(res => {
      this.userSetup();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.selection = changes['selection'].currentValue;
    this.setup();
  }

  private createFormControls() {
    this.pointsControl = new FormControl('', [
      Validators.min(0),
      Validators.max(this.pointsToCurrency)
    ]);
    this.walletControl = new FormControl('', [
      Validators.min(0),
      Validators.max(this.wallet)
    ]);
  }

  private createForm() {
    this.checkout = new FormGroup({
      points: this.pointsControl,
      wallet: this.walletControl
    });
  }

  /* Check if selection has been passed and so stuff */
  public setup() {
    if (this.selection !== undefined) {
      this.transport = this.transportTransform(this.selection['transport']);
      this.excursions = Object.keys(this.selection['excursions']).length > 0 ? this.getExcursions(this.selection['excursions']) : [];
      this.accommodation = this.accommodationTransform(this.selection['accommodation']);
    }
  }

  /* Transform transport */
  public transportTransform(obj) {
    return Object.keys(obj).map(key => {
      const splitKey = key.split('-');
      return {
        carrier: splitKey[0],
        class: splitKey[1],
        pax: parseFloat(splitKey[2]),
        unitPrice: parseFloat(splitKey[3]),
        totalPrice: obj[key]
      };
    });
  }

  private getExcursions(obj) {
    return Object.keys(obj).map(key => {
      const splitKey = key.split('-');
      return {
        name: splitKey[0],
        pax: parseFloat(splitKey[1]),
        unitPrice: parseFloat(splitKey[2]),
        totalPrice: obj[key]
      };
    });
  }

  // Transform accommodation
  private accommodationTransform(obj) {
    this.hotel = obj['hotel'];
    delete obj['hotel'];
    return Object.keys(obj).map(key => {
      const splitKey = key.split('-'); // An array containing the split values
      return {
        hotel: this.hotel,
        room: splitKey[0],
        guests: parseFloat(splitKey[1]),
        nights: this.selection['package']['nights'],
        price_per_night: parseFloat(splitKey[2]),
        totalPrice: obj[key]
      };
    });
  }

  /* Construct object to save */
  private createBooking() {
    const pack = this.selection['package'];
    pack['link'] = this.selection['packageUrl'];
    const earnedPoints = Math.round(this.currencyToPoints(this.totalCharge));
    const redeemedPoints = this.checkout.value.points ? this.checkout.value.points : 0;
    const user = {
      id: this.isLoggedIn ? this.userId : -1,
      type: this.isLoggedIn ? 'registered' : 'guest',
      name: this.isLoggedIn ? this.user[2] : `${this.guestUser['firstName']} ${this.guestUser['lastName']}`,
      email: this.isLoggedIn ? this.user[1] : this.guestUser['email'],
      phone: this.isLoggedIn ? this.user[3] : `${this.guestUser['dialCode']}${this.guestUser['phone'].slice(1)}`,
      passport: this.isLoggedIn ? this.user[4] || '' : this.guestUser['passport'],
      passport_issue_country: this.isLoggedIn ? this.user[5] || '' : this.guestUser['issueCountry']
    };
    const packages = [];
    packages.push(pack);

    this.booking = {
      user: user,
      packageType: this.selection['packageType'],
      packages,
      excursions: this.excursions || [],
      transport: this.transport,
      accommodation: this.accommodation,
      payments: [],
      installments: [],
      comments: '',
      departure: this.selection['departureDate'],
      redeemedPoints: this.currencyToPoints(redeemedPoints),
      earnedPoints: earnedPoints,
      totalPoints: (Number(this.points) + Number(earnedPoints)) - Number(this.currencyToPoints(redeemedPoints)),
      totalPaid: 0,
      totalCharge: this.totalCharge,
      invoiceNumber: this.payment.generateOrderNumber(),
      status: 'pending',
    };
  }

  /* Calculate points */
  get pointsToCurrency() {
    // 1 point = 10 bob
    return this.points * 5;
  }

  public currencyToPoints(amount) {
    return amount / 100;
  }

  get totalCharge() {
    return this.selection['totalCharge'] - (this.checkout.value.points || 0) - (this.checkout.value.wallet || 0);
  }

  get percentageSavings() {
    return ((this.checkout.value.points + this.checkout.value.wallet) / this.selection['totalCharge']) * 100;
  }

  /* Summary */
  get isLoggedIn() {
    return this.auth.isLoggedIn;
  }

  get isGuestOrLoggedIn() {
    return this.isLoggedIn || this.guestUser !== undefined ? true : false;
  }

  /* User setup - call this on init and when the user logs in from the checkout component */
  public async userSetup() {
    if (this.isLoggedIn) {
      this.goTo('summary');
      this.userId = JSON.parse(this.cookieService.get('user'))[0];
      this.user = JSON.parse(this.cookieService.get('user'));

      /* Get user points */
      await this.auth.getTotalPoints(this.userId).then(res => {
        if (res.length > 0) {
          this.points = res[0].totalPoints;
        }
      });

      /* Get user wallet balance */
      await this.auth.getWalletTotal(this.userId).then(res => {
        if (res.length > 0) {
          this.wallet = res[0].wallet_total;
        }
      });

      this.createFormControls();
      this.createForm();
    } else {
      this.createFormControls();
      this.createForm();
      this.goTo('home');
    }
  }

  /* Our little router */
  public goTo(state) {
    this.state = state;

    if (state === 'register' || state === 'guest') {
      this.changeDetector.detectChanges();
      this.input = this.el.nativeElement.querySelector('#phone');

      // this.input = this.el.nativeElement.querySelector('#phone');
      if (this.input !== undefined) {
        intlTelInput(this.input, {
          utilsScript: 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.0.1/js/utils.js',
          initialCountry: 'ke',
          autoPlaceholder: 'polite'
        });

        this.inputGlobal = window.intlTelInputGlobals;
        this.inputInstance = this.inputGlobal.getInstance(this.input);
      }
    }
  }

  /* User checks out as guest and opts not to log in or register */
  public guestCheckout() {
    if (this.guestForm.valid) {
      const selectedCountry = this.inputInstance.getSelectedCountryData();
      this.guestUser = {
        firstName: this.guestForm.value.firstName.split(' ')[0].trim(),
        lastName: this.guestForm.value.lastName.split(' ')[0].trim(),
        email: this.guestForm.value.email,
        phone: this.guestForm.value.phone,
        intlPhone: this.inputInstance.getNumber(),
        dialCode: selectedCountry.dialCode,
        nationality: selectedCountry.name,
        iso2: selectedCountry.iso2,
        passport: this.guestForm.value.passport,
        issueCountry: this.guestForm.value.country
      };
      this.goTo('summary');
    }
  }

  /* Register users */
  public registerUser() {
    if (this.registerForm.valid) {
      const value = this.registerForm.value;
      value.intlPhone = this.inputInstance.getNumber();
      this.selectedCountry = this.inputInstance.getSelectedCountryData();
      this.userRegistration(value);
    }
  }

  /* Register user */
  public userRegistration(formData: object) { /* Extra var - this registrationStatus */
    return this.auth.register(formData).then(r => {
      if (r) {
        return this.auth.getUserDetails(formData['email']).then(res => {
          if (res.data) {
            return this.auth.setUserGroup(res.data[0][0]).subscribe(resp => {
              if (resp) {
                return this.auth.setUserNationality(res.data[0][0], this.selectedCountry, this.registerForm.value.phone).subscribe(f => {
                  if (f) {
                    this.registrationStatus = true;
                    this.openSnackBar('You were successfully registered', 'Done');
                    this.goTo('login');
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  /* Log a user into their account */
  public logInUser() {
    if (this.loginForm.valid && !this.isLoggedIn) {
      this.userLogin(this.loginForm.value.email, this.loginForm.value.password);
    }
  }

  /* User log in functionality */
  async userLogin(email: string, password: string) {
    await this.cookieHandler(email, password);

    return this.flex.firstConnection().subscribe(res => {
      if (res) {
        return this.auth.login(email, this.hashedPass).then(resp => {
          if (resp) {
            return this.getUserDetails(email);
          } else {
            this.auth.setAdminCookie(this.adminCookie);
          }
        });
      }
    });
  }

  /* Get user details */
  private getUserDetails(email: string) {
    return this.auth.getUserDetails(email).then(res => {
      if (res) {
        this.cookieService.set('user', JSON.stringify(res.data[0]), 0, '/');
        this.global.loginStatusChange.emit();
        this.openSnackBar('Successfully logged you in', 'Logout');
        this.userSetup();
      }
    });
  }

  /* Open snackbar */
  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'left'
    });
  }

  /* Grab the admin cookie, store it and then delete it */
  async cookieHandler(email: string, password: string) {
    if (this.cookieService.check('user')) {
      if (this.cookieService.get('user')['email'] === email) {
        this.router.navigate(['users/myaccount', 'home']);
      } else {
        this.cookieService.delete('PHPSESSID');
        this.cookieService.delete('user');
        return;
      }
    } else {
      return this.auth.hashPassword(password).then(res => {
        if (res.data) {
          this.hashedPass = res.data[0][0];
          this.adminCookie = this.cookieService.get('PHPSESSID');
          this.auth.storeAdmin(this.adminCookie);
          this.cookieService.delete('PHPSESSID', '/'); // The only coookie that exists at this point is PHPSESSID
        }
      });
    }
  }

  /* Save user configuration */
  public payLater() {
    this.createBooking();
    this.auth.addUserBooking(this.userId, this.booking).subscribe(res => {
      if (res['msg'] === 'Success') {
        this.deferPayment = true;
        this.openSnackBar('Config successfully saved', 'View');
      }
    });
  }

  public async goPay() {
    this.createBooking();
    this.payment.userSelection = this.booking;
    const { invoiceNumber } = this.booking;
    const { phone, email } = this.booking.user;

    this.payment.data = {
      live: '0',
      oid: invoiceNumber.toString(),
      inv: invoiceNumber.toString(),
      ttl: this.totalCharge.toString(),
      tel: phone,
      eml: email,
      vid: 'demo',
      curr: 'KES',
      p1: '',
      p2: '',
      p3: '',
      p4: '',
      cbk: 'https://izaurasafaris.com/payment/response', // `${this.document.location.hostname}/payment/response`,
      cst: '1',
      crl: '0',
      hsh: ''
    };

    this.router.navigate(['payment']);
  }

  public processCheckout() {
    if (this.checkout.valid) {
      this.goPay();
    }
  }
}
