import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { OutboundDetailsService } from '../../../services/outbound/outbound-details.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { MatDialog } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { HotelDialogComponent } from '../hotel-dialog/hotel-dialog.component';
import { SummaryDialogComponent } from '../summary-dialog/summary-dialog.component';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  providers: [OutboundDetailsService]
})
export class DetailsComponent implements OnInit {
  public url: String;
  public packageId;
  private packageDestination: String;
  public package: Object;
  public packageImages: Object[];
  public itinerary: Object[];
  public excursions: object[];
  public includes: object;
  public antPackages: Object[];
  public priceCards: Object[];
  public hotels: Object[];
  public hotelImages: NgxGalleryImage[];
  public flights: Object[];
  public standardPackage: object;

  public pageReady = false;
  public loadStatus = 'Getting ready';
  public galleryOptions: NgxGalleryOptions[];
  public galleryImages: NgxGalleryImage[];

  public details = false; /* View package details */
  public tacOptions = false; /* View accommodation packages */
  public checkout = false; /* Checkout */
  private myStepper: MatStepper;

  public shareLink = 'Click to copy link';
  public tagFilter = 'Standard';
  public hasTiers: boolean;
  public loadstatus = 'Getting ready';
  public keysMap = {
    0: 'pkgId',
    1: 'pkgDescription',
    2: 'fk_package',
    3: 'fk_tag',
    4: 'fk_flight',
    5: 'fk_hotel',
    6: 'pkgTransport',
    7: 'hotelId',
    8: 'hotelName',
    9: 'hotelOverview',
    10: 'featuredImage',
    11: 'fk_rating',
    12: 'fk_boardType',
    13: 'singleRoomPrice',
    14: 'doubleRoomPrice',
    15: 'website',
    16: 'fk_country',
    17: 'childPolicy',
    18: 'diningExp',
    19: 'activities',
    20: 'accommodation',
    21: 'flightId',
    22: 'flightName',
    23: 'flightDestination',
    24: 'flightClass',
    25: 'flightPrice'
  };

  public snackbarMessage: String;
  public userSelection: object;

  constructor(private detailService: OutboundDetailsService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.activeRoute.paramMap.subscribe((params: ParamMap) => {
      this.packageId = params.get('packageId');
      this.packageDestination = params.get('destination');
    });

    this.url = this.router.url;
    this.getDetails(this.packageId, this.packageDestination);
    this.galleryOptions = [
      {
        width: '814px',
        height: '407px',
        imageAutoPlay: true,
        imageAutoPlayPauseOnHover: true,
        previewAutoPlay: true,
        previewAutoPlayPauseOnHover: true,
        thumbnailsMoveSize: 5,
        thumbnailsColumns: 5,
        thumbnailMargin: 10,
        previewFullscreen: true,
        previewKeyboardNavigation: true,
        previewCloseOnClick: true,
        previewCloseOnEsc: true
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];
  }

  /* Grab the stepper */
  @ViewChild('stepper', { static: false }) set content(content: MatStepper) {
    this.myStepper = content;
  }

  /* User selects custom options */
  public customOptions(stepper) {
    this.details = true;
    this.nextStep(stepper);
  }

  /* If user is navigating back to this page from payments send them straight to checkout (step 3) */
  private backToCheckout(obj: object) {
    // console.log('-------------- BACK TO CHECKOUT --------------');
    // console.log(obj);
    // if (Object.entries(obj).length === 0 && obj.constructor === Object) {
    //   this.userSelection = {};
    //   this.userSelection = obj;
    //   this.myStepper.selectedIndex = 2;
    // }
  }

  private async getDetails(id: any, dest: String) {
    return await Promise.all([
      this.detailService.fetchPackage(id),
      this.detailService.fetchPackageImages(id),
      this.detailService.fetchItinerary(id),
      this.detailService.fetchAntPackages(id),
      this.detailService.fetchHotelImages(),
      this.detailService.fetchExcursions(id),
      this.detailService.fetchIncludes(id)
      // this.detailService.fetchHotels(dest),
      // this.detailService.fetchFlights(dest)
    ]).then(res => {
      this.package = res[0].data[0];
      this.packageImages = res[1].data;
      this.itinerary = res[2].data;
      this.antPackages = res[3].data.map(item => {
        return this.renameKeys(this.keysMap, item);
      });
      this.hotelImages = this.groupHotelImages(res[4].data, 1);
      this.excursions = res[5].data;
      this.includes = res[6].data[0];
      // this.hotels = res[5].data;
      // this.flights = res[6].data;

      this.galleryImages = this.packageImages.map(item => {
        return {
          small: item[1],
          medium: item[1], /*.substr(0, item[3].length - 7) + '_md.png'*/
          big: item[1], /* .substr(0, item[3].length - 7) + '_bg.png' */
          description: item[2]
        };
      });

      this.priceCards = this.groupBy(this.antPackages, 'fk_tag');
      this.standardPackage = this.priceCards['Standard'];

      this.hasTiers = !!+this.package[8];
      this.pageReady = true;
      this.backToCheckout(this.detailService.userSelection);
      this.ref.detectChanges();
    });
  }

  /* Rename multiple keys of an object */
  private renameKeys(keysMap, obj) {
    return Object.keys(obj).reduce((acc, key) => ({
      ...acc,
      ...{ [keysMap[key] || key]: obj[key] }
    }), {});
  }

  /* Method to group antPackages based on thier tier */
  private groupBy(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = {}; }
      result[item[prop]] = {
        hotel: item.hotelName,
        rating: item.fk_rating,
        boardType: item.fk_boardType,
        singleRoom: item.singleRoomPrice,
        doubleRoom: item.doubleRoomPrice,
        flight: item.flightName,
        flPrice: item.flightPrice,
        flClass: item.flightClass,
        flDestination: item.flightDestination,
        transport: !!+item.pkgTransport /* The !!+ evaluates the value of transport e.g 0 to false and positive integers to true */
      };
      return result;
    }, {});
  }

  private groupHotelImages(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = []; }
      result[item[prop]].push(
        {
          small: item[2],
          medium: item[2], /*.substr(0, item[3].length - 7) + '_md.png'*/
          big: item[2], /* .substr(0, item[3].length - 7) + '_bg.png' */
          description: item[3]
        }
      );
      return result;
    }, {});
  }

  /* Open the dialog for the hotel */
  public openHotelDialog(hotelId) {
    const hotel = this.antPackages.filter(item => {
      return item['hotelId'] === hotelId;
    });

    const dialogRef = this.dialog.open(HotelDialogComponent, {
      width: '700px',
      height: '100%',
      data: {
        hotel: hotel[0],
        galleryOptions: this.galleryOptions,
        hotelImages: this.hotelImages[hotelId]
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.snackbarMessage = result;
    });
  }

  /* Open the dialog when a user selects a package */
  public packageSummaryDialog(pkgCard: Object, isPackage: Boolean): void {
    const dialogRef = this.dialog.open(SummaryDialogComponent, {
      width: '750px',
      height: '100%',
      data: {
        package: this.package,
        pkg: pkgCard,
        excursions: this.excursions,
        isPackage: isPackage
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.snackbarMessage = result;
        this.userSelection = {};
        this.userSelection = result;
        this.saveSelectionToService(result);
        this.details = true;
        this.tacOptions = true;
        this.myStepper.selectedIndex = 2;
      }
    });
  }

  /* When you select a hotel */
  public hotelSummaryDialog(hotel, isPackage: Boolean) {

    const dialogRef = this.dialog.open(SummaryDialogComponent, {
      width: '750px',
      height: '100%',
      data: {
        package: this.package,
        pkg: hotel,
        excursions: this.excursions,
        isPackage: isPackage,
        flights: this.antPackages
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.snackbarMessage = result; /* Any message you'd like to show */
        this.userSelection = {}; /* Data collected in the popup modal */
        this.userSelection = result;
        this.saveSelectionToService(result);
        this.myStepper.selectedIndex = 2;
        this.details = true;
        this.tacOptions = true;
      }
    });
  }

  private saveSelectionToService(result: object) {
    this.detailService.userSelection = {};
    this.detailService.userSelection = result;
  }

  public scrollToProceedBtn(el: HTMLElement) {
    el.scrollIntoView({ behavior: 'smooth', block: 'center' });
  }

  /* Function that allows user to copy link in the share section */
  public copyLink(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    // this.shareLink = 'Link copied';
  }

  public scroll(el: HTMLElement) {
    el.scrollIntoView({ behavior: 'smooth' });
  }

  private nextStep(stepper: MatStepper) {
    stepper.next();
  }

  private previousStep(stepper: MatStepper) {
    stepper.previous();
  }
}

