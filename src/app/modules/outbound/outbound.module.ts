import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { PipesModule } from '../../pipes.module';
import { NgxGalleryModule } from 'ngx-gallery';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DirectivesModule } from '../../directives.module';

import { OutboundRoutingModule } from './outbound-routing.module';
import { LandingComponent } from './landing/landing.component';
import { DetailsComponent } from './details/details.component';
import { LocationsComponent } from './locations/locations.component';
import { HotelDialogComponent } from './hotel-dialog/hotel-dialog.component';
import { SummaryDialogComponent } from './summary-dialog/summary-dialog.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { FormComponent } from './form/form.component';

@NgModule({
  declarations: [
    LandingComponent,
    DetailsComponent,
    LocationsComponent,
    HotelDialogComponent,
    SummaryDialogComponent,
    CheckoutComponent,
    FormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    OutboundRoutingModule,
    MaterialModule,
    PipesModule,
    FlexLayoutModule,
    NgxGalleryModule,
    DirectivesModule
  ],
  entryComponents: [HotelDialogComponent, SummaryDialogComponent, LocationsComponent, FormComponent]
})
export class OutboundModule { }
