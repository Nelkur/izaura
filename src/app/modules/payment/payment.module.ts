import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { PaymentRoutingModule } from './payment-routing.module';
import { LandingComponent } from './landing/landing.component';
import { ResponseComponent } from './response/response.component';
import { StatusComponent } from './status/status.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';

@NgModule({
  declarations: [LandingComponent, ResponseComponent, StatusComponent, ConfirmationComponent],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    FormsModule,
    MaterialModule,
    FlexLayoutModule
  ],
  entryComponents: [ConfirmationComponent]
})
export class PaymentModule { }
