import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PaymentService } from '../../../services/payments/payment.service';
import { Booking, PaymentModel } from '../../../models/booking.model';
import { Transaction } from '../../../models/transaction.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-response',
  templateUrl: './response.component.html',
  styleUrls: ['./response.component.scss']
})
export class ResponseComponent implements OnInit {
  private booking: Booking;
  private transaction: Transaction;
  private model: PaymentModel;
  private vendorId = '';
  private orderId = this.payment.data.oid;
  private ipnurl: string;
  private status: object;
  private statusMap = [
    {
      code: 'fe2707etr5s4wq',
      status: 'Failed',
      message: 'Not all parameters fulfilled'
    },
    {
      code: 'aei7p7yrx4ae34',
      status: 'Success',
      message: 'The transaction is valid'
    },
    {
      code: 'bdi6p2yy76etrs',
      status: 'Pending: Incoming Mobile Money Transaction Not found',
      message: 'Please try again in 5 minutes'
    },
    {
      code: 'cr5i3pgy9867e1',
      status: 'Used',
      message: 'This code has been used already'
    },
    {
      code: 'dtfi4p7yty45wq',
      status: 'Short',
      message: 'The amount that you have sent via mobile money is LESS than what was required to validate this transaction'
    },
    {
      code: 'eq3i7p5yt7645e',
      status: 'Excess',
      message: 'The amount that you have sent via mobile money is MORE than what was required to validate this transaction'
    }
  ];

  private id: string;
  private ivm: string;
  private qwh: string;
  private afd: string;
  private poi: string;
  private uyt: string;
  private ifd: string;
  private mc: number;
  private txncd: string;
  private msisdn_id: string;
  private msisdn_idnum: string;
  private card_mask: string;
  private msisdn_custnum: string;
  private paymentOption: string;
  private installmentRef: string;
  private installmentId: string;
  private p4: string;


  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private payment: PaymentService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.redirectIfUrl();
    this.activeRoute.queryParamMap.subscribe(params => {
      this.id = params.get('id');
      this.ivm = params.get('ivm');
      this.qwh = params.get('qwh');
      this.afd = params.get('afd');
      this.poi = params.get('poi');
      this.uyt = params.get('uyt');
      this.ifd = params.get('ifd');
      this.mc = parseFloat(params.get('mc'));
      this.txncd = params.get('txncd');
      this.msisdn_id = params.get('msisdn_id');
      this.msisdn_idnum = params.get('msisdn_idnum');
      this.card_mask = params.get('card_mask');
      this.msisdn_custnum = params.get('msisdn_custnum');
      this.paymentOption = params.get('p1');
      this.installmentRef = params.get('p2');
      this.installmentId = params.get('p3');
    });
    if (this.paymentOption !== 'deposit' && this.paymentOption !== 'installment') {
      this.booking = JSON.parse(localStorage.getItem('selection'));
      this.transaction = JSON.parse(localStorage.getItem('model'));
    } else if (this.paymentOption === 'installment') {
      this.transaction = JSON.parse(localStorage.getItem('selection'));
      this.model = JSON.parse(localStorage.getItem('model'));
    } else {
      this.transaction = JSON.parse(localStorage.getItem('selection'));
    }
    this.getStatus();
  }

  /* Prevent navigation to this page from url */
  private redirectIfUrl() {
    if (!this.paymentOption) {
      this.router.navigate(['/']);
      this.openSnackBar('You are not allowed to do that!');
    }
  }

  public getStatus() {
    // tslint:disable-next-line: max-line-length
    this.ipnurl = `https://www.ipayafrica.com/ipn/?vendor=${this.vendorId}&id=${this.id}&ivm=${this.ivm}&qwh=${this.qwh}&afd=${this.afd}&poi=${this.poi}&uyt=${this.uyt}&ifd=${this.ifd}`;
    // console.log(`IPN Url is '${this.ipnurl}'`);

    this.payment.getPaymentStatus(this.ipnurl).subscribe(res => {
      if (res) {
        // console.log(`Transaction status is "${res}"`);
        const status = this.statusMap.filter(obj => obj['code'] === res);
        this.status = status[0];
        if (this.orderId === this.id) {
          // Set variable in payment service to object containing status, msisdn_id, msisndn_idnum, msisdn_custnum, mc, txncd, card_mask
          const response = {
            status: {
              status: this.status['status'],
              statusCode: this.status['code'],
              statusMessage: this.status['message']
            },
            oid: this.id,
            ivm: this.ivm,
            mc: this.mc,
            txncd: this.txncd,
            msisdn_id: this.msisdn_id,
            msisdn_idnum: this.msisdn_idnum,
            card_mask: this.card_mask,
            msisdn_custnum: this.msisdn_custnum
          };

          this.payment.ipayResponse = response;

          if (this.paymentOption === 'fullAmount' || this.paymentOption === 'installments' || this.paymentOption === 'savings') {
            this.updateBooking();
            localStorage.removeItem('selection');
            localStorage.removeItem('model');
            this.payment.addBooking(this.booking).subscribe(async resp => {
              if (resp) {
                const payload = await this.updateTransaction(resp._id);
                this.payment.addTransaction(payload).subscribe(r => {
                  if (r) {
                    if (this.booking.user.id !== -1) {
                      this.router.navigate(['users/myaccount/bookings']);
                      this.openSnackBar('Your booking was successful.');
                    } else {
                      this.router.navigate(['/']);
                      this.openSnackBar('Your booking was successful. We\'l be in touch.');
                    }
                  }
                });
              }
            });
          } else if (this.paymentOption === 'installment') {
            localStorage.removeItem('selection');
            localStorage.removeItem('model');
            this.transaction.payment.amount = this.mc;
            this.payment.addTransaction(this.transaction).subscribe(async resp => {
              if (resp) {
                const payment = await this.updateModel(resp._id);
                this.payment.payInstallment(payment, this.installmentRef, parseFloat(this.installmentId)).subscribe(r => {
                  if (r) {
                    this.router.navigate(['users/myaccount/bookings']);
                    this.openSnackBar('Installment paid successfully');
                    return;
                  }
                });
              }
            });
          } else if (this.paymentOption === 'deposit') {
            localStorage.removeItem('selection');
            this.updateDepositTransaction();
            this.payment.addTransaction(this.transaction).subscribe(resp => {
              if (resp) {
                this.router.navigate(['users/myaccount/bookings']);
                this.openSnackBar(`Successfully deposited KSh ${this.mc} to your wallet`);
                return;
              }
            });
          }
          // At this point redirect to notification page perhaps
          // console.log('Redirect to notification page.');
        } else {
          // If order ids don't match redirect them to notification page with appropriate error
          console.log('Order ids do not match');
        }
      }
    });
  }

  private updateBooking() {
    if (this.paymentOption !== 'savings') {
      this.booking.payments[0].transactionId = this.txncd;
      this.booking.totalPaid = this.mc;
      return this.booking;
    }
    return this.booking;
  }

  private updateTransaction(id: string) {
    if (this.paymentOption === 'savings') {
      this.transaction.payment.wallet_op = this.addToWallet(this.mc);
      this.transaction.payment.wallet_balance = this.transaction.payment.wallet_balance + this.addToWallet(this.mc);
    }
    this.transaction.payment.amount = this.mc;
    this.transaction.details.ref = id;
    return this.transaction;
  }

  private async updateModel(id: string) {
    this.model.payment.amount = this.mc;
    this.model.payment.transactionId = id;
    return this.model;
  }

  private async updateDepositTransaction() {
    this.transaction.payment.amount = this.mc;
    const { wallet_op, wallet_balance } = this.transaction.payment;
    if (wallet_op !== this.mc) {
      this.transaction.payment.wallet_op = parseFloat(`+${this.mc}`);
      this.transaction.payment.wallet_balance = (wallet_balance - wallet_op) + this.mc;
    }
    return this.transaction;
  }

  private addToWallet(value: number) {
    const entry = `+${value}`;
    return parseFloat(entry);
  }

  /* Open snackbar */
  private openSnackBar(message: string, action?: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
      horizontalPosition: 'left'
    });
  }

}
