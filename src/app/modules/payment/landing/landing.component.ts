import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { PaymentService } from '../../../services/payments/payment.service';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
declare var paypal;

import { IpayPayload } from '../../../models/ipay-web.model';
import { Transaction, WalletTotal } from '../../../models/transaction.model';
import { Booking, User, Payment } from '../../../models/booking.model';
import * as _moment from 'moment';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  @ViewChild('form', { static: true }) form: any;
  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;
  private data: IpayPayload;
  private dataStr;
  public paymentOption = 'fullAmount';
  public paymentMethod = 'stkPush';
  public fullAmount: number;
  public installmentsAllowed = [2, 3, 4, 5];
  public installmentLimits = { 2: 50000, 3: 100000, 4: 150000 };
  private timeDelta: number;
  public lipaPolePoleDownpayment: number;
  public lipaPolePoleBalance: number;
  public numberOfInstallments: number;
  public lipaPolePoleInstallments: object[] = []; // [{installment: 1, dueDate: date}]
  public installmentsCheck = true;
  public savingsDeposit: number;
  public savingsBalance: number;
  private mpesaLimit: number;
  public stkPushAmount: number;
  public newsletterSubscription: false;
  public termsAndConditions: true;
  private amountDue: number;
  public user: User;
  // Paypal test product
  private paypalPrice: number;
  private paypalDescription: string;
  private walletBalance: WalletTotal[];

  paidFor = false;
  public processing = false;
  public flash = false;
  public flashMessage = '';

  constructor(
    private payment: PaymentService,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.redirectIfUrl();
    this.mpesaLimit = 70000;
    this.user = this.payment.userSelection.user;
    this.data = Object.assign({}, this.payment.data);
    this.fullAmount = this.payment.userSelection.totalCharge;
    this.dataStr = this.createDataStr;
    this.getHash(this.dataStr);
    this.lipaPolePoleInit();
    this.calculateSavings();
    paypal
      .Buttons({
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                description: this.paypalDescription,
                amount: {
                  currency_code: 'USD',
                  value: this.paypalPrice
                }
              }
            ]
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
          this.paidFor = true;
          this.openSnackBar('Payment successful!');

          this.updatePaymentsCommentsTotalAndStatus(
            order.id, 'paypal',
            parseFloat(order.purchase_units[0].amount.value),
            order.purchase_units[0].amount.currency_code
          );
          this.bookingsPayloadCleanup();

          // console.log('----------- BOOKING PAYLOAD ------------');
          // console.log(this.payment.userSelection);

          this.payment.addBooking(this.payment.userSelection).subscribe(async res => {
            if (res) {
              const payload = await this.modelTransaction('paypal', parseFloat(order.purchase_units[0].amount.value), res['_id']);
              this.payment.addTransaction(payload).subscribe(resp => {
                if (res) {
                  if (this.payment.userSelection.user.id !== -1) {
                    this.router.navigate(['users/myaccount/bookings']);
                    this.openSnackBar('YOUR BOOKING WAS SUCCESSFUL!', '');
                  } else {
                    this.router.navigate(['/']);
                    this.openSnackBar('YOUR BOOKING WAS SUCCESSFUL! WE\'L BE IN TOUCH.');
                  }
                }
              });
            }
          });
        },
        onError: err => {
          // console.log(err);
          console.log('An error occurred!');
        }
      })
      .render(this.paypalElement.nativeElement);
  }

  /* Prevent navigation to this page from url */
  private redirectIfUrl() {
    if (!this.payment.data) {
      this.router.navigate(['/']);
      this.openSnackBar('You need to select a package first!');
    }
  }

  private recomputeIpayHash() {
    const dataStr = this.createDataStr;
    this.getHash(dataStr);
  }

  public getPaymentMethod() {
    // tslint:disable-next-line: max-line-length
    this.paymentMethod = this.fullAmount <= this.mpesaLimit || this.lipaPolePoleDownpayment <= this.mpesaLimit || this.savingsDeposit <= this.mpesaLimit ? 'stkPush' : 'paypal';
    this.updateCharges();
  }

  private updateCharges() {
    if (this.paymentOption === 'fullAmount') {
      this.amountDue = 1;
      this.paypalPrice = this.fullAmount;
      this.setIpayTotal(this.fullAmount);
      return;
    } else if (this.paymentOption === 'installments') {
      this.amountDue = 1;
      this.paypalPrice = this.lipaPolePoleDownpayment;
      this.setIpayTotal(this.lipaPolePoleDownpayment);
      return;
    } else {
      this.amountDue = 1;
      this.paypalPrice = this.savingsDeposit;
      this.setIpayTotal(this.savingsDeposit);
      return;
    }
  }

  private lipaPolePoleInit() {
    const totalCharge = this.fullAmount;
    // const departure = this.payment.userSelection['departure']; // Get the departure date
    const departure = '2020-4-25';
    this.timeDelta = this.getTimeDelta(departure);
    this.lipaPolePoleDownpayment = this.roundOff((totalCharge * 0.3), 10); // Calculate 30% of the total
    this.getPaymentMethod(); // Set the payment method to the appropriate one
    this.lipaPolePoleBalance = totalCharge - this.lipaPolePoleDownpayment;
    this.calculateInstallments(this.timeDelta);
  }

  /* Round off to the nearest whatever */
  private roundOff(num: number, base: number): number {
    if (num % base !== 0) {
      return Math.ceil(num / base) * base;
    }
    return num;
  }

  /* Break down the php date (YYYY-MM-DD HH:MM:SS) to [YYYY, MM, DD] */
  private breakDateDown(departure: string) {
    const ymd = departure.split(' ')[0].split('-').map(item => {
      return parseInt(item, 10);
    });
    return ymd;
  }

  /* Calculate the time delta between now and departure date */
  private getTimeDelta(date: string) {
    const dateArr = this.breakDateDown(date);
    const year = dateArr[0];
    const month = dateArr[1] - 1; // Months in JS are zero indexed
    const day = dateArr[2];
    const now = Date.now();
    const departureDate = new Date(year, month, day);
    this.payment.userSelection.departure = departureDate;

    const delta = departureDate.getTime() - now; // Returns delta in miliseconds
    return this.getDays(delta);
  }

  private getNumberOfInstallments(balance: number) {
    if (balance <= 50000) {
      return 2;
    } else if (balance <= 100000) {
      return 3;
    } else if (balance <= 150000) {
      return 4;
    } else {
      return 5;
    }
  }

  /* Calculate installments */
  private calculateInstallments(delta: number) {
    this.numberOfInstallments = this.numberOfInstallments || this.getNumberOfInstallments(this.lipaPolePoleBalance);
    const installmentAmount = Math.ceil(this.lipaPolePoleBalance / this.numberOfInstallments);
    if (delta <= 10) {
      return [];
    } else if (delta <= 28) { // 4 weeks pay by 3 days before departure
      return this._computeInstallments(delta, installmentAmount, 3);
    } else if (delta <= 56) { // 8 weeks, pay by 7 days before departure
      return this._computeInstallments(delta, installmentAmount, 7);
    } else if (delta <= 84) { // 12 weeks, pay by 10 days before departure
      return this._computeInstallments(delta, installmentAmount, 10);
    } else if (delta <= 112) { // 16 weeks, pay by 12 days before departure
      return this._computeInstallments(delta, installmentAmount, 12);
    } else if (delta <= 140) { // 20 weeks, pay by 14 days before departure
      return this._computeInstallments(delta, installmentAmount, 14);
    } else if (delta <= 168) { // 24 weeks, pay by 21 days before departure
      return this._computeInstallments(delta, installmentAmount, 21);
    } else {
      return this._computeInstallments(delta, installmentAmount, 28);
    }
  }

  private _computeInstallments(delta: number, installmentAmount: number, dueBy: number) {
    const today = Date.now();
    const newDelta = delta - dueBy;
    const timeDifference = newDelta / this.numberOfInstallments;
    for (let i = 1; i <= this.numberOfInstallments; i++) {
      const timestamp = today + (this.getDays(timeDifference, true) * i);
      this.lipaPolePoleInstallments.push({
        installment: _moment(`1994080${i}`).format('Do'),
        amount: installmentAmount,
        dueDate: _moment(timestamp).format('dddd, MMMM Do YYYY'),
        relative: _moment(timestamp).startOf('day').fromNow(),
        date: _moment(timestamp).format('YYYY-MM-DD')
      });

      this.payment.userSelection.installments.push({
        id: i,
        amount: installmentAmount,
        due_date: new Date(timestamp),
        due_date_readable: _moment(timestamp).format('dddd, MMMM Do YYYY'),
        status: 'pending'
      });
    }
    this.paypalDescription = this.getPaypalDescription;
  }

  get getPaypalDescription() {
    if (this.payment.userSelection.packages.length > 1) {
      return 'Multiple inbound package payment.';
    }
    return `${this.payment.userSelection.packages[0].title} Package`;
  }

  /* Convert delta to days and reverse - accepts either milliseconds or days */
  private getDays(d: number, reverse?: boolean) {
    if (reverse) {
      return d * 24 * 60 * 60 * 1000;
    }
    return d / 24 / 60 / 60 / 1000;
  }

  public selectInstallmentNumber(num: number) {
    const maxInstallments = this.getNumberOfInstallments(this.lipaPolePoleBalance);
    if (num > maxInstallments) {
      this.installmentsCheck = false;
    }
    this.numberOfInstallments = num;
    this.lipaPolePoleInstallments = [];
    this.payment.userSelection.installments = [];
    this.calculateInstallments(this.timeDelta);
    this.installmentsCheck = true;
  }

  /* Lipa pole pole only active for amounts within M-Pesa limit (70k) */
  public activateMpesa() {
    if (this.paymentOption === 'fullAmount') {
      if (this.fullAmount > this.mpesaLimit) {
        return true; // as in disable radio button
      }
      this.stkPushAmount = this.fullAmount;
      return false;
    } else if (this.paymentOption === 'installments') {
      if (this.lipaPolePoleDownpayment > this.mpesaLimit) {
        return true;
      }
      this.stkPushAmount = this.lipaPolePoleDownpayment;
      return false;
    } else if (this.paymentOption === 'savings') {
      if (this.savingsDeposit > this.mpesaLimit) {
        return true;
      }
      this.stkPushAmount = this.savingsDeposit;
      return false;
    } else {
      return true;
    }
  }

  private setIpayTotal(total: number) {
    this.payment.data.ttl = total.toString();
    this.data.ttl = total.toString();
    this.recomputeIpayHash();
  }

  /* Calculate savings deposit */
  private calculateSavings() {
    this.savingsDeposit = this.roundOff((this.fullAmount * 0.1), 10);
    this.savingsBalance = this.fullAmount - this.savingsDeposit;
  }

  /* Open snackbar */
  private openSnackBar(message: string, action?: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'left'
    });
  }

  /* Concatenate the data into a string for hash key generation */
  get createDataStr() {
    return Object.entries(this.payment.data).reduce((result, value) => {
      return result.concat(value[1]);
    }, '');
  }

  private async getHash(dataStr: string) {
    const res = await this.payment.getHash(dataStr);
    if (res) {
      // urlencode your callback url - https://<server-ip-address>/payment/response
      // urlencode the cbk as the last and final step before sending the data to ipay for processing
      // this.data.cbk = 'https://izaurasafaris.com/payment/response';
      this.data.hsh = res['hash'];
    }
  }

  private updatePaymentsCommentsTotalAndStatus(txnId: string, method: string, amount: number, currency?: string) {
    const payment: Payment = {
      transactionId: txnId,
      method,
      description: this.paymentDescription,
      amount,
      currency: currency ? currency : 'KES'
    };
    this.payment.userSelection.payments = [];
    this.payment.userSelection.payments.push(payment);
    this.payment.userSelection.comments = this.form.value.comments;
    this.payment.userSelection.totalPaid = this.paymentOption === 'savings' ? 0 : amount;
    this.payment.userSelection.status = this.bookingStatus;
  }

  get paymentDescription() {
    if (this.paymentOption === 'fullAmount') {
      return 'full amount';
    } else if (this.paymentOption === 'installments') {
      return 'deposit';
    }
    return '';
  }

  get bookingStatus() {
    if (this.paymentOption === 'fullAmount') {
      return 'paid';
    } else if (this.paymentOption === 'installments') {
      return 'ongoing';
    } else {
      return 'pending';
    }
  }

  get transactionDescription() {
    if (this.paymentOption === 'fullAmount') {
      return 'Made full payment for package';
    } else if (this.paymentOption === 'installments') {
      return 'Paid deposit for package';
    } else {
      return 'Added money to wallet';
    }
  }

  get paymentType() {
    if (this.paymentOption === 'fullAmount') {
      return 'Full amount';
    } else if (this.paymentOption === 'installments') {
      return 'Lipa pole pole';
    } else {
      return 'Wallet deposit';
    }
  }

  // Store result in wallet action field - for computing balance using mongodb aggregation framework
  private addToWallet(value: number) {
    const entry = `+${value}`;
    return parseFloat(entry);
  }

  private async modelTransaction(paymentMethod: string, amount: number, _id?: string): Promise<Transaction> {
    const { type, email, id, phone } = this.payment.userSelection.user;
    this.walletBalance = await this.payment.getWalletBalance(id);

    const transaction: Transaction = {
      user: { id, type, email, phone },
      payment: {
        type: this.paymentType,
        method: paymentMethod, // will be passed in by the method that calls it
        amount,
        currency_code: paymentMethod === 'paypal' ? 'USD' : 'KES',
        wallet_action: this.paymentOption === 'savings' ? 'package-deposit' : 'none',
        wallet_op: this.paymentOption === 'savings' ? this.addToWallet(amount) : 0,
        // tslint:disable-next-line: max-line-length
        wallet_balance: this.computeNewWalletTotal(amount)
      },
      details: {
        type: this.paymentOption === 'fullAmount' || this.paymentOption === 'installments' ? 'direct-payment' : 'wallet',
        description: this.transactionDescription,
        ref: _id ? _id : ''
      }
    };

    // return Promise.resolve(transaction);
    return transaction;
  }

  private computeNewWalletTotal(amount: number) {
    const walletOp = this.addToWallet(amount);
    if (this.paymentOption === 'savings') {
      if (this.walletBalance.length === 0) {
        return 0 + walletOp;
      }
      return this.walletBalance[0].wallet_total + walletOp;
    } else {
      if (this.walletBalance.length === 0) {
        return 0;
      }
      return this.walletBalance[0].wallet_total;
    }
  }

  private bookingsPayloadCleanup() {
    if (this.paymentOption !== 'installments') {
      this.payment.userSelection.installments = [];
    }
    if (this.paymentOption === 'savings') {
      this.payment.userSelection.payments = [];
    }
  }

  private setP1() {
    this.payment.data.p1 = this.paymentOption;
    this.data.p1 = this.paymentOption;
    this.recomputeIpayHash();
  }

  public async onSubmit() {
    if (this.form.valid) {
      this.processing = true;
      const { id, phone } = this.payment.userSelection.user;
      if (this.paymentMethod === 'stkPush') {
        this.payment.mpesaStkPush(this.amountDue, parseFloat(phone)).subscribe(async res => {
          if (res.ResponseCode === '0') {
            this.updatePaymentsCommentsTotalAndStatus('-1', 'stkPush', this.amountDue);
            this.bookingsPayloadCleanup();
            const booking = this.payment.userSelection;
            const transaction = await this.modelTransaction('stkPush', 0);
            // console.log('-- Transaction ---');
            // console.log(transaction);
            this.processing = false;
            this.confirmMpesaPayment(this.paymentOption, 'stkPush', booking, transaction);
          } else {
            this.processing = false;
            this.flash = true;
            this.flashMessage = 'Something went wrong. Please try again.';
          }
        });
      } else if (this.paymentMethod === 'paybill') {
        this.payment.mpesaC2BRegisterUrls().subscribe(async res => {
          if (res.ResponseDescription === 'success') {
            this.updatePaymentsCommentsTotalAndStatus('', 'paybill', this.amountDue);
            this.bookingsPayloadCleanup();
            const booking = this.payment.userSelection;
            const transaction = await this.modelTransaction('paybill', 0);
            this.processing = false;
            this.confirmMpesaPayment(this.paymentOption, 'paybill', booking, transaction);
          } else {
            this.processing = false;
            this.flash = true;
            this.flashMessage = 'Something went wrong. Please try again.';
          }
        });
      } else if (this.paymentMethod === 'ipay') {
        this.updatePaymentsCommentsTotalAndStatus('null', 'ipay', 0);
        this.bookingsPayloadCleanup();
        this.setP1();
        const transaction = await this.modelTransaction('ipay', 0);

        localStorage.setItem('selection', JSON.stringify(this.payment.userSelection));
        localStorage.setItem('model', JSON.stringify(transaction));

        this.processing = false;
        return this.payment.webPay(this.data);
      } else {
        this.processing = false;
        this.flash = true;
      }
    }
  }

  private confirmMpesaPayment(paymentOption: string, paymentMethod: string, booking: Booking, transaction: Transaction) {
    const dialogRef = this.dialog.open(ConfirmationComponent, {
      width: '650px',
      height: '100%',
      data: {
        paymentOption,
        paymentMethod,
        booking,
        transaction
      }
    });

    dialogRef.afterClosed().subscribe(res => {
    });
  }

  public closeFlash() {
    this.flash = false;
  }
}
