import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { PaymentService } from '../../../services/payments/payment.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Booking } from '../../../models/booking.model';
import { Transaction } from '../../../models/transaction.model';
import { Router } from '@angular/router';

interface DialogData {
  userId: number;
  paymentOption: string;
  paymentMethod: string;
  booking: Booking;
  transaction: Transaction;
}

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss'],
  providers: [PaymentService]
})
export class ConfirmationComponent implements OnInit {
  @ViewChild('form', { static: true }) form: any;
  public processing = false;
  public flash = false;

  constructor(public dialogRef: MatDialogRef<ConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private snackBar: MatSnackBar,
    private payment: PaymentService,
    private router: Router) { }

  ngOnInit() {
  }

  public closeFlash() {
    this.flash = false;
  }

  public confirmPayment() {
    if (this.form.valid) {
      this.processing = true;
      const method = this.data.paymentMethod === 'stkPush' ? 'confirmStk' : 'confirmPaybill';
      const transactionId = this.form.value.transactionId.toUpperCase();
      // Where if method === stkPush would be
      this.payment[method](transactionId).subscribe(res => {
        if (res.transactionId) {
          if (this.data.paymentOption !== 'savings') {
            this.data.booking.payments[0].transactionId = res.transactionId;
            this.data.booking.payments[0].amount = res.amount;
            this.data.transaction.payment.amount = res.amount;
            this.payment.addBooking(this.data.booking).subscribe(booking => {
              if (booking) {
                this.data.transaction.details.ref = booking['_id'];
                this.payment.addTransaction(this.data.transaction).subscribe(transaction => {
                  if (transaction) {
                    this.processing = false;
                    if (transaction.user.id !== -1) {
                      this.router.navigate(['users/myaccount/bookings']);
                      this.openSnackBar('YOUR BOOKING WAS SUCCESSFULLY MADE.', '');
                    } else {
                      this.openSnackBar('YOUR BOOKING WAS SUCCESSFULLY MADE.', '');
                    }
                  }
                });
              }
            });
          } else {
            this.data.transaction.payment.amount = res.amount;
            this.data.transaction.payment.wallet_op = this.getWalletOp(res.amount);
            this.data.transaction.payment.wallet_balance = this.data.transaction.payment.wallet_balance + this.getWalletOp(res.amount);
            // console.log('After confirmation');
            // console.log(this.data.transaction);
            this.payment.addTransaction(this.data.transaction).subscribe(transaction => {
              if (transaction) {
                this.processing = false;
                if (transaction.user.id !== -1) {
                  this.router.navigate(['users/myaccount/bookings']);
                  this.openSnackBar('YOUR BOOKING WAS SUCCESSFULLY MADE.', '');
                } else {
                  this.openSnackBar('YOUR BOOKING WAS SUCCESSFULLY MADE.', '');
                }
              }
            });
          }
        } else {
          this.flash = true;
          this.processing = false;
        }
      });
    }
  }

  private getWalletOp(value: number) {
    const entry = `+${value}`;
    return parseFloat(entry);
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
