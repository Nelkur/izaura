import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SeasonaldetailsService } from '../../../services/seasonal/seasonaldetails.service';
import { NgxGalleryOptions, NgxGalleryImage } from 'ngx-gallery';
import { forkJoin } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { HotelDialogComponent } from '../hotel-dialog/hotel-dialog.component';
import { SummaryDialogComponent } from '../summary-dialog/summary-dialog.component';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  providers: [SeasonaldetailsService]
})
export class DetailsComponent implements OnInit {
  private packageId: string | number; /* the id of the package as gotten from the url */
  public url: string; /* Current url - for the share button */
  public galleryOptions: NgxGalleryOptions[];

  public package: object;
  public packageImages: NgxGalleryImage[];
  public itinerary: object[];
  public excursions: object[];
  public antPackages: object[];
  public standardPackage: object; // Standard package
  public hotelImages: NgxGalleryImage[];
  public priceCards: object[];
  private groupDepartures: object[];
  public details = false; /* View package details */
  public tacOptions = false; /* View accommodation packages */
  private myStepper: MatStepper;
  private keysMap = {
    0: 'pkgId',
    1: 'fk_package',
    2: 'fk_tag',
    3: 'fk_hotel',
    4: 'fk_transport',
    5: 'description',
    6: 'hotelId',
    7: 'hotelName',
    8: 'hotelOverview',
    9: 'featuredImage',
    10: 'fk_rating',
    11: 'fk_boardType',
    12: 'fk_destination',
    13: 'singleRoomPrice',
    14: 'doubleRoomPrice',
    15: 'website',
    16: 'childPolicy',
    17: 'diningExp',
    18: 'activities',
    19: 'accommodation',
    20: 'transportId',
    21: 'fk_packageId',
    22: 'tMeans',
    23: 'tDescription',
    24: 'tPrice'
  };

  public pageReady = false;
  public loadStatus = 'Getting ready';
  public shareLink = 'Click to copy link';

  public userSelection: object;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private sservice: SeasonaldetailsService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    /* First thing we do is grab the id from the url */
    this.activeRoute.paramMap.subscribe((params: ParamMap) => {
      this.packageId = params.get('id');
    });

    this.url = this.router.url;
    this.galleryOptions = [
      {
        width: '814px',
        height: '407px',
        imageAutoPlay: true,
        imageAutoPlayPauseOnHover: true,
        previewAutoPlay: true,
        previewAutoPlayPauseOnHover: true,
        thumbnailsMoveSize: 5,
        thumbnailsColumns: 5,
        thumbnailMargin: 10,
        previewFullscreen: true,
        previewKeyboardNavigation: true,
        previewCloseOnClick: true,
        previewCloseOnEsc: true
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];
    this.getDetails(this.packageId);
  }

  /* Grab the stepper */
  @ViewChild('stepper', { static: false }) set content(content: MatStepper) {
    this.myStepper = content;
  }

  private getDetails(id: string | number) {
    return forkJoin(
      this.sservice.fetchPackage(id),
      this.sservice.fetchPackageImages(id),
      this.sservice.fetchItinerary(id),
      this.sservice.fetchAntPackages(id),
      this.sservice.fetchHotelImages(),
      this.sservice.fetchExcursions(id)
    ).subscribe(res => {
      const bundled = !!res[0].data[0][12]; // True or false value of whether or not this packge has bundles
      this.package = res[0].data[0]; // The actual package
      this.packageImages = res[1].data.map(item => { // The package images transformed to NgxGalleryImage[]
        return {
          small: item[2],
          medium: item[2], /*.substr(0, item[3].length - 7) + '_md.png'*/
          big: item[2], /* .substr(0, item[3].length - 7) + '_bg.png' */
          description: item[3]
        };
      });
      this.itinerary = res[2].data; // Package itinerary
      this.excursions = res[5].data;
      this.antPackages = res[3].data.map(item => { // Accommodation and transport packages join tables renamed
        return this.renameKeys(this.keysMap, item);
      });
      this.hotelImages = this.groupHotelImages(res[4].data, 1); // Hotel images grouped by hotel id

      this.priceCards = this.groupBy(this.antPackages, 'fk_tag');
      this.standardPackage = this.priceCards['Standard'];

      this.pageReady = true;
      this.getGroupDepartures();
    },
      error => console.log('Error :', error));
  }

  /* If there are multiple group departures, fetch em */
  private getGroupDepartures() {
    if (!!this.package[14]) {
      return this.sservice.fetchGroupDepartures(this.package[0]).subscribe(res => {
        if (res) {
          this.groupDepartures = res.data;
        }
      });
    } else {
      this.groupDepartures = [];
    }
  }

  /* Rename multiple keys of an object */
  private renameKeys(keysMap, obj) {
    return Object.keys(obj).reduce((acc, key) => ({
      ...acc,
      ...{ [keysMap[key] || key]: obj[key] }
    }), {});
  }

  /* Group hotel images */
  private groupHotelImages(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = []; }
      result[item[prop]].push(
        {
          small: item[2],
          medium: item[2], /*.substr(0, item[3].length - 7) + '_md.png'*/
          big: item[2], /* .substr(0, item[3].length - 7) + '_bg.png' */
          description: item[3]
        }
      );
      return result;
    }, {});
  }

  /* Method to group antPackages based on thier tier */
  private groupBy(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = {}; }
      result[item[prop]] = {
        hotelId: item.hotelId,
        hotel: item.hotelName,
        rating: item.fk_rating,
        boardType: item.fk_boardType,
        singleRoom: item.singleRoomPrice,
        doubleRoom: item.doubleRoomPrice,
        transport: item.tMeans,
        tPrice: item.tPrice,
        tDescription: item.tDescription
      };
      return result;
    }, {});
  }

  /* Hotel dialog */
  public openHotelDialog(hotelId) {
    const hotel = this.antPackages.filter(item => {
      return item['hotelId'] === hotelId;
    });

    const dialogRef = this.dialog.open(HotelDialogComponent, {
      width: '700px',
      height: '100%',
      data: {
        hotel: hotel[0],
        galleryOptions: this.galleryOptions,
        hotelImages: this.hotelImages[hotelId]
      }
    });

    dialogRef.afterClosed().subscribe(res => {
    });
  }

  /* Summary dialogs */
  public hotelSummaryDialog(hotel, isPackage: Boolean) {
    const dialogRef = this.dialog.open(SummaryDialogComponent, {
      width: '750px',
      height: '100%',
      data: {
        package: this.package,
        pkg: hotel,
        excursions: this.excursions,
        isPackage: isPackage,
        transport: this.antPackages,
        groupDepartures: this.groupDepartures
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userSelection = {};
        this.userSelection = result;
        this.details = true;
        this.tacOptions = true;
        this.myStepper.selectedIndex = 2;
      }
    });
  }

  public packageSummaryDialog(pkgCard: object, isPackage: boolean) {
    const dialogRef = this.dialog.open(SummaryDialogComponent, {
      width: '750px',
      height: '100%',
      data: {
        package: this.package,
        excursions: this.excursions,
        pkg: pkgCard,
        isPackage: isPackage,
        groupDepartures: this.groupDepartures
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userSelection = {};
        this.userSelection = result;
        this.details = true;
        this.tacOptions = true;
        this.myStepper.selectedIndex = 2;
      }
    });
  }

  /* Share link */
  public copyLink(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    // this.shareLink = 'Link copied';
  }

  /* User selects custom options */
  public customOptions(stepper) {
    this.details = true;
    this.nextStep(stepper);
  }

  public scroll(el: HTMLElement) {
    el.scrollIntoView({ behavior: 'smooth' });
  }

  private nextStep(stepper: MatStepper) {
    stepper.next();
  }

  private previousStep(stepper: MatStepper) {
    stepper.previous();
  }

}
