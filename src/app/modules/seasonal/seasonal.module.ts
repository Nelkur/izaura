import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../../material.module';
import { PipesModule } from '../../pipes.module';
import { DirectivesModule } from '../../directives.module';
import { NgxGalleryModule } from 'ngx-gallery';

import { SeasonalRoutingModule } from './seasonal-routing.module';

import { LandingComponent } from './landing/landing.component';
import { DetailsComponent } from './details/details.component';
import { SummaryDialogComponent } from './summary-dialog/summary-dialog.component';
import { HotelDialogComponent } from './hotel-dialog/hotel-dialog.component';
import { CheckoutComponent } from './checkout/checkout.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SeasonalRoutingModule,
    MaterialModule,
    PipesModule,
    FlexLayoutModule,
    DirectivesModule,
    NgxGalleryModule
  ],
  declarations: [LandingComponent, DetailsComponent, SummaryDialogComponent, HotelDialogComponent, CheckoutComponent],
  entryComponents: [HotelDialogComponent, SummaryDialogComponent]
})
export class SeasonalModule { }
