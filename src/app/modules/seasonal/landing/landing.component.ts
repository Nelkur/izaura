import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SeasonalService } from '../../../services/seasonal/seasonal.service';
import { CookieService } from 'ngx-cookie-service';
import { GlobalService } from '../../../services/global/global.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  providers: [SeasonalService]
})
export class LandingComponent implements OnInit {
  public sHolidays: object[];
  public sDestinations: object[];
  public sPackages: object[];
  public filterValue = 'All';
  public seasonFilter = 'All';
  public opened = true;

  private isLoggedIn: boolean;
  private type = 'seasonal';
  public favorites: Array<object>;
  public userId: number;
  public favList = [];
  public favCount: number;

  constructor(
    private seasonalservice: SeasonalService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private cookieService: CookieService,
    private global: GlobalService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(param => {
      this.seasonFilter = param['name'];
    });

    this.getHolidays();
    this.getDestinations();
    this.getPackages();

    /* Favorites */
    this.favCount = localStorage.getItem('__paypal_storage__') === null ? localStorage.length : localStorage.length - 1;
    this.isLoggedIn = this.cookieService.check('user');
    this.fetchUser();
    this.fetchFavorites(this.userId); // Hopefully the id will exist before this method is called

    /* Subscribe to user logged in status change event */
    this.global.loginStatusChange.subscribe(res => {
      this.fetchUser();
      this.fetchFavorites(this.userId);
    });
    this.updateCount(this.favCount);
  }

  /* Get seasonal_holidays */
  private getHolidays() {
    return this.seasonalservice.fetchHolidays().subscribe(res => {
      this.sHolidays = res.data;
    });
  }

  /* Get seasonal_destinations */
  private getDestinations() {
    return this.seasonalservice.fetchDestinations().subscribe(res => {
      this.sDestinations = res.data;
    });
  }

  /* Get seasonal_packages*/
  private getPackages() {
    return this.seasonalservice.fetchPackages().subscribe(res => {
      this.sPackages = res.data;
      this.favorites = this.transformPackages(res.data);
    });
  }

  public filterBy(value) {
    this.filterValue = value;
  }

  /* Navigate to season deals */
  public goToSeason(name) {
    this.router.navigate(['seasonal', name]);
    this.filterValue = 'All';
  }

  public getPackageDetails(packageId) {
    const url = `seasonal/${this.seasonFilter}`;
    this.router.navigate([url, packageId]);
  }

  /* Favorites */
  private fetchUser() {
    if (this.isLoggedIn) {
      this.userId = JSON.parse(this.cookieService.get('user'))[0];
    }
  }

  private fetchFavorites(userId: number) {
    if (this.isLoggedIn) {
      return this.global.getFavorites(userId).subscribe(res => {
        if (res) {
          this.favList = this.transformFavorites(res.data);
          console.log(this.favList);
        }
      });
    } else {
      this.fetchFavList();
    }
  }

  private fetchFavList() {
    if (this.favCount > 0) {
      return Object.entries(localStorage).filter(arr => {
        if (arr[1] === this.type) {
          this.favList.push(arr[0].substring(4));
        }
      });
    }
  }

  /* Transform the array of objects { id, pacageType } - filter by type then extract just the id */
  private transformFavorites(arr) {
    return arr.filter(obj => obj[1] === this.type).reduce((result, item) => {
      result.push(item[0]);
      return result;
    }, []);
  }

  private transformPackages(arr: Array<object>) {
    return arr.map(item => {
      return {
        packageId: item[0],
        packageType: this.type,
        title: item[4],
        image: item[3],
        description: item[6],
        url: `seasonal/All/${item[0]}`,
        duration: `${item[8]}-days`,
        price: item[7]
      };
    });
  }

  /* Check if favorite is in favList */
  public isSaved(id) {
    if (this.favList.length >= 1) {
      return this.favList.indexOf(id) > -1;
    } else {
      return false;
    }
  }

  /* Update the favorites count */
  private updateCount(count) {
    this.global.favCountEvent.emit(count);
  }

  // toggleSave

  public toggleFavorite(id: number, pkg) {
    if (!this.isLoggedIn) {
      if (localStorage.getItem(`sfav${id}`) === null) {
        localStorage.setItem(`sfav${id}`, this.type);
        this.favList.push(id);
        this.openSnackBar('Added to favorites', 'Undo');
        this.updateCount(this.favCount += 1);
        console.log(this.favList);
      } else {
        localStorage.removeItem(`sfav${id}`);
        this.favList = this.favList.filter(item => item !== id);
        this.updateCount(this.favCount -= 1);
        this.openSnackBar('Removed from favorites', 'Undo');
      }
    } else { // Check fav list, if true, delete but if doesn't exist delete it from db
      if (this.isSaved(id)) {
        this.global.removeFavorite(this.userId, pkg).subscribe(res => {
          this.favList = this.favList.filter(item => item !== id);
          this.updateCount(this.favCount -= 1);
          this.openSnackBar('Removed from favorites', 'Undo');
        });
      } else {
        this.global.postToFavorites(this.userId, pkg).subscribe(res => {
          this.favList.push(id);
          this.updateCount(this.favCount += 1);
          this.openSnackBar('Added to favorites', 'Undo');
        });
      }
    }
  }

  public openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
