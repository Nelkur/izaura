import { Component, Inject, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  package: object;
  pkg: object;
  excursions?: object[];
  isPackage: boolean;
  transport?: object[];
  groupDepartures: object[];
}

@Component({
  selector: 'app-summary-dialog',
  templateUrl: './summary-dialog.component.html',
  styleUrls: ['./summary-dialog.component.scss']
})

export class SummaryDialogComponent {
  private condensedPkg: object;
  public counter: Array<any> = [];
  public doubleRooms: Array<any> = [];
  public departureType = 'group';
  public departureDate = '';
  public group = !!this.data.package[13]; /* If this exists, then it's only one */
  public tickets = 0;
  public selectedExcursions: { [key: string]: number } = {};
  private excursions = [];
  private excursionsTotal = 0;

  @ViewChild('form', { static: true }) private form;
  public selectedTransport: { [key: string]: number } = {};
  public selectedAccommodation: { [key: string]: any } = {};
  public userSelection: object = {};

  constructor(public dialogRef: MatDialogRef<SummaryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private router: Router) {
    this.populateArray();

    this.condensedPkg = {
      id: this.data.package[0],
      title: this.data.package[4],
      byline: this.data.package[1],
      image: this.data.package[3],
      days: this.data.package[8],
      nights: parseFloat(this.data.package[9])
    };

    console.log('--- Package ---');
    console.log(this.data.pkg);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  private populateArray() {
    for (let i = 1; i <= 10; i++) {
      this.counter.push(i);
    }
    this.doubleRooms = this.counter.filter(room => {
      return room % 2 === 0;
    });
  }

  // Excursion handling
  public toggleExcursion(ex: object, isDropdown: boolean = false) {
    if (isDropdown) {
      this.selectedExcursions = this.data.excursions.filter(x => !!x['pax']).map(i => ({
        [`${i[2]}-${i['pax']}-${i[3]}`]: i[3] * (i['pax'] || 0)
      })).reduce((a, b) => ({ ...a, ...b }), {});
    }
    if (this.excursions.indexOf(ex[2]) === -1) {
      this.excursions.push(ex[2]);
      this.excursionsTotal += parseInt(ex[3], 10);
    } else {
      this.excursions.splice(this.excursions.indexOf(ex[2]), 1);
      this.excursionsTotal -= parseInt(ex[3], 10);
    }
  }

  get excursionTotal() {
    const ttl = this.data.excursions.map(ex => Number.parseFloat(ex[3]) * (ex['pax'] || 0)).reduce((total, price) => total + price, 0);
    this.excursionsTotal = ttl;
    return ttl;
  }

  public onPaxChange() {
    // console.log(this.data.transport);
    if (this.data.transport) {
      this.selectedTransport = this.data.transport.filter(t => !!t['pax']).map(t => ({
        [`${t[2]}-${t['pax']}-${t[4]}`]: t[4] * (t['pax'] || 0)
      })).reduce((a, b) => ({ ...a, ...b }), {});
      console.log(this.selectedTransport);
    } else {
      this.selectedTransport = {
        // tslint:disable-next-line: max-line-length
        [`${this.data.pkg['transport']}-${this.data.pkg['pax']}-${this.data.pkg['tPrice']}`]: this.data.pkg['tPrice'] * (this.data.pkg['pax'] || 0)
      };
      console.log(this.selectedTransport);
    }
  }

  /* Total transport if user selects bundle */
  get tTotal() {
    return (this.data.pkg['tPrice'] || 0) * (this.data.pkg['pax'] || 0);
  }

  /* Total transport */
  get transportTotal() {
    if (this.data.isPackage) {
      return this.tTotal;
    } else {
      // tslint:disable-next-line:max-line-length
      return this.data.transport.map(t => Number.parseFloat(t[4]) * (t['pax'] || 0)).reduce((total, tPrice) => total + tPrice, 0);
    }
  }

  /*Accommodation calculations */
  /* Row Totals */
  get singleRoomTotal() {
    if (this.data.isPackage) {
      return ((this.data.pkg['paxSingle'] || 0) * this.data.package[9]) * this.data.pkg['singleRoom'];
    } else {
      return ((this.data.pkg['paxSingle'] || 0) * this.data.package[9]) * this.data.pkg['singleRoomPrice'];
    }
  }

  get doubleRoomTotal() {
    if (this.data.isPackage) {
      return ((this.data.pkg['paxDouble'] || 0) * this.data.package[9]) * this.data.pkg['doubleRoom'];
    } else {
      return ((this.data.pkg['paxDouble'] || 0) * this.data.package[9]) * this.data.pkg['doubleRoomPrice'];
    }
  }

  public onAcPaxChange() {
    if (!!this.data.pkg['paxSingle'] && !!this.data.pkg['paxDouble']) {
      if (this.data.isPackage) {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotel'],
          [`single room-${this.data.pkg['paxSingle']}-${this.data.pkg['singleRoom']}`]:
            this.data.pkg['singleRoom'] * (this.data.package[9] * (this.data.pkg['paxSingle'] || 0)),

          [`double room-${this.data.pkg['paxDouble']}-${this.data.pkg['doubleRoom']}`]:
            this.data.pkg['doubleRoom'] * (this.data.package[9] * (this.data.pkg['paxDouble'] || 0))
        };
        console.log(this.selectedAccommodation);
      } else {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotelName'],
          [`single room-${this.data.pkg['paxSingle']}-${this.data.pkg['singleRoomPrice']}`]:
            this.data.pkg['singleRoomPrice'] * (this.data.package[9] * (this.data.pkg['paxSingle'] || 0)),

          [`double room-${this.data.pkg['paxDouble']}-${this.data.pkg['doubleRoomPrice']}`]:
            this.data.pkg['doubleRoomPrice'] * (this.data.package[9] * (this.data.pkg['paxDouble'] || 0))
        };
        console.log(this.selectedAccommodation);
      }
    } else if (!!this.data.pkg['paxSingle']) {
      if (this.data.isPackage) {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotel'],
          [`single room-${this.data.pkg['paxSingle']}-${this.data.pkg['singleRoom']}`]:
            this.data.pkg['singleRoom'] * (this.data.package[9] * (this.data.pkg['paxSingle'] || 0))
        };
        console.log(this.selectedAccommodation);
      } else {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotelName'],
          [`single room-${this.data.pkg['paxSingle']}-${this.data.pkg['singleRoomPrice']}`]:
            this.data.pkg['singleRoomPrice'] * (this.data.package[9] * (this.data.pkg['paxSingle'] || 0))
        };
        console.log(this.selectedAccommodation);
      }
    } else if (!!this.data.pkg['paxDouble']) {
      if (this.data.isPackage) {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotel'],
          [`double room-${this.data.pkg['paxDouble']}-${this.data.pkg['doubleRoom']}`]:
            this.data.pkg['doubleRoom'] * (this.data.package[9] * (this.data.pkg['paxDouble'] || 0))
        };
        console.log(this.selectedAccommodation);
      } else {
        this.selectedAccommodation = {
          hotel: this.data.pkg['hotelName'],
          [`double room-${this.data.pkg['paxDouble']}-${this.data.pkg['doubleRoomPrice']}`]:
            this.data.pkg['doubleRoomPrice'] * (this.data.package[9] * (this.data.pkg['paxDouble'] || 0))
        };
        console.log(this.selectedAccommodation);
      }
    }
  }

  get accommodationTotal() {
    return this.singleRoomTotal + this.doubleRoomTotal;
  }

  /* Total for transport and accommodation */
  get fullTotal() {
    return this.transportTotal + this.accommodationTotal + this.excursionsTotal;
  }

  /* Checkout section */
  public checkout() {
    if (this.form.valid) {
      this.userSelection = {
        ['package']: this.condensedPkg,
        ['packageType']: 'seasonal',
        ['packageUrl']: `https://izaurasafaris.com${this.router.url}`,
        ['departureType']: this.form.value.departureType,
        ['departureDate']: this.form.value.departureDate, // Get this from the form
        ['transport']: this.selectedTransport,
        ['accommodation']: this.selectedAccommodation,
        ['excursions']: Object.keys(this.selectedExcursions).length > 0 ? this.selectedExcursions : [],
        ['totalCharge']: this.fullTotal
      };
      this.dialogRef.close(this.userSelection);
      console.log(this.userSelection);
    }
  }
}
