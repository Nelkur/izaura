import { Component, ElementRef, Renderer2, AfterViewInit, OnInit } from '@angular/core';
import { HomeService } from '../../../services/home/home.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { GlobalService } from '../../../services/global/global.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  providers: [HomeService]
})
export class LandingComponent implements OnInit, AfterViewInit {
  public images: Array<any> = [];
  private slides;
  private dots;
  private slideIndex: any = 1;
  private len;
  private imagesSnap;

  public destinations;
  public packages;
  public filterValue: any = 'All';

  private isLoggedIn: boolean;
  private type = 'seasonal';
  public favorites: Array<object>;
  public userId: number;
  public favList = [];
  public favCount: number;

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private homeservice: HomeService,
    private router: Router,
    private cookieService: CookieService,
    private global: GlobalService,
    private snackBar: MatSnackBar) {
    this.images = [
      {
        path: '../../../assets/images/banner/image1.png',
        path_xs: '../../../assets/images/banner/image1_xs.png',
        caption: 'Banner Image One',
        alt: 'Image number one'
      },
      {
        path: '../../../assets/images/banner/image2.png',
        path_xs: '../../../assets/images/banner/image2_xs.png',
        caption: 'Banner Image two',
        alt: 'Image number two'
      },
      {
        path: '../../../assets/images/banner/image3.png',
        path_xs: '../../../assets/images/banner/image3_xs.png',
        caption: 'Banner Image three',
        alt: 'Image number three'
      },
      {
        path: '../../../assets/images/banner/image4.png',
        path_xs: '../../../assets/images/banner/image4_xs.png',
        caption: 'Banner Image four',
        alt: 'Image number four'
      },
      {
        path: '../../../assets/images/banner/image5.png',
        path_xs: '../../../assets/images/banner/image5_xs.png',
        caption: 'Banner Image five',
        alt: 'Image number five'
      },
      {
        path: '../../../assets/images/banner/image6.png',
        path_xs: '../../../assets/images/banner/image6_xs.png',
        caption: 'Banner Image six',
        alt: 'Image number six'
      }
    ];
  }

  ngOnInit() {
    this.favCount = localStorage.getItem('__paypal_storage__') === null ? localStorage.length : localStorage.length - 1;
    this.getDestinations();
    this.getPackages();

    this.isLoggedIn = this.cookieService.check('user');
    this.fetchUser();
    this.fetchFavorites(this.userId); // Hopefully the id will exist before this method is called

    /* Subscribe to user logged in status change event */
    this.global.loginStatusChange.subscribe(res => {
      this.fetchUser();
      this.fetchFavorites(this.userId);
    });
    this.updateCount(this.favCount);
  }

  ngAfterViewInit() {
    this.slides = this.el.nativeElement.querySelectorAll('.slide');
    this.dots = this.el.nativeElement.querySelectorAll('.slider__dots > *');
    this.len = this.slides.length;

    this.showImage(this.slideIndex);
    this.autoslide();
  }

  /* The main slider method */
  private showImage(n) {

    if (n > this.len) { this.slideIndex = 1; }
    if (n < 1) { this.slideIndex = this.len; }

    for (let i = 0; i < this.len; i++) {
      this.renderer.setStyle(this.slides[i], 'display', 'none');
      this.renderer.removeStyle(this.dots[i], 'backgroundColor');
    }

    this.renderer.addClass(this.slides[this.slideIndex - 1], 'fade');
    this.renderer.setStyle(this.slides[this.slideIndex - 1], 'display', 'block');
    this.renderer.setStyle(this.dots[this.slideIndex - 1], 'backgroundColor', 'darkslategrey');
  }


  /* Cycles through the images, either previous or next */
  public imageCycle(n) {
    this.showImage(this.slideIndex += n);
  }

  /* Handles the dots */
  public currentImage(n) {
    this.showImage(this.slideIndex = n);
  }

  /* Automatically slide through the images */
  private autoslide() {
    this.imageCycle(1);
    setTimeout(this.autoslide.bind(this), 15000);
  }

  /* Get seasonal destinations */
  private getDestinations() {
    return this.homeservice.fetchDestinations().subscribe(res => {
      this.destinations = res.data;
    });
  }

  /* Get seasonal_packages */
  private getPackages() {
    return this.homeservice.fetchPackages().subscribe(res => {
      this.packages = res.data;
      this.favorites = this.transformPackages(res.data);
    });
  }

  public filterBy(value) {
    this.filterValue = value;
  }

  public getPackageDetails(packageId) {
    const url = 'seasonal/All/';
    this.router.navigate([url, packageId]);
  }

  /* Favorites */
  private fetchUser() {
    if (this.isLoggedIn) {
      this.userId = JSON.parse(this.cookieService.get('user'))[0];
    }
  }

  private fetchFavorites(userId: number) {
    if (this.isLoggedIn) {
      return this.global.getFavorites(userId).subscribe(res => {
        if (res.data.length > 0) {
          this.favList = this.transformFavorites(res.data);
        }
      });
    } else {
      this.fetchFavList();
    }
  }

  private fetchFavList() {
    if (this.favCount > 0) {
      return Object.entries(localStorage).filter(arr => {
        if (arr[1] === this.type) {
          this.favList.push(arr[0].substring(4));
        }
      });
    }
  }

  /* Transform the array of objects { id, pacageType } - filter by type then extract just the id */
  private transformFavorites(arr) {
    return arr.filter(obj => obj[1] === this.type).reduce((result, item) => {
      result.push(item[0]);
      return result;
    }, []);
  }

  private transformPackages(arr: Array<object>) {
    return arr.map(item => {
      return {
        packageId: item[0],
        packageType: this.type,
        title: item[4],
        image: item[3],
        description: item[6],
        url: `seasonal/All/${item[0]}`,
        duration: `${item[8]}-days`,
        price: item[7]
      };
    });
  }

  /* Check if favorite is in favList */
  public isSaved(id) {
    if (this.favList.length >= 1) {
      return this.favList.indexOf(id) > -1;
    } else {
      return false;
    }
  }

  /* Update the favorites count */
  private updateCount(count) {
    this.global.favCountEvent.emit(count);
  }

  // toggleSave

  public toggleFavorite(id: number, pkg) {
    if (!this.isLoggedIn) {
      if (localStorage.getItem(`sfav${id}`) === null) {
        localStorage.setItem(`sfav${id}`, this.type);
        this.favList.push(id);
        this.openSnackBar('Added to favorites', 'Undo');
        this.updateCount(this.favCount += 1);
        // console.log(this.favList);
      } else {
        localStorage.removeItem(`sfav${id}`);
        this.favList = this.favList.filter(item => item !== id);
        this.updateCount(this.favCount -= 1);
        this.openSnackBar('Removed from favorites', 'Undo');
      }
    } else { // Check fav list, if true, delete but if doesn't exist delete it from db
      if (this.isSaved(id)) {
        this.global.removeFavorite(this.userId, pkg).subscribe(res => {
          this.favList = this.favList.filter(item => item !== id);
          this.updateCount(this.favCount -= 1);
          this.openSnackBar('Removed from favorites', 'Undo');
        });
      } else {
        this.global.postToFavorites(this.userId, pkg).subscribe(res => {
          // console.log(res);
          this.favList.push(id);
          this.updateCount(this.favCount += 1);
          this.openSnackBar('Added to favorites', 'Undo');
        });
      }
    }
  }

  public openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
