import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { GlobalService } from '../../../services/global/global.service';
import { forkJoin } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  public hasFavs = localStorage.length;
  public pageReady = false;
  public favorites: object;
  public packages: Array<object> = [];
  public filterValue: '';

  constructor(private cookieService: CookieService, private global: GlobalService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.favorites = this.favList();
    this.fetchPackages(this.favorites);
  }

  /* Filter the favorites based on inbound, outbound, seasonal, weekends */
  public filterBy(value) {
    this.filterValue = value;
    console.log(`${value}`);
  }

  /* Grab the key value pairs from local storage and use them to construct an object like... {seasonal: [], inbound: []} */
  private favList() {
    if (this.hasFavs) {
      return Object.entries(localStorage).reduce((result, item) => {
        if (!result[item[1]]) { result[item[1]] = []; }
        result[item[1]].push(item[0].substring(4));
        return result;
      }, {});
    }
  }

  /* Fetch packages from the db and transform em and save to favList */
  private fetchPackages(data: any) {
    let keys;
    if (this.hasFavs) {
      keys = Object.keys(data);

      if (keys.length === 1) {
        const method = `${keys[0]}Transform`;
        this.global.fetchPackages(keys[0]).subscribe(res => {
          if (this[method]) {
            this.packages = this[method](keys[0], res.data);
          }
        });
        this.pageReady = true;
      } else {
        this.transformer(keys);
      }
    }
  }

  /* Package transformations */
  private seasonalTransform(key, arr: Array<object>) {
    return arr.filter(item => this.favorites[key].indexOf(item[0]) > -1).map(item => {
      return {
        packageId: item[0],
        packageType: key,
        title: item[4],
        image: item[3],
        description: item[6],
        url: `seasonal/${item[1]}/${item[0]}`,
        duration: `${item[8]}-day`,
        price: item[7]
      };
    });
  }

  private inboundTransform(key, arr: Array<object>) {
    return arr.filter(item => this.favorites[key].indexOf(item[0]) > -1).map(item => {
      return {
        packageId: item[0],
        packageType: key,
        title: item[1],
        image: item[2],
        description: item[3],
        url: `inbound/${item[7]}/${item[0]}`,
        duration: `${item[5]}-day`,
        price: item[4]
      };
    });
  }

  private outboundTransform(key, arr: Array<object>) {
    return arr.filter(item => this.favorites[key].indexOf(item[0]) > -1).map(item => {
      return {
        packageId: item[0],
        packageType: key,
        title: item[1],
        image: item[2],
        description: item[3],
        url: `outbound/${item[5]}/${item[0]}`,
        duration: `${item[6]}-day`,
        price: item[4]
      };
    });
  }

  private weekendsTransform(key, arr: Array<object>) {
    return arr.filter(item => this.favorites[key].indexOf(item[0]) > -1).map(item => {
      return {
        packageId: item[0],
        packageType: key,
        title: item[1],
        image: item[2],
        description: item[3],
        url: `weekends/${item[0]}`,
        duration: '',
        price: item[10]
      };
    });
  }

  private transformer(targets: string[]) {
    const observables = new Array();

    for (const target of targets) {
      observables.push(this.global.fetchPackages(target));
    }

    return forkJoin(observables).subscribe(res => {
      if (res) {
        console.log(`Logging ${res}`);

        for (let i = 0; i < targets.length; i++) {
          const method = `${targets[i]}Transform`;
          if (this[method]) {
            console.log(`Logging... ${res[i]}`);
            // this.packages = this.packages.concat(this[method](targets[i], res[i].data)); // execute transform
          }
        }
        this.pageReady = true;
      }
    }, error => console.log('Error: ', error));
  }

  /* Remove from favorite */
  public remove(fav) {
    const id = fav['packageId'];
    const type = fav['packageType'];

    // Remove from packages
    this.packages = this.packages.filter(pkg => pkg !== fav);

    // Remove from favorites
    this.favorites[type] = this.favorites[type].filter(pkgId => pkgId !== id);
    this.openSnackBar('Removed from favorites', 'Undo');

    // Remove from localstorage
    Object.entries(localStorage).forEach(
      ([key, value]) => {
        if (value === type && key.substr(4) === id) {
          localStorage.removeItem(key);
          this.hasFavs = this.hasFavs - 1;
        }
      }
    );
  }

  public openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
