import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../../material.module';
import { HomeRoutingModule } from './home-routing.module';
import { LandingComponent } from './landing/landing.component';

import { PipesModule } from '../../pipes.module';
import { DirectivesModule } from '../../directives.module';
import { FavoritesComponent } from './favorites/favorites.component';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    MaterialModule,
    PipesModule,
    DirectivesModule
  ],
  declarations: [LandingComponent, FavoritesComponent]
})
export class HomeModule { }
