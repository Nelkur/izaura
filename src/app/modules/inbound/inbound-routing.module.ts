import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { PackageDetailsComponent } from './package-details/package-details.component';
import { LocationDetailsComponent } from './location-details/location-details.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent
  },
  {
    path: ':destination',
    children: [
      {
        path: '',
        component: LandingComponent
      },
      {
        path: ':packageId',
        component: PackageDetailsComponent
      }
      // {
      //   path: 'locations/:id',
      //   component: LocationDetailsComponent
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InboundRoutingModule { }
