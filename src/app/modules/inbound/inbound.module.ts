import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { PipesModule } from '../../pipes.module';
import { NgxGalleryModule } from 'ngx-gallery';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DirectivesModule } from '../../directives.module';

import { InboundRoutingModule } from './inbound-routing.module';
import { LandingComponent } from './landing/landing.component';
import { PackageDetailsComponent } from './package-details/package-details.component';
import { LocationDetailsComponent } from './location-details/location-details.component';
import { HotelDialogComponent } from './hotel-dialog/hotel-dialog.component';
import { SummaryDialogComponent } from './summary-dialog/summary-dialog.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { LocationsComponent } from './locations/locations.component';
import { FormComponent } from './form/form.component';

@NgModule({
  declarations: [
    LandingComponent,
    PackageDetailsComponent,
    LocationDetailsComponent,
    HotelDialogComponent,
    SummaryDialogComponent,
    CheckoutComponent,
    LocationsComponent,
    FormComponent
  ],
  imports: [
    CommonModule,
    InboundRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    PipesModule,
    FlexLayoutModule,
    NgxGalleryModule,
    DirectivesModule
  ],
  entryComponents: [HotelDialogComponent, SummaryDialogComponent, FormComponent, LocationsComponent]
})
export class InboundModule { }
