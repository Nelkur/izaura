import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { forkJoin } from 'rxjs';
import { InboundDetailsService } from '../../../services/inbound/inbound-details.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
  selector: 'app-package-details',
  templateUrl: './package-details.component.html',
  styleUrls: ['./package-details.component.scss'],
  providers: [InboundDetailsService]
})
export class PackageDetailsComponent implements OnInit {
  private packageId: any;
  public url: String;
  public pageReady = false;
  public loadStatus = 'Getting ready';
  public shareLink = 'Click to copy link';

  public package: Object;
  public packageImages: NgxGalleryImage[];
  public itinerary: Array<Object> = [];
  public galleryOptions: NgxGalleryOptions[];

  constructor(private detailsService: InboundDetailsService,
    private router: Router,
    private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activeRoute.paramMap.subscribe((params: ParamMap) => {
      this.packageId = params.get('packageId');
    });
    this.url = this.router.url;
    this.getData(this.packageId);
    this.galleryOptions = [
      {
        width: '850px',
        height: '450px',
        imageAutoPlay: true,
        imageAutoPlayPauseOnHover: true,
        previewAutoPlay: true,
        previewAutoPlayPauseOnHover: true,
        thumbnailsMoveSize: 5,
        thumbnailsColumns: 5,
        thumbnailMargin: 10,
        previewFullscreen: true,
        previewKeyboardNavigation: true,
        previewCloseOnClick: true,
        previewCloseOnEsc: true
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];
  }

  private getData(id) {
    return forkJoin(
      this.detailsService.fetchPackage(id),
      this.detailsService.fetchPackageImages(id),
      this.detailsService.fetchItinerary(id)
    ).subscribe(res => {
      this.package = res[0].data[0];
      this.packageImages = res[1].data.map(item => {
        return {
          small: item[2],
          medium: item[2], /*.substr(0, item[3].length - 7) + '_md.png'*/
          big: item[2], /* .substr(0, item[3].length - 7) + '_bg.png' */
          description: item[3]
        };
      });
      this.itinerary = res[2].data;
      this.pageReady = true;
    });
  }

  /* Function that allows user to copy link in the share section */
  public copyLink(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    // this.shareLink = 'Link copied';
  }

}
