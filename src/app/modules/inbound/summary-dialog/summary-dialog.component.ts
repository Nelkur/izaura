import { Component, Inject, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as _moment from 'moment';

export interface DialogData {
  packages: Array<object>;
  hotels: any;
  formData: any;
  bundled: any;
  multiple: boolean;
  pickups: Array<Object>;
  transport: Array<Object>;
  excursions: object[];
}

@Component({
  selector: 'app-summary-dialog',
  templateUrl: './summary-dialog.component.html',
  styleUrls: ['./summary-dialog.component.scss']
})
export class SummaryDialogComponent {
  public counter: Array<any> = [];
  public doubleRooms: Array<any> = [];
  public flight = 'custom';
  public rows: { [key: string]: { paxSingle: number, paxDouble: number } } = {};
  public selected: { [key: string]: { [key: string]: any } } = {};
  public userSelection: object = {};
  public pax = this.data.formData.travellers.adults + this.data.formData.travellers.children;
  public totals: { [key: string]: number } = {};
  public message = '';
  public packageOrder: Array<String>;
  public packageFirst;
  public pickups;
  public groupedPickups;
  public pick;
  public pickupLocation: { [key: string]: any };
  public transportPackage;
  public transport;
  public selectedTransport: { [key: string]: any } = {};
  // public selectedExcursions: { [key: string]: number } = {};
  public selectedExcursions = {};
  public excursionsMap: { [key: string]: string } = {};
  public excursions = Object.keys(this.data.excursions);
  private excursionsTotal = 0;
  public date = '';
  public flightPax: number;

  @ViewChild('form', { static: true }) private form;

  constructor(public dialogRef: MatDialogRef<SummaryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private el: ElementRef, private renderer: Renderer2) {
    this.populateArray();
    this.rowTotals();
    this.packageOrder = this.orderPackages();
    this.firstPackage();
    this.sortPickups();
    this.pick = this.pickups[0][0];
    this.sortTransport();
    this.flightPax = !!this.data.formData.travellers.children ? 1 : this.data.formData.travellers.adults;
    this.constructExcursionMap();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get getDate() {
    const { year, month, date } = this.data.formData.date._i;
    return _moment(new Date(year, month, date).getTime()).format('dddd, MMMM Do YYYY');
  }

  private populateArray() {
    for (let i = 1; i <= 10; i++) {
      this.counter.push(i);
    }
    this.doubleRooms = this.counter.filter(room => {
      return room % 2 === 0;
    });
  }

  private constructExcursionMap() {
    Object.entries(this.data.packages).forEach(
      ([key, value]) => Object.assign(this.excursionsMap, {
        [value[0]]: key
      })
    );
  }

  public toggleExcursion() {
    this.selectedExcursions = this.data.excursions.filter(x => !!x['pax']).reduce((result, item) => {
      if (!result[this.excursionsMap[item['package']]]) { result[this.excursionsMap[item['package']]] = {}; }
      result[this.excursionsMap[item['package']]] = Object.assign(result[this.excursionsMap[item['package']]], {
        [`${item['title']}-${item['pax']}-${item['price']}`]: item['price'] * (item['pax'] || 0)
      });

      return result;
    }, {});
  }

  get excursionTotal() {
    // tslint:disable-next-line: max-line-length
    const ttl = this.data.excursions.map(ex => Number.parseFloat(ex['price']) * (ex['pax'] || 0)).reduce((total, price) => total + price, 0);
    this.excursionsTotal = ttl;
    return ttl;
  }

  /* Package order if multiple */
  private orderPackages() {
    if (this.data.multiple) {
      return Object.keys(this.data.packages);
    }
  }

  /* Set the default package order if multiple - for radio */
  private firstPackage() {
    if (this.data.multiple) {
      this.packageFirst = this.packageOrder.indexOf('safari') > -1 ? 'safari' : this.packageOrder[0];
    }
  }

  /* Return all the pickup points for the first package */
  public sortPickups() {
    let packageId;
    if (this.data.multiple) {
      packageId = this.data.packages[this.packageFirst][0];
    } else {
      packageId = Object.entries(this.data.packages)[0][1][0];
    }
    // Get the id of the first package
    this.pickups = this.data.pickups[packageId];
    this.groupedPickups = this.groupPickups(this.pickups, 3);
    this.transportPackage = this.data.transport[packageId];
    this.sortTransport();
  }

  public sortTransport() {
    this.transport = this.transportPackage.filter(item => {
      return item[2] === this.pick;
    });
  }

  private getPickup() {
    if (this.form.value.pickup !== 'idk') {
      const pickup = this.pickups.filter(item => item[0] === this.form.value.pickup)[0];
      this.pickupLocation = {
        city_or_town: pickup[3],
        location: pickup[4]
      };
    } else {
      this.pickupLocation = {
        city_or_town: 'idk',
        location: 'idk'
      };
    }
  }

  public getPax(type: string, pax: number) {
    if (type === 'flight' || type === 'Flight') {
      return this.flightPax;
    }
    return pax;
  }

  public getPaxTotal(type: string, price: number) {
    if (type === 'flight' || type === 'Flight') {
      return this.flightPax * price;
    }
    return price;
  }

  /* Handle selecting a transport package */
  public selectTransport(obj: object) {
    // const formDataPax = this.data.formData.travellers.adults + this.data.formData.travellers.children;
    // const pax = obj[3] === 'flight' || obj[3] === 'Flight' ? this.flightPax : formDataPax;
    const pax = obj[3] === 'flight' || obj[3] === 'Flight' ? this.flightPax : 1;
    this.selectedTransport = {};
    this.selectedTransport = {
      id: obj[0],
      means: obj[3],
      pax,
      unitPrice: obj[6],
      totalPrice: obj[6] * pax
    };
    return;
  }

  public unselectTransport() {
    this.selectedTransport = {};
  }

  /* Handle accordion */
  public isSaved(id) {
    return id === this.selectedTransport['id'];
  }

  private groupPickups(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = []; }
      result[item[prop]].push(item);
      return result;
    }, {});
  }

  /* Relevant calculations for accommodation  - to be accessed through {{ row[hotel.key]['pax...']}} */
  public rowTotals() {
    Object.entries(this.data.hotels).forEach(
      ([key, value]) => Object.assign(this.rows, {
        [key]: {
          paxSingle: ((value['paxSingle'] || 0) * this.data.packages[key][6]) * value['singleRoomPrice'],
          paxDouble: ((value['paxDouble'] || 0) * this.data.packages[key][6]) * value['doubleRoomPrice']
        }
      })
    );
  }

  /* Get the total for a package accommodation */
  public computeTotal(holidayType) {
    return this.rows[holidayType]['paxSingle'] + this.rows[holidayType]['paxDouble'];
  }

  /* Total for the entire thing */
  get fullTotal() {
    let total = 0;
    // tslint:disable-next-line:forin
    for (const key in this.rows) {
      total += this.rows[key].paxSingle + this.rows[key].paxDouble;
    }
    return total + this.excursionsTotal + this.selectedTransport.totalPrice;
  }

  /* On change */
  public onPaxChange(key) {
    if (!!this.data.hotels[key]['paxSingle'] && !!this.data.hotels[key]['paxDouble']) {
      // Concatenate result to the object don't assign
      Object.assign(this.selected, {
        [key]: {
          hotel: this.data.hotels[key]['hotelName'],
          [`single room-${this.data.hotels[key]['paxSingle']}-${this.data.hotels[key]['singleRoomPrice']}`]:
            (this.data.hotels[key]['singleRoomPrice'] * this.data.packages[key][6]) * (this.data.hotels[key]['paxSingle'] || 0),
          [`double room-${this.data.hotels[key]['paxDouble']}-${this.data.hotels[key]['doubleRoomPrice']}`]:
            (this.data.hotels[key]['doubleRoomPrice'] * this.data.packages[key][6]) * (this.data.hotels[key]['paxDouble'] || 0)
        }
      });
      this.rowTotals();
    } else if (!!this.data.hotels[key]['paxSingle']) {
      Object.assign(this.selected, {
        [key]: {
          hotel: this.data.hotels[key]['hotelName'],
          [`single room-${this.data.hotels[key]['paxSingle']}-${this.data.hotels[key]['singleRoomPrice']}`]:
            (this.data.hotels[key]['singleRoomPrice'] * this.data.packages[key][6]) * (this.data.hotels[key]['paxSingle'] || 0)
        }
      });
      this.rowTotals();
    } else if (!!this.data.hotels[key]['paxDouble']) {
      Object.assign(this.selected, {
        hotel: this.data.hotels[key]['hotelName'],
        [`double room-${this.data.hotels[key]['paxdouble']}-${this.data.hotels[key]['doubleRoomPrice']}`]:
          (this.data.hotels[key]['doubleRoomPrice'] * this.data.packages[key][6]) * (this.data.hotels[key]['paxdouble'] || 0)
      });
      this.rowTotals();
    }
  }

  /* Compare number to travellers to those in accommodation - need to be equal */
  get guestCount() {
    return Object.entries(this.data.hotels).forEach(
      ([key, value]) => {
        Object.assign(this.totals, {
          [key]: (value['paxSingle'] ? value['paxSingle'] : 0) + (value['paxDouble'] ? value['paxDouble'] : 0)
        });
      }
    );
  }

  public comparePaxTravellers() {
    return 0;
  }

  private condensePackages() {
    const result = [];
    Object.entries(this.data.packages).forEach(
      ([key, value]) => {
        result.push({
          id: value[0],
          type: key, // value[8]
          title: value[1],
          byline: `${value[9]}, ${value[7]}`,
          image: value[2],
          days: value[5],
          nights: value[6],
          link: ''
        });
      }
    );
    return result;
  }

  /* Checkout */
  public checkout() {
    if (this.form.valid) {
      this.getPickup();
      this.userSelection = {
        ['packages']: this.condensePackages(),
        ['packageType']: 'inbound',
        ['firstPackage']: this.data.multiple ? this.packageFirst : null,
        ['durations']: this.data.formData.durations,
        ['excursions']: Object.keys(this.selectedExcursions).length > 0 ? this.selectedExcursions : [],
        ['accommodation']: this.selected,
        ['transport']: this.selectedTransport,
        ['flight']: this.form.value.flight,
        ['pickup']: this.pickupLocation,
        ['airport']: this.form.value.airport ? this.form.value.airport : false,
        ['departure']: this.getDate,
        ['totalCharge']: this.fullTotal
      };
      this.dialogRef.close(this.userSelection);
      // this.form.reset();
    }
  }
}
