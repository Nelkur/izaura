import { Component, OnInit, ViewChild, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { InboundService } from '../../../services/inbound/inbound.service';

import { MatDialog } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { HotelDialogComponent } from '../hotel-dialog/hotel-dialog.component';
import { SummaryDialogComponent } from '../summary-dialog/summary-dialog.component';
import { LocationsComponent } from '../locations/locations.component';
import { FormComponent } from '../form/form.component';

import { CookieService } from 'ngx-cookie-service';
import { GlobalService } from '../../../services/global/global.service';
import { MatSnackBar } from '@angular/material/snack-bar';

/* Domain model for our form */
class SearchForm {
  constructor(public destination: String = '', public holidayTypes = []) { }
}

class FilterForm {
  constructor(public countries: Array<String> = [], public types: Array<any> = [], public locations: Array<any> = []) { }
}

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  providers: [InboundService]
})
export class LandingComponent implements OnInit {
  public model = new SearchForm();
  public filters = new FilterForm();
  public sidebarState = true;
  @ViewChild('form', { static: true }) form: any;
  private formValue;
  public counter = [];
  public destinationFilter = 'All';
  public searched = false;
  public selectedPackages = []; // Stores the ids of the selected packages
  public accPackages: Object; // Accommodation packages grouped by package id
  public accData = false; // Have we yet retrieved accommodation packages
  public hotelImages: NgxGalleryImage[];
  public galleryOptions: NgxGalleryOptions[];
  public stepTitles = [];
  public selectedHotels: { [key: string]: any }; // Define a loose object to hold selected hotel values
  public packageBundle: { [key: string]: boolean };
  public userSelection: object;
  @ViewChild('stepper', { static: true }) private myStepper: MatStepper;
  @ViewChild('proceed', { static: false }) proceed: any;

  public destinations: Array<Object> = [];
  public types: Array<Object> = [];
  public destinationTypes: Array<Object> = [];
  public packages: Array<Object> = [];
  public locations: Array<Object> = [];
  public places: object[] = [];
  public residentTypes: Array<Object> = [];
  private pickupPoints: Array<Object> = [];
  private transportOptions: Array<Object> = [];
  private excursions: object[] = [];
  public residency = 'resident';

  // Customize itinerary
  public userLocations: string[] = [];
  private userSelections: object[] = [];
  public customLocations: object;

  public locationFilters: { [key: number]: string[] } = {};

  /* Favorites */
  private isLoggedIn: boolean;
  private type = 'inbound';
  public favorites: Array<object>;
  public userId: number;
  public favList = [];
  public favCount: number;
  @ViewChildren('accOptions') private acSections: QueryList<any>;

  public country: string[] = [];
  public holidayTypes: string[] = [];
  public city_or_town: string[] = [];

  constructor(private inboundService: InboundService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
    private dialog: MatDialog,
    private cookieService: CookieService,
    private global: GlobalService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getDestinations();
    this.getInboundTypes();
    this.getDestinationTypes();
    this.getPackages();
    this.getLocations();
    this.getCityTowns();
    this.getPickupTypes();
    this.populateCounter();
    this.galleryOptions = [
      {
        width: '814px',
        height: '407px',
        imageAutoPlay: true,
        imageAutoPlayPauseOnHover: true,
        previewAutoPlay: true,
        previewAutoPlayPauseOnHover: true,
        thumbnailsMoveSize: 5,
        thumbnailsColumns: 5,
        thumbnailMargin: 10,
        previewFullscreen: true,
        previewKeyboardNavigation: true,
        previewCloseOnClick: true,
        previewCloseOnEsc: true
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];

    this.activeRoute.params.subscribe(param => {
      this.destinationFilter = param['destination'];
    });
    this.destinationFilter = 'All';

    /* Favorites */
    this.favCount = localStorage.getItem('__paypal_storage__') === null ? localStorage.length : localStorage.length - 1;
    this.isLoggedIn = this.cookieService.check('user');
    this.fetchUser();
    this.fetchFavorites(this.userId); // Hopefully the id will exist before this method is called

    /* Subscribe to user logged in status change event */
    this.global.loginStatusChange.subscribe(res => {
      this.fetchUser();
      this.fetchFavorites(this.userId);
    });
    this.updateCount(this.favCount);
  }

  get validForm() {
    return this.form.valid && this.form.value.location !== '' ? true : false;
  }

  /* Get destination */
  private getDestinations() {
    return this.inboundService.fetchDestinations().subscribe(res => {
      this.destinations = res.data;
    });
  }

  /* Get inbound types */
  private getInboundTypes() {
    return this.inboundService.fetchTypes().subscribe(res => {
      this.types = res.data;
    });
  }

  /* Get package types */
  private getDestinationTypes() {
    return this.inboundService.fetchDestinationTypes().subscribe(res => {
      this.destinationTypes = res.data;
    });
  }

  /* Get packages */
  private getPackages() {
    return this.inboundService.fetchPackages().subscribe(res => {
      this.packages = res.data;
      this.favorites = this.transformPackages(res.data);
    });
  }

  /* Get inbound locations */
  private getLocations() {
    return this.inboundService.fetchLocations().subscribe(res => {
      this.locations = res.data;
    });
  }

  private getCityTowns() {
    return this.inboundService.fetchCityTowns().subscribe(res => {
      this.places = res.data;
    });
  }

  /* For resident or non resident options */
  private getPickupTypes() {
    return this.inboundService.fetchPickupTypes().subscribe(res => {
      this.residentTypes = res.data.map(item => {
        return item[1];
      }).reverse();
    });
  }

  /* When the search form is submitted */
  public onSubmit() {
    this.searched = true;
    this.sidebarState = false;
    if (this.form.valid) {
      this.formValue = this.form.value;
      this.searched = true;
    }
  }

  /* When the user selects the next step, push the id of the selected package to the array */
  public nextStep(id, index) {
    this.selectedPackages.push(id);
    this.isLast(index);
  }

  /* When the user goes back one step, remove that step's id from the selectedPackages array */
  public previousStep(id) {
    this.selectedPackages.pop();
  }

  /* If the user is on the last step, take the package ids and fetch accommodaiton options */
  public isLast(index) {
    const length = parseInt(index, 10) + 1;
    if (length === this.model.holidayTypes.length) {
      /* Fetch accommodation options and hotel images */
      return forkJoin(
        this.inboundService.fetchAccommodationPackages(),
        this.inboundService.fetchHotelImages(),
        this.inboundService.fetchPickupPoints(),
        this.inboundService.fetchTransport(),
        this.inboundService.fetchExcursions(`${this.selectedPackages.join()}`)
      ).subscribe(res => {
        // Loop through the returned accommodation packages and filter against the selected packages
        const acOptions = res[0].data.filter(item => {
          if (this.selectedPackages.indexOf(item[2]) > -1) {
            return item;
          }
        });

        this.accPackages = this.groupAccPackages(acOptions, 2);
        this.hotelImages = this.groupHotelImages(res[1].data, 1);
        this.excursions = this.formatPackageExcursions(res[4].data);
        this.stepTitles = this.generateTitles(this.accPackages);

        /* Populate the selected Hotels object that holds the user selected options */
        this.selectedHotels = this.model.holidayTypes.reduce((result, holiday) => {
          if (!result[holiday]) { result[holiday] = {}; }
          return result;
        }, {});
        this.packageBundle = this.model.holidayTypes.reduce((result, holiday) => {
          if (!result[holiday]) { result[holiday] = false; }
          return result;
        }, {});
        this.accData = true; // Display accommodation options
        this.changeDetector.detectChanges();

        /* Pickup points */
        const pickups = this.filterData(res[2].data, 1);
        this.pickupPoints = this.groupPickups([].concat(...pickups), 1);
        /* Transport options */
        const trans = this.filterData(res[3].data, 1);
        this.transportOptions = this.groupTransport([].concat(...trans), 1);
      },
        error => console.log('Error: ', error));
    }
  }

  private groupHotelImages(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = []; }
      result[item[prop]].push(
        {
          small: item[2],
          medium: item[2], /*.substr(0, item[3].length - 7) + '_md.png'*/
          big: item[2], /* .substr(0, item[3].length - 7) + '_bg.png' */
          description: item[3]
        }
      );
      return result;
    }, {});
  }

  private formatPackageExcursions(arr) {
    return arr.map(ex => {
      return {
        id: ex[0],
        package: ex[1],
        title: ex[2],
        price: ex[3],
        description: ex[4]
      };
    });
  }

  private groupAccPackages(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = []; }
      result[item[prop]].push(
        {
          pkgId: item[0],
          pkgTag: item[1],
          fk_package: item[2],
          fk_hotel: item[3],
          hotelId: item[5],
          hotelName: item[6],
          featuredImage: item[7],
          hotelOverview: item[8],
          fk_rating: item[9],
          fk_boardType: item[10],
          singleRoomPrice: item[11],
          doubleRoomPrice: item[12],
          website: item[13],
          fk_country: item[14],
          fk_cityTown: item[15],
          childPolity: item[16],
          dinningExp: item[17],
          activities: item[18],
          accommodation: item[19]
        }
      );
      return result;
    }, {});
  }

  /* Filter an array of objects containing the foreign key to a package based on the selected packages */
  private filterData(arr: Array<object>, prop: any) {
    return this.selectedPackages.map(id => {
      return arr.filter(obj => {
        return obj[prop] === id;
      });
    });
  }

  /* Group an array of objects based on a property on the objects */
  private groupPickups(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = []; }
      if (item[2] === this.formValue.residency) {
        result[item[prop]].push(item);
      }
      return result;
    }, {});
  }

  /* Group an array of objects based on a property on the objects */
  private groupTransport(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = []; }
      if (item[11] === this.formValue.residency) {
        result[item[prop]].push(item);
      }
      return result;
    }, {});
  }

  /* A version of group data but for selecte packages */
  private groupSelectedPackages(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = {}; }
      result[item[prop]] = item;
      return result;
    }, {});
  }

  /* Returns an array containing the holidayTypes based on accPackages groupings */
  private generateTitles(obj) {
    const keys = Object.keys(obj); // Will contain an array of the selected package ids
    const indexes = keys.map(item => {
      return this.selectedPackages.indexOf(item);
    });
    return indexes.map(index => {
      return this.model.holidayTypes[index];
    });
  }

  /* Selecting accommodation */
  public selectHotel(hotel: object, holidayType: string, bundled: boolean, index: number) {
    this.selectedHotels[holidayType] = hotel;
    this.packageBundle[holidayType] = bundled;
    const nextIndex = index + 1;
    const queryList = this.acSections.toArray();

    if (nextIndex === this.acSections.length) {
      this.proceed.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    } else {
      queryList[nextIndex].nativeElement.scrollIntoView({ behavior: 'smooth' });
    }
  }

  public getLocationsImages(location: object) {
    return this.inboundService.fetchLocationImages(location[0]).subscribe(res => {
      const galleryImages = res.data.map(item => {
        return {
          small: item[2],
          medium: item[2], /*.substr(0, item[3].length - 7) + '_md.png'*/
          big: item[2], /* .substr(0, item[3].length - 7) + '_bg.png' */
          description: item[3]
        };
      });

      this.openLocationsDialog(location, galleryImages);
    });
  }

  /* Open the dialog for the individual location */
  private openLocationsDialog(location: object, images: NgxGalleryImage[]) {
    const dialogRef = this.dialog.open(LocationsComponent, {
      width: '700px',
      height: '100%',
      data: {
        location: location,
        galleryOptions: this.galleryOptions,
        locationImages: images
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  public openHotelDialog(packageId, hotelId) {
    // Get the array of hotels from the accPackages object and get our specific hotel
    const hotel = this.accPackages[packageId].filter(item => {
      return item['hotelId'] === hotelId;
    });

    const dialogRef = this.dialog.open(HotelDialogComponent, {
      width: '700px',
      height: '100%',
      data: {
        hotel: hotel[0],
        galleryOptions: this.galleryOptions,
        hotelImages: this.hotelImages[hotelId]
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  /* Customize locations */
  public toggleLocation(name: string, country: string) {
    if (this.userLocations.indexOf(name) === -1) {
      this.userLocations.push(name);
      this.userSelections.push({ location: name, country: country });
    } else {
      this.userLocations.splice(this.userLocations.indexOf(name), 1);
      this.userSelections = this.userSelections.filter((item) => {
        return item['location'] !== name;
      });
    }
    this.customLocations = this.groupLocations(this.userSelections, 'country');
  }

  private groupLocations(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = []; }
      result[item[prop]].push(item['location']);
      return result;
    }, {});
  }

  public isSelected(location: string): boolean {
    return this.userLocations.indexOf(location) > -1;
  }

  public filterCountry(name: string) {
    const index = this.country.indexOf(name);
    if (index === -1) {
      this.country.push(name);
    } else {
      this.country.splice(index, 1);
    }
    this.country = [].concat(this.country);
  }

  public filterHolidayType(name: string) {
    const index = this.holidayTypes.indexOf(name);
    if (index === -1) {
      this.holidayTypes.push(name);
    } else {
      this.holidayTypes.splice(index, 1);
    }
    this.holidayTypes = [].concat(this.holidayTypes);
  }

  public filterCity(name: string) {
    const index = this.city_or_town.indexOf(name);
    if (index === -1) {
      this.city_or_town.push(name);
    } else {
      this.city_or_town.splice(index, 1);
    }
    this.city_or_town = [].concat(this.city_or_town);
  }

  /* Open the dialog when a user selects a package */
  public packageSummaryDialog(): void {
    // Returns an array containing array inside it each of which contains an object
    const selectedPackages = this.selectedPackages.map(id => {
      return this.packages.filter(pkg => {
        return pkg[0] === id;
      });
    });

    // Converts the array of arrays into just an array of the objects inside the inner array
    const packages = [].concat(...selectedPackages);
    // console.log(this.groupSelectedPackages([].concat(...selectedPackages), 8));

    const dialogRef = this.dialog.open(SummaryDialogComponent, {
      width: '750px',
      height: '100%',
      data: {
        packages: this.groupSelectedPackages([].concat(...selectedPackages), 8),
        hotels: this.selectedHotels,
        formData: this.formValue,
        bundled: this.packageBundle,
        multiple: selectedPackages.length > 1,
        pickups: this.pickupPoints,
        transport: this.transportOptions,
        excursions: this.excursions
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userSelection = {};
        this.userSelection = result;
        this.myStepper.next(); // Go to checkout
      }
    });
  }

  public openFormDialog() {
    const dialogRef = this.dialog.open(FormComponent, {
      width: '500px',
      data: {
        selection: this.customLocations
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.openSnackBar('Your selection has been saved. We\'l be in touch soon.', '');
      }
    });
  }

  /* Filter destinations via the filter pipe */
  public filterDestinations(dest) {
    this.destinationFilter = dest;
  }

  /* Generate numbers one to ten for the dropdowns */
  private populateCounter() {
    for (let i = 1; i <= 10; i++) {
      this.counter.push(i);
    }
  }

  public fetchPackageDetails(destination, packageId) {
    const url = `inbound/${destination}`;
    this.router.navigate([url, packageId]);
  }

  // (click)="fetchLocationDetails(location[6], location[0])"
  public fetchLocationDetails(destination, locationId) {
    const url = `inbound/${destination}/locations`;
    this.router.navigate([url, locationId]);
  }


  /* Favorites */
  private fetchUser() {
    if (this.isLoggedIn && this.cookieService.check('user')) {
      this.userId = JSON.parse(this.cookieService.get('user'))[0];
    }
  }

  private fetchFavorites(userId: number) {
    if (this.isLoggedIn) {
      return this.global.getFavorites(userId).subscribe(res => {
        if (res.data.length > 0) {
          this.favList = this.transformFavorites(res.data);
        }
      });
    } else {
      this.fetchFavList();
    }
  }

  private fetchFavList() {
    if (this.favCount > 0) {
      return Object.entries(localStorage).filter(arr => {
        if (arr[1] === this.type) {
          this.favList.push(arr[0].substring(4));
        }
      });
    }
  }

  /* Transform the array of objects { id, pacageType } - filter by type then extract just the id */
  private transformFavorites(arr) {
    return arr.filter(obj => obj[1] === this.type).reduce((result, item) => {
      result.push(item[0]);
      return result;
    }, []);
  }

  private transformPackages(arr: Array<object>) {
    return arr.map(item => {
      return {
        packageId: item[0],
        packageType: this.type,
        title: item[1],
        image: item[2],
        description: item[3],
        url: `inbound/${item[7]}/${item[0]}`,
        duration: `${item[5]}-days`,
        price: item[4]
      };
    });
  }

  /* Check if favorite is in favList */
  public isSaved(id) {
    if (this.favList.length >= 1) {
      return this.favList.indexOf(id) > -1;
    } else {
      return false;
    }
  }

  /* Update the favorites count */
  private updateCount(count) {
    this.global.favCountEvent.emit(count);
  }

  // toggleSave

  public toggleFavorite(id: number, pkg) {
    if (!this.isLoggedIn) {
      if (localStorage.getItem(`ifav${id}`) === null) {
        localStorage.setItem(`ifav${id}`, this.type);
        this.favList.push(id);
        this.openSnackBar('Added to favorites', 'Undo');
        this.updateCount(this.favCount += 1);
      } else {
        localStorage.removeItem(`ifav${id}`);
        this.favList = this.favList.filter(item => item !== id);
        this.updateCount(this.favCount -= 1);
        this.openSnackBar('Removed from favorites', 'Undo');
      }
    } else { // Check fav list, if true, delete but if doesn't exist delete it from db
      if (this.isSaved(id)) {
        this.global.removeFavorite(this.userId, pkg).subscribe(res => {
          this.favList = this.favList.filter(item => item !== id);
          this.updateCount(this.favCount -= 1);
          this.openSnackBar('Removed from favorites', 'Undo');
        });
      } else {
        this.global.postToFavorites(this.userId, pkg).subscribe(res => {
          this.favList.push(id);
          this.updateCount(this.favCount += 1);
          this.openSnackBar('Added to favorites', 'Undo');
        });
      }
    }
  }

  public openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
