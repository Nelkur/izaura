import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '../../../services/auth/auth.service';
import { PaymentService } from '../../../services/payments/payment.service';
import { User } from '../../../models/booking.model';
import { Transaction } from '../../../models/transaction.model';

interface DialogData {
  bid: string;
  title: string;
  amount: number;
  points: number;
  wallet_balance: number;
}

@Component({
  selector: 'app-cancel',
  templateUrl: './cancel.component.html',
  styleUrls: ['./cancel.component.scss']
})
export class CancelComponent {
  public confirmed = false;
  public processing = false;
  public credited: number;
  public newBalance: number;

  constructor(public dialogRef: MatDialogRef<CancelComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private auth: AuthService,
    private payment: PaymentService
  ) { }

  public cancelBooking() {
    this.processing = true;
    return this.auth.cancelBooking(this.data.bid, this.data.points).subscribe(async res => {
      if (res) {
        const transaction = await this.modelTransaction(res.user, res._id, res.totalPaid);
        this.payment.addTransaction(transaction).subscribe(resp => {
          if (resp) {
            this.processing = false;
            this.confirmed = true;
          }
        });
      }
    });
  }

  private async modelTransaction(user: User, ref: string, amount: number) {
    const amt = this.computeRefund(amount);
    this.credited = amt;
    this.newBalance = this.data.wallet_balance + amt;
    const { id, type, email, phone } = user;
    const transaction: Transaction = {
      user: { id, type, email, phone },
      payment: {
        type: 'booking cancellation',
        method: 'wallet',
        amount: amt,
        currency_code: 'KES',
        wallet_action: 'deposit',
        wallet_op: amt,
        wallet_balance: this.data.wallet_balance + amt
      },
      details: {
        type: 'wallet',
        description: 'cancelled booking',
        ref
      }
    };

    return transaction;
  }

  get forfeited() {
    return Math.ceil(this.data.amount * 0.05);
  }

  private computeRefund(amount: number): number {
    const forfeited = Math.round(amount * 0.05);
    return amount - forfeited;
  }

}
