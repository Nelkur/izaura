import { Component, ElementRef, Renderer2, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../../../services/auth/auth.service';
import { PaymentService } from '../../../services/payments/payment.service';
import { GlobalService } from '../../../services/global/global.service';
import { forkJoin } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { UpdateComponent } from '../update/update.component';
import { PaymentComponent } from '../payment/payment.component';
import { CancelComponent } from '../cancel/cancel.component';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Booking, User, Accommodation, Package, Installments, BookingTotals } from '../../../models/booking.model';
import { Transaction } from '../../../models/transaction.model';
import * as _moment from 'moment';

@Component({
	selector: 'app-account',
	templateUrl: './account.component.html',
	styleUrls: [ './account.component.scss' ]
})
export class AccountComponent implements OnInit {
	public menus = [ 'home', 'bookings', 'wallet', 'favorites', 'reviews & ratings', 'notifications', 'izaura points' ];
	public icons = [
		'home',
		'bookmarks',
		'account_balance_wallet',
		'favorite',
		'rate_review',
		'notification_important',
		'control_point'
	];
	public months = [ 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC' ];
	public retrievedUser: Object;
	private userId: number;
	public user: Object;
	public profile: Object;
	public bookings: Booking[];
	public filteredBookings: Booking[] = [];
	public bookingTotals: BookingTotals[];
	public walletBalance: number;
	public transactions: Transaction[];
	public installments: Installments[];
	public favorites: Array<Object>;
	public favGroup: Object;
	public savings: Array<Object>;
	public totalPoints: number;
	public totalSavings: number;
	public secondaryEmail: boolean;
	public secondaryPhone: boolean;
	public favList: Array<object>;
	public pageReady = false;
	public transferWarning: boolean;
	public walletWarning: boolean;
	public success = false;
	public successMessage = '';
	public filters = [ 'pending', 'ongoing', 'paid', 'cancelled' ];
	public filterValue: any = 'All';

	public payment_notifications = {
		title: 'payments',
		description: 'You will be notified when you',
		data: [
			'Make a payment',
			'Deposit into your savings account',
			'Start a the booking process but do not complete it',
			'Have a problem with payment'
		]
	};
	public marketing_notifications = {
		title: 'marketing',
		description:
			'We’ll send info that’s relevant to you. You can choose what you’d like to get from us and how we should send it.',
		data: [
			{
				category: 'news',
				description:
					'We’ll send important info about our products and benefits to help you get the most from your account.'
			},
			{
				category: 'offers',
				description:
					'From travel to technology and fashion to food, we’ll send discounts and offers from our partner brands.'
			},
			{
				category: 'surveys',
				// tslint:disable-next-line:max-line-length
				descrption:
					'From time to time, we’ll invite you to share your opinions. By taking part, you can help us create an even better Izaura.'
			},
			{
				category: 'Site updates',
				description:
					'From travel to technology and fashion to food, we’ll send discounts and offers from our partner brands.'
			}
		]
	};

	public opened = true;
	public path = '../../../assets/user.png';
	public activeMenu = 'home';
	private rows: any;

	constructor(
		private el: ElementRef,
		private renderer: Renderer2,
		private router: Router,
		private activeRoute: ActivatedRoute,
		private cookieService: CookieService,
		private dialog: MatDialog,
		private auth: AuthService,
		private payment: PaymentService,
		private global: GlobalService,
		private snackBar: MatSnackBar
	) {}

	ngOnInit() {
		this.activeRoute.params.subscribe((param) => {
			this.activeMenu = param['menu'];
		});

		if (this.cookieService.check('user')) {
			this.retrievedUser = JSON.parse(this.cookieService.get('user'));
		}

		this.userId = this.retrievedUser[0]; // Get the user id
		this.getBookings(this.userId);
		this.getBookingTotals(this.userId);
		this.getNextInstallments(this.userId);
		this.getWalletTransactions(this.userId);
		this.getwalletBalance(this.userId);
		this.getUserAccountInfo(this.retrievedUser[0]);
		this.getTotalPoints(this.userId);
	}

	toggleActive(event) {
		if (event.target.closest('.bookings__row')) {
			const row = event.target.closest('.bookings__row');
			if (row.classList.contains('active-booking')) {
				this.renderer.removeClass(row, 'active-booking');
			} else {
				this.renderer.addClass(row, 'active-booking');
			}
			const expanded = row.nextElementSibling;

			if (expanded.style.maxHeight) {
				this.renderer.removeStyle(expanded, 'maxHeight');
			} else {
				this.renderer.setStyle(expanded, 'maxHeight', `${expanded.scrollHeight}px`);
			}
		}

		if (event.target.closest('.close-btn')) {
			const parent = event.target.closest('.bookings__row--expanded');
			if (parent.style.maxHeight) {
				this.renderer.removeStyle(parent, 'maxHeight');
			} else {
				this.renderer.setStyle(parent, 'maxHeight', `${parent.scrollHeight}px`);
			}
		}
	}

	private getBookings(id: number) {
		return this.auth.getBookings(id).subscribe((res: Booking[]) => {
			if (res.length > 0) {
				this.bookings = res;
				this.filterBookings();
			}
		});
	}

	private getWalletTransactions(id: number) {
		return this.auth.getWalletTransactions(id).subscribe((res: Transaction[]) => {
			if (res) {
				this.transactions = res;
			}
		});
	}

	private getBookingTotals(id: number) {
		return this.auth.getBookingTotals(id).subscribe((res: BookingTotals[]) => {
			if (res) {
				this.bookingTotals = res;
			}
		});
	}

	private getNextInstallments(id: number) {
		return this.auth.findNextInstallments(id).subscribe((installments: Installments[]) => {
			if (installments.length > 0) {
				this.installments = installments;
			}
		});
	}

	private getwalletBalance(id: number) {
		return this.auth.getWalletTotal(id).then((res) => {
			const total = res;
			if (total.length > 0) {
				this.walletBalance = total[0].wallet_total;
				this.walletWarning = this.walletBalance === 0 ? true : false;
			} else {
				this.walletBalance = 0;
				this.walletWarning = true;
			}
		});
	}

	private getTotalPoints(id: number) {
		return this.auth.getTotalPoints(id).then((res) => {
			if (res.length > 0) {
				this.totalPoints = res[0].totalPoints;
			}
		});
	}

	private filterBookings() {
		this.filteredBookings = this.bookings.filter((b) => {
			return b.status === 'ongoing' || b.status === 'pending';
		});
		if (this.filteredBookings.length > 0) {
			this.transferWarning = false;
		} else {
			this.transferWarning = true;
		}
		return;
	}

	public closeTransferWarning() {
		this.transferWarning = false;
	}

	public closeWalletWarning() {
		this.walletWarning = false;
	}

	public formatId(id: string) {
		return id.substr(id.length - 15);
	}

	public getPackageTitles(packages: Package[]): string {
		let str = '';
		if (packages.length > 1) {
			packages.forEach((pkg, index) => {
				if (index === packages.length - 1) {
					str += ` and ${pkg.title}`;
					return;
				}
				str += `${pkg.title}`;
			});
			return str;
		} else {
			return packages[0].title;
		}
	}

	public uniqueHotelName(accommodations: Accommodation[], index: number) {
		const next = accommodations[index + 1].hotel;
		if (next === undefined) {
			return accommodations[index].hotel;
		} else if (accommodations[index].hotel !== accommodations[index + 1].hotel) {
			return accommodations[index].hotel;
		} else {
			return '';
		}
	}

	public getByline(pkg: Package[]) {
		let country = '';
		const places = [];
		pkg.forEach((p) => {
			const split = p.byline.split(',');
			places.push(split[0]);
			country = split[1].trim();
		});
		return this.constructByline(country, places);
	}

	private constructByline(country: string, places: string[]) {
		let str = '';
		places.forEach((item, index) => {
			if (index === places.length - 1) {
				str += `and ${item}, ${country}`;
				return;
			} else if (index === places.length - 2) {
				str += `${item} `;
				return;
			} else {
				str += `${item}, `;
			}
		});
		return str;
	}

	/* Fetch user account information */
	private getUserAccountInfo(userId: number) {
		return forkJoin(
			this.auth.fetchUserAccountInfo(userId),
			this.auth.fetchUserProfile(userId),
			this.auth.fetchUserBookings(userId),
			this.auth.fetchUserFavorites(userId),
			this.auth.fetchUserSavings(userId),
			this.auth.userTotalPoints(userId),
			this.auth.userTotalSavings(userId)
		).subscribe(
			(res) => {
				this.user = res[0].data[0];
				this.profile = res[1].data[0];
				// this.bookings = res[2].data;
				this.favGroup = this.groupPackages(res[3].data, 3);
				this.savings = res[4].data;
				// this.totalPoints = res[5].data; // [0][12]
				this.totalSavings = res[5].data.length > 0 ? res[5].data[0][0] : 0; // [0][5]
				this.secondaryEmail = !!this.profile[7];
				this.secondaryPhone = !!this.profile[8];
				this.pageReady = true;
				this.favorites = this.generateFavList();
			},
			(error) => console.log('Error: ', error)
		);
	}

	public getMonth(date: string): string {
		const index = _moment(date).month();
		return this.months[index];
	}

	public getDayOfMonth(date: string): number {
		return _moment(date).date();
	}

	public getSign(amount: number) {
		if (Math.sign(amount) === 1) {
			return '+';
		} else {
			return '-';
		}
	}

	public formatDeparture(date: string) {
		return _moment(date).format('MMM Do YYYY');
	}

	public checkDeparture(date: string, duration?: number) {
		const today = Date.now();
		const departure = new Date(date).getTime();
		const returnDate = today + duration * 24 * 60 * 60 * 1000;

		if (departure > today) {
			return [ 'Departs on', _moment(departure).format('MMM Do YYYY') ];
		} else if (today > returnDate) {
			return [
				'Departed on',
				_moment(departure).format('MMM Do YYYY'),
				'and returned',
				_moment(returnDate).format('MMM Do YYYY')
			];
		} else {
			return [ 'Already departed', 'currently active' ];
		}
	}

	public getNextInstallmentDue(id: string) {
		const booking = this.installments.filter((item) => {
			return item._id === id;
		});
		return booking[0].installment;
	}

	public getTag(str: string): string {
		if (str === 'deposit' || str === 'paid' || str === 'package-deposit') {
			return 'success';
		} else if (str === 'pending' || str === 'ongoing' || str === 'transfer') {
			return 'warning';
		} else if (str === 'cancelled' || str === 'withdrawal') {
			return 'danger';
		}
		return;
	}

	public getTagTitle(action: string) {
		if (action !== 'package-deposit') {
			return action;
		}
		return 'deposit';
	}

	private groupPackages(arr, prop) {
		return arr.reduce((result, item) => {
			if (!result[item[prop]]) {
				result[item[prop]] = [];
			}
			result[item[prop]].push(item);
			return result;
		}, {});
	}
	/* Open modal updater */
	public update(item: string, action: string, pLoad?: any) {
		const dialogRef = this.dialog.open(UpdateComponent, {
			width: '650px',
			height: '100%',
			data: {
				userId: this.userId,
				item: item,
				action: action,
				pLoad: pLoad
			}
		});

		dialogRef.afterClosed().subscribe((res) => {});
	}

	public openPayment(action: string, ref?: string, installmentId?: number, amount?: number) {
		const dialogRef = this.dialog.open(PaymentComponent, {
			width: '650px',
			height: '100%',
			data: {
				action,
				walletBalance: this.walletBalance,
				bookings: this.filteredBookings,
				installment: {
					id: installmentId,
					ref,
					amount
				}
			}
		});

		dialogRef.afterClosed().subscribe((res) => {
			if (res) {
				this.success = res['success'];
				this.successMessage = res['message'];
			}
		});
	}

	public cancelBooking(
		bid: string,
		title: string,
		totalPaid: number,
		epoints: number,
		tpoints: number,
		rpoints: number
	) {
		const points = rpoints !== undefined ? epoints + tpoints + rpoints : epoints + tpoints;
		const dialogRef = this.dialog.open(CancelComponent, {
			width: '650px',
			data: {
				bid,
				title,
				amount: totalPaid,
				points,
				wallet_balance: this.walletBalance
			}
		});

		dialogRef.afterClosed().subscribe((res) => {
			if (res) {
				this.pageReady = false;
				this.getBookings(this.userId);
				this.getBookingTotals(this.userId);
				this.getNextInstallments(this.userId);
				this.getWalletTransactions(this.userId);
				this.getwalletBalance(this.userId);
				this.getUserAccountInfo(this.retrievedUser[0]);
				this.getTotalPoints(this.userId);
			}
		});
	}

	public closeSuccessFlash() {
		this.success = false;
	}

	/* Remove favorite */
	public remove(pkg) {
		this.global.removeFav(this.userId, pkg).subscribe((res) => {
			if (res['msg'] === 'Success') {
				this.favGroup[pkg[3]] = this.favGroup[pkg[3]].filter((p) => p !== pkg);
				this.favorites = this.generateFavList();
				this.openSnackBar('Removed from favorites', 'Undo');
			}
		});
	}

	/* Generate a fav list for all the favorited pacakges */
	public generateFavList() {
		return Object.values(this.favGroup).reduce((result, item) => {
			result.push(...item);
			return result;
		}, []);
	}

	/* Open snackbar */
	public openSnackBar(message: string, action: string) {
		this.snackBar.open(message, action, {
			duration: 2000
		});
	}

	/* Change menu and url param on click */
	public changeMenu(menu) {
		this.router.navigate([ 'users/myaccount', menu ]);
	}

	public filterBy(value) {
		this.filterValue = value;
	}
}
