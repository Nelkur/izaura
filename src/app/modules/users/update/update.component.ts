import { Component, Inject, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '../../../services/auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as intlTelInput from 'intl-tel-input';

export interface DialogData {
  userId: number;
  item: string;
  action: string;
  pLoad?: any;
}

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss'],
  providers: [ AuthService ]
})
export class UpdateComponent implements OnInit, AfterViewInit {
  public userId: number;
  public phoneTypes = ['mobile', 'home', 'work'];
  public countries: Array<Object>;
  public input;
  private global;
  private inputInstance; /* Plugin instance */
  public errorMap = [ 'Invalid number', 'Invalid country code', 'Too short', 'Too long', 'Invalid number'];
  public isValid: boolean;
  public errorCode: number;
  @ViewChild('form', { static: true }) form: any;

  constructor(public dialogRef: MatDialogRef<UpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private auth: AuthService,
    private el: ElementRef,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.countries = window.intlTelInputGlobals.getCountryData();
  }

  ngAfterViewInit() {
    if (this.data.item === 'phone' && this.data.action === 'add') {
      this.input = this.el.nativeElement.querySelector('#phone');

      intlTelInput(this.input, {
        utilsScript: 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.0.1/js/utils.js',
        initialCountry: 'ke',
        autoPlaceholder: 'polite'
      });

      this.global = window.intlTelInputGlobals;
      this.inputInstance = this.global.getInstance(this.input);
    }
  }

  /* Check for phone number validity */
  public validPhone() {
    if (this.inputInstance.isValidNumber()) {
      this.isValid = true;
    } else {
      this.isValid = false;
      this.errorCode = this.inputInstance.getValidationError();
    }
  }

  /* Methods to add columns to the user profiles tables */
  public addNationalId()  {
    if (this.data.action === 'add') {
      return this.auth.addNationalId(this.data.userId, this.form.value['id']).subscribe(res => {
        if (res) {
          this.dialogRef.close(res);
        }
      });
    } else if (this.data.action === 'edit') {
      return this.auth.editNationalId(this.data.userId, this.form.value['id']).subscribe(res => {
        if (res) {
          this.dialogRef.close(res);
        }
      });
    }
  }

  public addPassport() {
    if (this.data.action === 'add') {
      return this.auth.addPassport(this.data.userId, this.form.value).subscribe(res => {
        if (res) {
          this.openSnackBar('Added passport');
          this.dialogRef.close(res);
        }
      });
    } else if (this.data.action === 'edit') {
      return this.auth.editPassport(this.data.userId, this.form.value['passport']).subscribe(res => {
        if (res) {
          this.openSnackBar('Updated passport');
          this.dialogRef.close(res);
        }
      });
    }
  }

  public addEmail() {
    return this.auth.addSecondaryEmail(this.data.userId, this.form.value['email']).subscribe(res => {
      if (res) {
        this.openSnackBar('Added secondary email');
        this.dialogRef.close(res);
      }
    });
  }

  public addPhone() {
    if (this.data.action === 'add') {
      const value  = this.form.value;
      value.dialCode = this.inputInstance.getSelectedCountryData()['dialCode'];
      return this.auth.addSecondaryPhone(this.data.userId, value).subscribe(res => {
        if (res) {
          this.openSnackBar('Added secondary phone');
          this.dialogRef.close(res);
        }
      });
    } else if (this.data.action === 'edit') {
      return this.auth.editPhone(this.data.userId, this.form.value['phone'], this.data.pLoad['column']).subscribe(res => {
        if (res) {
          this.openSnackBar('Updated phone number');
          this.dialogRef.close(res);
        }
      });
    }
  }

  /* Make edits to already existing data */
  public editName() {
    let fullName = '';
    if (this.form.value.fname && this.form.value.lname) {
      fullName  = `${this.form.value.fname.split(' ')[0].trim()} ${this.form.value.lname.split(' ').trim()}`;
    } else if (this.form.value.fname) {
      fullName = `${this.form.value.fname.split(' ')[0].trim()} ${this.data.pLoad['lname']}`;
    } else if (this.form.value.lname) {
      fullName = `${this.data.pLoad['fname']} ${this.form.value.lname.split(' ')[0].trim()}`;
    }

    return this.auth.editName(this.data.userId, fullName).subscribe(res => {
      if (res) {
        this.openSnackBar('Made the name correction');
        this.dialogRef.close(res);
      }
    });
  }

  /* Open snackbar */
  private openSnackBar(message: string, action?: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'left'
    });
  }

}
