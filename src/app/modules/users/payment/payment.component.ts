import { Component, Inject, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PaymentService } from '../../../services/payments/payment.service';
import { CookieService } from 'ngx-cookie-service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Booking, Payment, PaymentModel } from '../../../models/booking.model';
import { Transaction } from '../../../models/transaction.model';
import { IpayPayload } from '../../../models/ipay-web.model';
declare var paypal;

interface Installment {
  id: number;
  ref: string;
  amount: number;
}

export interface DialogData {
  action: string;
  walletBalance?: number;
  bookings?: Booking[];
  installment?: Installment;
}

interface User {
  id?: number;
  type: string;
  name: string;
  email: string;
  phone: string;
  passport?: string;
  passport_issue_country?: string;
}

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;
  @ViewChild('f', { static: false }) form: any;
  private user: User;
  public mpesaLimit = 70000;
  private allowedChars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  public balanceDue: number;
  public walletForm: FormGroup;
  public amount: FormControl;
  public package: FormControl;
  public paymentOption: FormControl;
  public mpesaTransacted = false;
  public processing = false;
  public flash = false;
  public flashMessage = '';
  private paypalPrice: number;
  private paypalDescription: string;
  private paypalSuccess = false;
  private ipay: IpayPayload;

  constructor(public dialogRef: MatDialogRef<PaymentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private payment: PaymentService,
    private cookieService: CookieService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    if (this.data.bookings.length > 0) {
      this.user = this.data.bookings[0].user;
    } else {
      const user = JSON.parse(this.cookieService.get('user'));
      this.user = {
        id: 0,
        type: 'registered',
        name: user[2],
        email: user[1],
        phone: user[3]
      };
    }

    this.package.valueChanges.subscribe(value => {
      this.getBalanceDue(value);
      this.amount.setValidators([
        Validators.required,
        Validators.min(1000),
        Validators.max(this.getMaxValue)
      ]);
    });

    this.paymentOption.valueChanges.subscribe(value => {
      if (this.paymentOption.value === 'paypal') {
        this.paypalPrice = this.amount.value;
        this.paypalDescription = this.getDescription;
      }
    });
    this.activatePaypal();

    if (this.data.action === 'installment') {
      this.walletForm.get('amount').setValue(this.data.installment.amount);
    }
  }

  private activatePaypal() {
    if (this.data.action === 'deposit' || this.data.action === 'installment') {
      paypal
        .Buttons({
          createOrder: (data, actions) => {
            return actions.order.create({
              purchase_units: [
                {
                  description: this.paypalDescription,
                  amount: {
                    currency_code: 'USD',
                    value: this.paypalPrice
                  }
                }
              ]
            });
          },
          onApprove: async (data, actions) => {
            const order = await actions.order.capture();
            const amount = parseFloat(order.purchase_units[0].amount.value);
            if (this.data.action === 'installment') {
              const transaction = await this.modelTransaction(amount, this.data.installment.ref, this.paymentOption.value);
              this.payment.addTransaction(transaction).subscribe(async (res) => {
                if (res) {
                  const payment = await this.modelPayment(amount, res._id, this.data.installment.id);
                  this.payment.payInstallment(payment, this.data.installment.ref, this.data.installment.id).subscribe(resp => {
                    if (resp) {
                      this.dialogRef.close({
                        success: true,
                        message: 'Installment paid successfully.'
                      });
                    }
                  });
                }
              });
              this.paypalSuccess = true;
            } else if (this.data.action === 'deposit') {
              const transaction = await this.modelTransaction(amount);
              this.payment.addTransaction(transaction).subscribe(res => {
                if (res) {
                  this.dialogRef.close({
                    success: true,
                    // tslint:disable-next-line: max-line-length
                    message: `YOU HAVE SUCCESSFULLY DEPOSITED KES ${amount} TO YOUR WALLET. NEW BALANCE IS KES ${this.data.walletBalance + this.getWalletOp(amount)}`
                  });
                }
              });
              this.paypalSuccess = true;
            }
          },
          onError: err => {
            console.log(err);
          }
        })
        .render(this.paypalElement.nativeElement);
    }
  }

  private createFormControls() {
    this.amount = new FormControl('', [
      Validators.required,
      Validators.min(1000),
      Validators.max(this.getMaxValue)
    ]);
    this.package = new FormControl('', []);
    this.paymentOption = new FormControl('stkPush', []);
  }

  private createForm() {
    this.walletForm = new FormGroup({
      amount: this.amount,
      package: this.package
    });
  }

  get disableAmountField() {
    return this.data.action === 'deposit' && this.paymentOption.value === 'paybill';
  }

  get disableWalletOption() {
    return this.amount.value > this.data.walletBalance;
  }

  get buttonText() {
    return this.data.action === 'deposit' && this.paymentOption.value === 'paybill' ? 'Next' : 'Submit';
  }

  private setFlash(message) {
    this.flashMessage = message;
    this.flash = true;
  }

  public activateMpesa() {
    if (this.amount.value > this.mpesaLimit) {
      return true;
    }
    return false;
  }

  public getBalanceDue(id: string) {
    const result = this.data.bookings.filter(item => {
      return item._id === id;
    });

    if (result.length > 0) {
      const doc = result[0];
      this.balanceDue = doc.totalCharge - doc.totalPaid;
      return;
    }
    return;
  }

  get modalHeading() {
    if (this.data.action === 'deposit') {
      return 'Deposit to wallet';
    } else if (this.data.action === 'withdraw') {
      return 'Withdraw';
    } else if (this.data.action === 'transfer') {
      return 'Pay for package using wallet';
    } else if (this.data.action === 'installment') {
      return 'Pay next installment';
    } else if (this.paymentOption.value === 'stkPush' || 'paybill' && this.mpesaTransacted === true) {
      return 'Confirm payment';
    } else {
      return 'No valid action specified *';
    }
  }

  public getTitle() {
    if (this.amount.value > 70000) {
      return 'The amount you entered exceeds the daily mpesa limit of KSh 70,000';
    }
    return '';
  }

  get getMaxValue() {
    if (this.data.action === 'transfer') {
      if (this.balanceDue !== undefined) {
        return this.data.walletBalance < this.balanceDue ? this.balanceDue : this.data.walletBalance;
      } else {
        return;
      }
    } else if (this.data.action === 'withdraw') {
      return this.data.walletBalance;
    }
    return 100000;
  }

  get getMaxError() {
    if (this.data.action === 'transfer') {
      // console.log(`Max is ${this.getMaxValue}`);
      // tslint:disable-next-line: max-line-length
      return this.data.walletBalance > this.balanceDue ? `The amount you entered exceeds your balance due which is KES ${this.balanceDue}` : `The amount you entered exceeds your wallet balance which is KES ${this.data.walletBalance}`;
    } else if (this.data.action === 'withdraw') {
      return `Your wallet balance is KES ${this.data.walletBalance}. That's the max you can withdraw.`;
    } else {
      return 'Sorry. We only allow a maximum deposit of KES 100000 per day.';
    }
  }

  get generateTransactionId() {
    let ans = '';
    for (let i = 17; i > 0; i--) {
      ans += this.allowedChars[Math.floor(Math.random() * this.allowedChars.length)];
    }
    return ans;
  }

  get getDescription() {
    if (this.data.action === 'transfer') {
      return 'Routed money towards package payment';
    } else if (this.data.action === 'deposit') {
      return 'Added money to wallet';
    } else if (this.data.action === 'withdraw') {
      return 'Withdrew money from wallet';
    } else if (this.data.action === 'installment') {
      return 'Paid installment';
    }
    return '';
  }

  get getTransactionType() {
    if (this.data.action === 'installment' && this.paymentOption.value === 'wallet') {
      return 'wallet';
    } else if (this.data.action === 'installment' && this.paymentOption.value !== 'wallet') {
      return 'direct-payment';
    } else {
      return 'wallet';
    }
  }

  get getWalletAction() {
    if (this.data.action === 'installment' && this.paymentOption.value === 'wallet') {
      return 'installment';
    } else if (this.data.action === 'installment' && this.paymentOption.value !== 'wallet') {
      return 'none';
    } else {
      return this.data.action;
    }
  }

  private getWalletOp(amount: number): number {
    if (this.data.action === 'installment' && this.paymentOption.value === 'wallet') {
      return parseFloat(`-${this.data.installment.amount}`);
    } else if (this.data.action === 'installment' && this.paymentOption.value !== 'wallet') {
      return 0;
    } else {
      const value = this.data.action === 'deposit' ? `+${amount}` : `-${amount}`;
      return parseFloat(value);
    }
  }

  private getWalletBalance(amount: number): number {
    if (this.data.action === 'installment' && this.paymentOption.value === 'wallet') {
      return this.data.walletBalance + parseFloat(`-${this.data.installment.amount}`);
    } else if (this.data.action === 'installmet' && this.paymentOption.value !== 'wallet') {
      return this.data.walletBalance;
    } else {
      return this.data.walletBalance + this.getWalletOp(amount);
    }
  }

  private async modelTransaction(amount: number, ref?: string, method: string = 'wallet'): Promise<Transaction> {
    const { type, email, id, phone } = this.user;
    const transaction: Transaction = {
      user: { id, type, email, phone },
      payment: {
        type: this.data.action,
        method,
        amount,
        currency_code: 'KES',
        wallet_action: this.getWalletAction,
        wallet_op: this.getWalletOp(amount),
        wallet_balance: this.getWalletBalance(amount)
      },
      details: {
        type: this.getTransactionType,
        description: this.getDescription,
        ref: ref ? ref : ''
      }
    };

    return transaction;
  }

  private async modelPayment(amount: number, txnId: string, installmentId?: number): Promise<PaymentModel> {
    const payment: Payment = {
      transactionId: txnId,
      method: 'wallet',
      description: this.data.installment.id ? 'installment-payment' : 'package-transfer',
      amount,
      currency: 'KES'
    };

    return { payment, installmentId };
  }

  private modelIpayPayload(amount: number, paymentOption: string, installmentRef?: string, installmentId?: number) {
    const order = this.payment.generateOrderNumber().toString();
    this.ipay = {
      live: '0',
      oid: order,
      inv: order,
      ttl: amount.toString(),
      tel: this.user.phone,
      eml: this.user.email,
      vid: 'demo',
      curr: 'KES',
      p1: paymentOption,
      p2: installmentRef ? installmentRef : '',
      p3: installmentId ? installmentId.toString() : '',
      p4: '',
      cbk: 'https://izaurasafaris.com/payment/response',
      cst: '1',
      crl: '0',
      hsh: ''
    };
    return;
  }

  /* Concatenate the data into a string for hash key generation */
  public createDataStr(data: IpayPayload) {
    return Object.entries(data).reduce((result, value) => {
      return result.concat(value[1]);
    }, '');
  }

  private async getHash(dataStr: string) {
    const res = await this.payment.getHash(dataStr);
    if (res) {
      // urlencode your callback url - https://<server-ip-address>/payment/response
      // urlencode the cbk as the last and final step before sending the data to ipay for processing
      // this.ipay.cbk = '';
      this.ipay.hsh = res['hash'];
    }
  }

  public async processForm() {
    if (this.walletForm.valid) {
      this.processing = true;
      const { amount, ref } = this.walletForm.value;

      if (this.data.action === 'transfer' || this.data.action === 'installment' && this.paymentOption.value === 'wallet') {
        const transaction = await this.modelTransaction(amount, ref);
        this.payment.addTransaction(transaction).subscribe(async (res) => {
          if (res) {
            if (this.data.action === 'transfer') {
              const payment = await this.modelPayment(amount, res._id);
              this.payment.transferFunds(payment, ref).subscribe(resp => {
                if (resp) {
                  this.dialogRef.close({
                    success: true,
                    message: 'Transfer was successful.'
                  });
                }
              });
            } else {
              const payment = await this.modelPayment(amount, res._id, this.data.installment.id);
              this.payment.payInstallment(payment, this.data.installment.ref, this.data.installment.id).subscribe(resp => {
                if (resp) {
                  this.dialogRef.close({
                    success: true,
                    message: 'Successfully paid installment.'
                  });
                }
              });
            }
          }
        });
      } else if (this.data.action === 'installment' && this.paymentOption.value !== 'wallet') {
        // Call payment method then add transaction - if method returns txnId e.g paypal, pass that to modelPayment instead
        // If said payment is successful, then we add the transaction
        if (this.paymentOption.value === 'ipay') {
          this.modelIpayPayload(amount, 'installment', this.data.installment.ref, this.data.installment.id);
          const dataStr = this.createDataStr(this.ipay);
          await this.getHash(dataStr);

          const transaction = await this.modelTransaction(amount, this.data.installment.ref, this.paymentOption.value);
          const payment = await this.modelPayment(amount, 'null', this.data.installment.id);
          localStorage.setItem('selection', JSON.stringify(transaction));
          localStorage.setItem('model', JSON.stringify(payment));

          this.processing = false;
          return this.payment.webPay(this.ipay);
        } else if (this.paymentOption.value === 'paybill') {
          this.payment.mpesaC2BRegisterUrls().subscribe(res => {
            if (res.ResponseDescription === 'success') {
              this.mpesaTransacted = true;
            } else {
              this.setFlash('Something went wrong. Please try again.');
              this.processing = false;
            }
          });
        } else if (this.paymentOption.value === 'stkPush') {
          this.payment.mpesaStkPush(amount, parseFloat(this.user.phone)).subscribe(res => {
            if (res.ResponseCode === '0') {
              this.mpesaTransacted = true;
            } else {
              this.setFlash('Something went wrong. Please try again.');
              this.processing = false;
            }
          });
        } else {
          this.setFlash('No valid payment option was selected. Please try again.');
          this.processing = false;
        }
      } else if (this.data.action === 'withdraw') {
        // Carry out the B2C transaction - if it returns a txnId, we'l see
        this.payment.mpesab2c(amount, parseFloat(this.user.phone)).subscribe(async res => {
          if (res.ResponseCode === '0') {
            const transaction = await this.modelTransaction(amount);
            this.payment.addTransaction(transaction).subscribe(resp => {
              if (resp) {
                this.dialogRef.close({
                  success: true,
                  // tslint:disable-next-line: max-line-length
                  message: `YOU HAVE SUCCESSFULLY WITHDRAWN KES ${amount} FROM YOUR WALLET. NEW BALANCE IS KES ${this.data.walletBalance + this.getWalletOp(amount)}`
                });
              }
            });
          }
        });
      } else if (this.data.action === 'deposit') {
        if (this.paymentOption.value === 'ipay') {
          this.modelIpayPayload(amount, 'deposit');
          const dataStr = this.createDataStr(this.ipay);
          await this.getHash(dataStr);

          const transaction = await this.modelTransaction(amount, this.data.installment.ref, this.paymentOption.value);
          localStorage.setItem('selection', JSON.stringify(transaction));

          this.processing = false;
          return this.payment.webPay(this.ipay);
        } else if (this.paymentOption.value === 'paybill') {
          this.payment.mpesaC2BRegisterUrls().subscribe(res => {
            if (res.ResponseDescription === 'success') {
              this.mpesaTransacted = true;
            } else {
              this.setFlash('Something went wrong. Please try again.');
              this.processing = false;
            }
          });
        } else if (this.paymentOption.value === 'stkPush') {
          this.payment.mpesaStkPush(amount, parseFloat(this.user.phone)).subscribe(res => {
            if (res.ResponseCode === '0') {
              this.mpesaTransacted = true;
            } else {
              this.setFlash('Something went wrong. Please try again.');
              this.processing = false;
            }
          });
        } else {
          this.setFlash('No valid payment option was selected. Please try again.');
          this.processing = false;
        }
      } else {
        this.openSnackBar('NO VALID ACTION SELECTED', 'OKAY');
      }
    }
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  public closeFlash() {
    this.flash = false;
  }

  public confirmPayment() {
    if (this.form.valid) {
      const transactionId = this.form.value.transactionId;
      const amount = this.walletForm.value.amount;
      if (this.paymentOption.value === 'stkPush') {
        this.payment.confirmStk(transactionId).subscribe(async res => {
          if (res.transactionId) {
            if (this.data.action === 'deposit') {
              this.executeDepositTransaction(amount);
            } else if (this.data.action === 'installment') {
              this.executeInstallmentOperation(amount);
            } else {
              this.setFlash('No valid action was selected. Please try again.');
            }
          } else {
            this.setFlash('We couldn\'t confirm your payment. Please re-enter your transaction ID and try again.');
          }
        });
      } else if (this.paymentOption.value === 'paybill') {
        this.payment.confirmPaybill(transactionId).subscribe(res => {
          if (res.transactionId) {
            if (this.data.action === 'deposit') {
              this.executeDepositTransaction(amount);
            } else if (this.data.action === 'installment') {
              this.executeInstallmentOperation(amount);
            } else {
              this.setFlash('No valid action was selected. Please try again.');
            }
          } else {
            this.setFlash('We couldn\'t confirm your payment. Please re-enter your transaction ID and try again.');
          }
        });
      }
    }
  }

  private async executeDepositTransaction(amount) {
    const transaction = await this.modelTransaction(amount);
    this.payment.addTransaction(transaction).subscribe(res => {
      if (res) {
        this.dialogRef.close({
          success: true,
          // tslint:disable-next-line: max-line-length
          message: `YOU HAVE SUCCESSFULLY DEPOSITED KES ${amount} TO YOUR WALLET. NEW BALANCE IS KES ${this.data.walletBalance + this.getWalletOp(amount)}`
        });
      }
    });
  }

  private async executeInstallmentOperation(amount) {
    const transaction = await this.modelTransaction(amount, this.data.installment.ref, this.paymentOption.value);
    this.payment.addTransaction(transaction).subscribe(async (res) => {
      if (res) {
        const payment = await this.modelPayment(amount, res._id, this.data.installment.id);
        this.payment.payInstallment(payment, this.data.installment.ref, this.data.installment.id).subscribe(resp => {
          if (resp) {
            this.dialogRef.close({
              success: true,
              message: 'Installment paid successfully.'
            });
          }
        });
      }
    });
  }
}
