import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FlexconnectService } from '../../../services/flexconnect/flexconnect.service';
import { AuthService } from '../../../services/auth/auth.service';
import { CookieService } from 'ngx-cookie-service';
import { GlobalService } from '../../../services/global/global.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [ './login.component.scss' ]
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;
	email: FormControl;
	password: FormControl;
	private adminCookie;
	public submitted = false;
	public hide = true;
	private hashedPass: string;

	constructor(
		private flex: FlexconnectService,
		private auth: AuthService,
		private router: Router,
		private cookieService: CookieService,
		private global: GlobalService,
		private snackBar: MatSnackBar
	) {}

	ngOnInit() {
		this.createFormControls();
		this.createForm();
	}

	private createFormControls() {
		this.email = new FormControl('', [ Validators.required, Validators.email ]);
		this.password = new FormControl('', [ Validators.required, Validators.minLength(8), Validators.maxLength(20) ]);
	}

	private createForm() {
		this.loginForm = new FormGroup({
			email: this.email,
			password: this.password
		});
	}

	public emailErrorHandler() {
		return this.email.hasError('required')
			? 'You must enter a value'
			: this.email.hasError('email') ? 'Not a valid email address' : '';
	}

	/* When the user login form is submitted */
	public loginSubmit() {
		if (this.loginForm.valid) {
			this.submitted = true;
			this.login(this.loginForm.value.email, this.loginForm.value.password);
		}
	}

	/* User log in functionality */
	async login(email: string, password: string) {
		await this.cookieHandler(email, password);

		return this.flex.firstConnection().subscribe((res) => {
			if (res) {
				return this.auth.login(email, this.hashedPass).then((resp) => {
					if (resp) {
						console.log('Successful login');
						return this.getUserDetails(email);
					} else {
						this.auth.setAdminCookie(this.adminCookie);
					}
				});
			}
		});
	}

	/* Get user details */
	private getUserDetails(email: string) {
		return this.auth.getUserDetails(email).then((res) => {
			if (res) {
				this.cookieService.set('user', JSON.stringify(res.data[0]), 0, '/');
				this.global.loginStatusChange.emit();
				this.openSnackBar('Successfully logged you in', 'Logout');
				this.router.navigate([ 'users/myaccount', 'home' ]);
			}
		});
	}

	/* Grab the admin cookie, store it and then delete it */
	async cookieHandler(email: string, password: string) {
		if (this.cookieService.check('user')) {
			if (this.cookieService.get('user')['email'] === email) {
				this.router.navigate([ 'users/myaccount', 'home' ]);
			} else {
				this.cookieService.delete('PHPSESSID');
				this.cookieService.delete('user');
				return;
			}
		} else {
			return this.auth.hashPassword(password).then((res) => {
				if (res.data) {
					this.hashedPass = res.data[0][0];
					this.adminCookie = this.cookieService.get('PHPSESSID');
					this.auth.storeAdmin(this.adminCookie);
					this.cookieService.delete('PHPSESSID', '/'); // The only coookie that exists at this point is PHPSESSID
				}
			});
		}
	}

	/* Open snackbar */
	private openSnackBar(message: string, action: string) {
		this.snackBar.open(message, action, {
			duration: 2000
		});
	}
}
