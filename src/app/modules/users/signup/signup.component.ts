import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth/auth.service';
import { Router } from '@angular/router';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import * as intlTelInput from 'intl-tel-input';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;
  phone: FormControl;
  nationalId: FormControl;
  passport: FormControl;
  password: FormControl;
  public registrationError = '';
  public registrationStatus: boolean;
  public submitted = false;
  public hide = true;
  private input;
  private global;
  private inputInstance;
  public isValid;
  private errorCode;
  private errorMap = ['Invalid number', 'Invalid country code', 'Too short', 'Too long', 'Invalid number'];
  public errorMessage = '';
  private selectedCountry: Object;

  constructor(private auth: AuthService, private router: Router, private el: ElementRef) { }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    this.input = this.el.nativeElement.querySelector('#phone');

    intlTelInput(this.input, {
      utilsScript: 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.0.1/js/utils.js',
      initialCountry: 'ke',
      autoPlaceholder: 'polite'
      // separateDialCode: true
    });

    this.global = window.intlTelInputGlobals;
    this.inputInstance = this.global.getInstance(this.input);
  }

  private createFormControls() {
    this.firstName = new FormControl('', [
      Validators.required,
      Validators.maxLength(15),
      Validators.minLength(2)
    ]);

    this.lastName = new FormControl('', [
      Validators.required,
      Validators.maxLength(15),
      Validators.minLength(2)
    ]);

    this.email = new FormControl('', [
      Validators.required,
      Validators.email
    ]);
    this.phone = new FormControl('', [
      Validators.required
    ]);
    this.nationalId = new FormControl();
    this.passport = new FormControl();
    this.password = new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(20)
    ]);
  }

  private createForm() {
    this.signupForm = new FormGroup({
      name: new FormGroup({
        firstName: this.firstName,
        lastName: this.lastName
      }),
      email: this.email,
      phone: this.phone,
      nationalId: this.nationalId,
      passport: this.passport,
      password: this.password
    });
  }

  public check() {
    this.isValid = this.inputInstance.isValidNumber(); // Check if phone number is valid
    this.errorCode = this.inputInstance.getValidationError();
    this.errorMessage = this.errorMap[this.errorCode];
  }

  /* Form is submitted */
  public signupSubmit() {
    if (this.signupForm.valid) {
      const value = this.signupForm.value; // Value of the form as submitted
      value.intlPhone = this.inputInstance.getNumber(); // Add international phone to value
      this.selectedCountry = this.inputInstance.getSelectedCountryData(); // Get the country data
      this.submitted = true;
      this.registrationStatus = false;
      this.registerUser(value);
    }
  }

  /* Register user */
  public registerUser(formData: object) { /* Extra var - this registrationStatus */
    return this.auth.register(formData).then(r => {
      if (r) {
        return this.auth.getUserDetails(formData['email']).then(res => {
          if (res.data) {
            // console.log(res.data); // The saved user
            return this.auth.setUserGroup(res.data[0][0]).subscribe(resp => {
              if (resp) {
                return this.auth.setUserNationality(res.data[0][0], this.selectedCountry, this.signupForm.value.phone).subscribe(f => {
                  if (f) {
                    this.registrationStatus = true;
                    this.router.navigate(['users/login']);
                  }
                });
              }
            });
          }
        });
      }
    });
  }
}
