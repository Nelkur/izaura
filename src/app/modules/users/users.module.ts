import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PipesModule } from '../../pipes.module';
import { DirectivesModule } from '../../directives.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { UsersRoutingModule } from './users-routing.module';
import { AccountComponent } from './account/account.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';

import { FlexconnectService } from '../../services/flexconnect/flexconnect.service';
import { AuthService } from '../../services/auth/auth.service';
import { PaymentService } from '../../services/payments/payment.service';
import { UpdateComponent } from './update/update.component';
import { PaymentComponent } from './payment/payment.component';
import { CancelComponent } from './cancel/cancel.component';

@NgModule({
  declarations: [AccountComponent, SignupComponent, LoginComponent, UpdateComponent, PaymentComponent, CancelComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    PipesModule,
    DirectivesModule,
    ReactiveFormsModule,
    FormsModule
  ],
  entryComponents: [UpdateComponent, PaymentComponent, CancelComponent],
  providers: [FlexconnectService, AuthService, PaymentService]
})
export class UsersModule { }
