import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { WeekendDetailsService } from '../../../services/weekends/weekend-details.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { MatDialog } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { HotelDialogComponent } from '../hotel-dialog/hotel-dialog.component';
import { SummaryDialogComponent } from '../summary-dialog/summary-dialog.component';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  providers: [WeekendDetailsService]
})
export class DetailsComponent implements OnInit {
  public url: String; /* Grab the url value for the share part */
  private eventId;
  public pageReady = false;
  public loadStatus = 'Getting ready';
  public shareLink = 'Click to copy link';
  public bundled: Boolean;
  public snackbarMessage: String;

  public event: Object;
  public eventImages: Object[];
  public itinerary: Object[];
  private itineraryActivities = [];
  public activities = [];
  public antPackages: Object[];
  public transport: Object[];
  public pickups: Object[];
  public excursions: object[];
  public hotelImages: Object;
  public gssBundles: Object;
  public standardPackage: object;
  private keysMap = {
    0: 'pkgId',
    1: 'fk_tag',
    2: 'fk_event',
    3: 'fk_hotel',
    4: 'fk_transport',
    5: 'singleRoomCap',
    6: 'doubleRoomCap',
    7: 'hotelId',
    8: 'hotelName',
    9: 'featuredImage',
    10: 'hotelOverview',
    11: 'fk_destination',
    12: 'fk_rating',
    13: 'fk_boardType',
    14: 'singleRoomPrice',
    15: 'doubleRoomPrice',
    16: 'website',
    17: 'accommodation',
    18: 'dinning',
    19: 'activities',
    20: 'transportId',
    21: 'fk_eventId',
    22: 'tMeans',
    23: 'tDescription',
    24: 'tPrice'
  };

  public galleryOptions: NgxGalleryOptions[];
  public galleryImages: NgxGalleryImage[];

  public details = false; /* View package details */
  public tacOptions = false; /* View accommodation packages */
  public checkout = false; /* Checkout */
  private myStepper: MatStepper;
  public userSelection: Object;

  constructor(private detailService: WeekendDetailsService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private ref: ChangeDetectorRef) { }

  ngOnInit() {
    this.activeRoute.paramMap.subscribe((params: ParamMap) => {
      this.eventId = params.get('eventId');
    });
    this.url = this.router.url;
    this.getDetails(this.eventId);

    this.galleryOptions = [
      {
        width: '814px',
        height: '407px',
        imageAutoPlay: true,
        imageAutoPlayPauseOnHover: true,
        previewAutoPlay: true,
        previewAutoPlayPauseOnHover: true,
        thumbnailsMoveSize: 5,
        thumbnailsColumns: 5,
        thumbnailMargin: 10,
        previewFullscreen: true,
        previewKeyboardNavigation: true,
        previewCloseOnClick: true,
        previewCloseOnEsc: true
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];
  }

  /* Grab the stepper */
  @ViewChild('stepper', { static: false }) set content(content: MatStepper) {
    this.myStepper = content;
  }

  /* User selects custom options */
  public customOptions(stepper) {
    this.details = true;
    this.nextStep(stepper);
  }

  /* Get package details */
  private async getDetails(id: any) {
    return await Promise.all([
      this.detailService.fetchEvent(id),
      this.detailService.fetchEventImages(id),
      this.detailService.fetchEventItinerary(id),
      this.detailService.fetchAntPackages(id),
      this.detailService.fetchEventTransport(id),
      this.detailService.fetchPickupLocations(id),
      this.detailService.fetchHotelImages(),
      this.detailService.fetchExcursions(id)
    ]).then(res => {
      this.event = res[0].data[0];
      this.eventImages = res[1].data;
      this.itinerary = res[2].data;
      this.itineraryActivities = this.itinerary.map(item => {
        return item[3];
      });
      this.activities = this.splitter();
      this.antPackages = res[3].data.map(pkg => {
        return this.renameKeys(this.keysMap, pkg);
      });
      this.transport = res[4].data;
      this.pickups = res[5].data;
      this.hotelImages = this.groupHotelImages(res[6].data, 1);
      this.excursions = res[7].data;

      this.galleryImages = this.eventImages.map(item => {
        return {
          small: item[2],
          medium: item[2], /*.substr(0, item[3].length - 7) + '_md.png'*/
          big: item[2], /* .substr(0, item[3].length - 7) + '_bg.png' */
          description: item[3]
        };
      });

      /* If the event has Gold, Silver and Standard antPackages then bundle them up - if (this.event[16])*/
      this.gssBundles = this.groupBy(this.antPackages, 'fk_tag');
      this.standardPackage = this.gssBundles['Standard'];

      this.bundled = !!+this.event[16];
      this.pageReady = true;
      this.ref.detectChanges();
    });
  }

  /* Function to split string based on delimeter and return the array */
  /* Rename multiple keys of an object */
  private renameKeys(keysMap, obj) {
    return Object.keys(obj).reduce((acc, key) => ({
      ...acc,
      ...{ [keysMap[key] || key]: obj[key] }
    }), {});
  }

  /* Method to group antPackages based on thier tier */
  private groupBy(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = {}; }
      result[item[prop]] = {
        hotel: item.hotelName,
        rating: item.fk_rating,
        boardType: item.fk_boardType,
        singleRoom: item.singleRoomPrice,
        doubleRoom: item.doubleRoomPrice,
        transport: item.tMeans,
        tPrice: item.tPrice,
        tDescription: item.tDescription
      };
      return result;
    }, {});
  }

  private groupHotelImages(arr, prop) {
    return arr.reduce((result, item) => {
      if (!result[item[prop]]) { result[item[prop]] = []; }
      result[item[prop]].push(
        {
          small: item[2],
          medium: item[2], /*.substr(0, item[3].length - 7) + '_md.png'*/
          big: item[2], /* .substr(0, item[3].length - 7) + '_bg.png' */
          description: item[3]
        }
      );
      return result;
    }, {});
  }

  private splitter() {
    const res = this.itineraryActivities.map(str => {
      const results = {};
      for (const result of str.split('.').map(x => x.trim()).slice(0, -1)) {
        if (result) {
          const [time, actions] = result.split(' - ');
          results[time] = actions.split(',').map(x => x.trim());
        }
      }
      return results;
    });
    return res;
  }

  private splitterTwo(arr) {
    const result = arr.map(string => {
      const firstSplit = string.split('.').map(x => x.trim()).slice(0, -1);
      return firstSplit.reduce((object, hourAndChores) => {
        const splittedHourAndChores = hourAndChores.split('-').map(x => x.trim());
        object[splittedHourAndChores[0]] = splittedHourAndChores[1].split(',');
        return object;
      }, {});
    });
    return result;
  }

  /* Open hotel summary dialog from price cards */
  public openFromBundle(tag: string) {
    const hotelId = this.antPackages.filter(hotel => hotel['fk_tag'] === tag)[0]['hotelId'];
    this.openHotelDialog(hotelId);
  }

  /* Open the dialog for the hotel */
  public openHotelDialog(hotelId) {
    const hotel = this.antPackages.filter(item => {
      return item['hotelId'] === hotelId;
    });

    const dialogRef = this.dialog.open(HotelDialogComponent, {
      width: '700px',
      height: '100%',
      data: {
        hotel: hotel[0],
        galleryOptions: this.galleryOptions,
        hotelImages: this.hotelImages[hotelId]
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.snackbarMessage = result;
    });
  }

  /* Open the dialog when a user selects a package */
  public packageSummaryDialog(bundle: Object, isBundle: Boolean): void {
    const dialogRef = this.dialog.open(SummaryDialogComponent, {
      width: '700px',
      height: '100%',
      data: {
        event: this.event,
        pkg: bundle,
        pickups: this.pickups,
        excursions: this.excursions,
        isBundle: isBundle
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.snackbarMessage = result;
        this.userSelection = {};
        this.userSelection = result;
        this.details = true;
        this.tacOptions = true;
        this.myStepper.selectedIndex = 2;
      }
    });
  }

  /* When you select a hotel */
  public hotelSummaryDialog(hotel, isBundle: Boolean) {
    const dialogRef = this.dialog.open(SummaryDialogComponent, {
      width: '700px',
      height: '100%',
      data: {
        event: this.event,
        pkg: hotel,
        pickups: this.pickups,
        excursions: this.excursions,
        isBundle: isBundle,
        transport: this.transport
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.snackbarMessage = result;
        this.userSelection = {};
        this.userSelection = result;
        this.details = true;
        this.tacOptions = true;
        this.myStepper.selectedIndex = 2;
      }
    });
  }

  /* Function that allows user to copy link in the share section */
  public copyLink(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }

  public scroll(el: HTMLElement) {
    el.scrollIntoView({ behavior: 'smooth' });
  }

  private nextStep(stepper: MatStepper) {
    stepper.next();
  }

  private previousStep(stepper: MatStepper) {
    stepper.previous();
  }

}
