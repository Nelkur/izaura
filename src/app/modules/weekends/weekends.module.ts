import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { WeekendsRoutingModule } from './weekends-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../../material.module';
import { PipesModule } from '../../pipes.module';
import { DirectivesModule } from '../../directives.module';
import { NgxGalleryModule } from 'ngx-gallery';

import { LandingComponent } from './landing/landing.component';
import { DetailsComponent } from './details/details.component';
import { HotelDialogComponent } from './hotel-dialog/hotel-dialog.component';
import { SummaryDialogComponent } from './summary-dialog/summary-dialog.component';
import { CheckoutComponent } from './checkout/checkout.component';

@NgModule({
  declarations: [LandingComponent, DetailsComponent, HotelDialogComponent, SummaryDialogComponent, CheckoutComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WeekendsRoutingModule,
    FlexLayoutModule,
    MaterialModule,
    PipesModule,
    DirectivesModule,
    NgxGalleryModule
  ],
  entryComponents: [HotelDialogComponent, SummaryDialogComponent]
})
export class WeekendsModule { }
