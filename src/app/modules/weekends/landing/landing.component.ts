import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WeekendsService } from '../../../services/weekends/weekends.service';
// import * as moment from 'moment';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  providers: [WeekendsService]
})
export class LandingComponent implements OnInit {
  public destinationFilter = 'All';
  public departFromFilter = 'All';
  public dateFilter = 'All';

  public departures: Array<Object> = [];
  public destinations: Array<Object> = [];
  public events: Array<Object> = [];
  public dates = [];
  public day = 'day';
  public date = 'date';
  public month = 'month';

  constructor(private weekendsService: WeekendsService, private router: Router) { }

  ngOnInit() {
    this.getDepartures();
    this.getDestinations();
    this.getEvents();
  }

  private getDepartures() {
    return this.weekendsService.fetchDepartureLocations().subscribe(res => {
      this.departures = res.data;
    });
  }

  public getDestinations() {
    return this.weekendsService.fetchDestinations().subscribe(res => {
      this.destinations = res.data;
    });
  }

  public getEvents() {
    return this.weekendsService.fetchEvents().subscribe(res => {
      this.events = res.data;
      // this.getDates();
    });
  }

  public eventDetails(eventId) {
    this.router.navigate(['weekends', eventId]);
  }

  // private getDates() {
  //   this.dates = this.events.map(event => {
  //     console.log(event);
  //     return moment(event['16']).format('MMM D ddd').split(' ');
  //   });
  //   console.log(this.dates);
  // }

  public filterDestinations(value) {
    this.destinationFilter = value;
  }

  public filterDepartures(value) {
    this.departFromFilter = value;
  }

  public filterDate(value) {
    this.dateFilter = value;
  }

}
