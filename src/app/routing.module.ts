import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'seasonal',
    loadChildren: () => import('./modules/seasonal/seasonal.module').then(m => m.SeasonalModule)
  },
  {
    path: 'weekends',
    loadChildren: () => import('./modules/weekends/weekends.module').then(m => m.WeekendsModule)
  },
  {
    path: 'outbound',
    loadChildren: () => import('./modules/outbound/outbound.module').then(m => m.OutboundModule)
  },
  {
    path: 'inbound',
    loadChildren: () => import('./modules/inbound/inbound.module').then(m => m.InboundModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./modules/payment/payment.module').then(m => m.PaymentModule)
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class IzauraRoutingModule { }
