// This pipe loops over an object and returns both the keys and values
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keys'
})

export class KeysPipe implements PipeTransform {
  transform(value: Object, args: string[]): any {
    const keys = [];
    for (const key in value) {
      if (value.hasOwnProperty(key)) {
        keys.push({key: key, value: value[key]});
      }
    }
    return keys;
  }
}

/* https://stackoverflow.com/questions/35534959/access-key-and-value-of-object-using-ngfor */
