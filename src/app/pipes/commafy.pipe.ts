import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'commafy'
})

export class CommafyPipe implements PipeTransform {
  transform(value: any) {
    if (typeof value === 'number' && isFinite(value)) {
      const val = value.toString();
      if (val.includes('.')) {
        const x = val.split('.');
        x[0] = x[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return x.join('.');
      }
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    return value.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
}

/* This pipe takes a number and adds commas per thousand */
