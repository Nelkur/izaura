import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'date'
})

export class DatePipe implements PipeTransform {
  transform(date: string, showTime: boolean = false, what?: string) {
    /* If it's a mysql datetime i.e 2019-05-16 00:00:00 split at the space... */
    if (date.split(' ').length > 1) {
      return showTime === false ? moment(date).format('MMMM Do YYYY') : moment(date).format('MMMM Do YYYY, h:mm:ss a');
    }

    if (what === 'day') {
      return moment(date).format('ddd');
    } else if (what === 'date') {
      return moment(date).format('D');
    } else if (what === 'month') {
      return moment(date).format('MMM');
    } else if (what === 'year') {
      return moment(date).format('YYYY');
    } else {
      return moment(date).format('MMMM Do YYYY').split(' ');
    }
  }
}
