import { Pipe, PipeTransform } from '@angular/core';

@Pipe ({
    name: 'sortby'
})

export class SortPipe implements PipeTransform {

  transform(items: any[], key: any, order: string) {
    if (items) {
      return items.sort((a, b) => {
        // If the property does not exist on either object return 0
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            return 0;
        }
        const varA = (typeof a[key] === 'string') ? a[key].toLowerCase() : a[key];
        const varB = (typeof b[key] === 'string') ? b[key].toLowerCase() : b[key];
        let comparison = 0;
        if (varA > varB) {
          comparison = 1;
        } else if (varA < varB) {
          comparison = -1;
        }
        return ( (order === 'desc') ? (comparison * -1) : comparison
        );
      });
    }
  }
}

/* This pipe will be used as follows...
While looping over an array of objects, let's call it {{ packages }}, returned from the server inside our template
If we would like to sort that array of objects based on a particular property we'd do so like this...

let package of packages | sortby:reservation_tally:asc  - For ascending order OR

let package of packages | sortby:reservation_tally:desc - For descending order */
