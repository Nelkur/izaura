import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})

export class FilterPipe implements PipeTransform {
    transform(items: any[], property: any, criteria: any) {
      if ( criteria === 'All' ) {
        return items;
      } else {
        return items.filter(item => {
          return item[property] === criteria;
        });
      }
    }
}
