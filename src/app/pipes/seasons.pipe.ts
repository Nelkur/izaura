import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'seasonfilter'
})

export class SeasonFilter implements PipeTransform {
  transform(items: any[], property: number, criteria: string) {
    let results = [];
    if (items.length > 0) {
      results = items.filter(item => {
        return item[property] === criteria;
      });
      return this.uniqueLocation(results);
    } else {
      return items;
    }
  }

  // Pass in an array of objects (seasonal_destinations)
  uniqueLocation(arr) {
    const processed = [];
    for (let i = 0; i < arr.length; i++) {
      if (processed.indexOf(arr[i][1]) < 0) {
        processed.push(arr[i]);
      } else {
        arr.splice(processed.indexOf(arr[i][1]), 1);
      }
    }
    return arr;
  }
}

/*
  transform(items: Array<object>, property: any, criteria: any) {
    if (items) {
      return items.filter(item => item[property] === criteria );
    }
  }
*/
