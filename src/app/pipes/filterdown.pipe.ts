import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterdown'
})

export class FilterDown implements PipeTransform {
  transform(items: any[], property: any, criteria: string[]) {
    if (criteria.length > 0) {
      const result = items.filter(item => {
        return criteria.indexOf(item[property]) > -1;
      });
      return result;
    } else {
      return items;
    }
  }
}
