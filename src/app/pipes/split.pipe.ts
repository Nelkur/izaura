import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'split'
})

export class SplitPipe implements PipeTransform {
  transform(str: String, itinerary?: true) {
    if (!itinerary) {
      return str.split(`.`);
    }

    let result = {};
    const splitMap = str.split('.').map(x => x.trim());
    for (result of splitMap) {
      if (result) {
        const [time, actions] = str.split(' - ');
        result[time] = actions.split(',').map(x => x.trim());
      }
    }
    return result;
  }
}
