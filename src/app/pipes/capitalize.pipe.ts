import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

  transform(value: any, words: boolean) {

    if (value) {
      if (words) {
        return value.replace(/\b\w/g, first => first.toLocaleUpperCase());
      } else {
        return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
      }
    }

    return value;
  }
}

/*In our app.component.ts let’s assume we have a our title defined as such:

this.title = 'create a capitalize string pipe for angular 2'
We would use the following methods to output the data in our template .html files.

{{ title | capitalize }}
This would result in:
Create a capitalize string pipe for angular 2

{{ title | capitalize:true }}
Resulting in:
Create A Capitalize String Pipe For Angular 2
*/
