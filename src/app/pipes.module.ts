import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CapitalizePipe } from './pipes/capitalize.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { DatePipe } from './pipes/date.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { FilterDown } from './pipes/filterdown.pipe';
import { TimesPipe } from './pipes/times.pipe';
import { CommafyPipe } from './pipes/commafy.pipe';
import { SplitPipe } from './pipes/split.pipe';
import { KeysPipe } from './pipes/keys.pipe';
import { SeasonFilter } from './pipes/seasons.pipe';

@NgModule({
  declarations: [
    CapitalizePipe,
    TruncatePipe,
    SortPipe,
    DatePipe,
    FilterPipe,
    FilterDown,
    TimesPipe,
    CommafyPipe,
    SplitPipe,
    KeysPipe,
    SeasonFilter
  ],
  imports: [CommonModule],

  exports: [
    CapitalizePipe,
    TruncatePipe,
    SortPipe,
    DatePipe,
    FilterPipe,
    FilterDown,
    TimesPipe,
    CommafyPipe,
    SplitPipe,
    KeysPipe,
    SeasonFilter
  ]
})

export class PipesModule { }
