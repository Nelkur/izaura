export interface IpayPayload {
  live: string;
  oid: string;
  inv: string;
  ttl: string;
  tel: string;
  eml: string;
  vid: string;
  curr: string;
  p1: string;
  p2: string;
  p3: string;
  p4: string;
  cbk: string;
  cst: string;
  crl: string;
  hsh: string;
}

// export interface Initialize {
//   live: number;
//   oid: string;
//   inv: string;
//   ttl: number;
//   tel: string;
//   eml: string;
//   vid: string;
//   curr: string;
//   p1: string;
//   p2: string;
//   p3: string;
//   p4: string;
//   cbk: string;
//   cst: number;
//   crl: number;
//   hsh: string;
// }
