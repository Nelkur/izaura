export interface IpayResponse {
  status: {
    status: string;
    statusCode: string;
    statusMessage: string;
  };
  oid: string;
  ivm: string;
  mc: number;
  txncd: string;
  msisdn_id: string;
  msisdn_idnum: string;
  card_mask?: string;
  msisdn_custnum?: string;
}
