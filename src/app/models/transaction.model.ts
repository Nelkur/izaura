export interface WalletTotal {
  _id: string;
  wallet_total: number;
}

export interface Transaction {
  _id?: string;
  user: {
    id: number;
    type: string;
    email: string;
    phone: string;
  };
  payment: {
    type: string;
    method: string;
    amount: number;
    currency_code?: string;
    wallet_action: string;
    wallet_op: number;
    wallet_balance: number;
  };
  details: {
    type: string;
    description: string;
    ref?: string;
  };
  status?: string;
  date?: string;
}
