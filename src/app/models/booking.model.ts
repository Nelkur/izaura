export interface User {
  id: number;
  type: string;
  name: string;
  email: string;
  phone: string;
  passport?: string;
  passport_issue_country?: string;
}

export interface Package {
  id: number;
  title: string;
  byline: string;
  image: string;
  days?: number;
  nights: number;
  link: string;
}

export interface Excursion {
  package?: string;
  name: string;
  pax: number;
  unitPrice: number;
  totalPrice: number;
}

export interface Transport {
  means?: string;
  carrier?: string;
  class?: string;
  pax: number;
  unitPrice: number;
  totalPrice: number;
}

export interface Accommodation {
  type?: string;
  hotel: string;
  room: string;
  guests: number;
  nights: number;
  price_per_night: number;
  totalPrice: number;
}

export interface Payment {
  transactionId: string;
  method: string;
  description: string;
  amount: number;
  currency: string;
  date?: Date;
}

export interface PaymentModel {
  payment: Payment;
  installmentId: number;
}

export interface Installment {
  id: number;
  amount: number;
  due_date: Date;
  due_date_readable: string;
  status: string;
  paidDate?: string;
}

export interface Installments {
  _id: string;
  installment: Installment;
}

export interface BookingTotals {
  _id: string;
  excursion_total: number;
  transport_total: number;
  accommodation_total: number;
  payment_total: number;
}

export interface Booking {
  _id?: string;
  user: User;
  packageType: string;
  packages: Package[];
  excursions: Excursion[];
  transport: Transport[];
  flight?: string;
  airport?: string;
  pickup?: {
    city_or_town: string;
    location: string;
  };
  accommodation: Accommodation[];
  payments: Payment[];
  installments: Installment[];
  comments: string;
  departure: Date;
  departureType?: string;
  redeemedPoints: number;
  earnedPoints: number;
  totalPoints: number;
  totalPaid: number;
  totalCharge: number;
  invoiceNumber: number;
  status: string;
  created?: Date;
}
