export interface StkResponse {
  MerchantRequestID: string;
  CheckoutRequestID: string;
  ResponseCode: string;
  ResultDesc: string;
  ResponseDescription: string;
  CustomerMessage: string;
}

export interface StkConfirmation {
  transactionId: string;
  amount: number;
}

export interface PaybillResponse {
  ConversationID: string;
  OriginatorCoversationID: string;
  ResponseDescription: string;
}

export interface PaybillConfirmation {
  transactionId: string;
  amount: number;
}

export interface B2CResponse {
  ConversationID: string;
  OriginatorConversationID: string;
  ResponseCode: string;
  ResponseDescription: string;
}

export interface B2CConfirmation {
  transactionId: string;
  amount: number;
}
