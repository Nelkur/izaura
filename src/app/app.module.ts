import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IzauraRoutingModule } from './routing.module';

import { AppComponent } from './app.component';

import { PipesModule } from './pipes.module';
import { NgxGalleryModule } from 'ngx-gallery';
import { FlexconnectService } from './services/flexconnect/flexconnect.service';
import { PaymentService } from './services/payments/payment.service';
import { CookieService } from 'ngx-cookie-service';
import { GlobalService } from './services/global/global.service';
// import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IzauraRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    PipesModule,
    NgxGalleryModule
  ],
  providers: [FlexconnectService, CookieService, PaymentService, GlobalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
