import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BgImageDirective } from './directives/bg-image/bg-image.directive';
import { PanelDirective } from './directives/panel/panel.directive';
import { FavToggleDirective } from './directives/favorites/fav-toggle.directive';
import { AccordionDirective } from './directives/accordion/accordion.directive';

@NgModule({
  declarations: [
    BgImageDirective,
    PanelDirective,
    FavToggleDirective,
    AccordionDirective
  ],
  imports: [CommonModule],

  exports: [
    BgImageDirective,
    PanelDirective,
    FavToggleDirective,
    AccordionDirective
  ]
})

export class DirectivesModule {}
