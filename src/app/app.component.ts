import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
import { FlexconnectService } from './services/flexconnect/flexconnect.service';
import { CookieService } from 'ngx-cookie-service';
import { GlobalService } from './services/global/global.service';
import { AuthService } from './services/auth/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ AuthService ]
})
export class AppComponent implements OnInit {
  @ViewChild('sidenav', { static: true }) sidenav: MatSidenav;
  public seasons: any[];
  public loggedIn: boolean;
  public user: object;
  public fname: string;
  public favCount: number;

  constructor(private flexconnect: FlexconnectService,
              private cookieService: CookieService,
              private router: Router,
              private global: GlobalService,
              private auth: AuthService,
              private cdr: ChangeDetectorRef,
              private snackBar: MatSnackBar) {}

  ngOnInit() {
    this.flexConnect();

    if (this.cookieService.check('user')) {
      this.user = JSON.parse(this.cookieService.get('user'));
      this.fname = this.user[2].split(' ')[0];
    }

    /* Subscribe to the favorite counter event emitter */
    this.global.favCountEvent.subscribe(value => {
      this.favCount = value;
      this.cdr.detectChanges();
    });

    /* Subscribe to the user login event emitter */
    this.global.loginStatusChange.subscribe(res => {
      this.isLoggedIn();
      if (this.loggedIn) {
        this.user = JSON.parse(this.cookieService.get('user'));
        this.fname = this.user[2].split(' ')[0];
      }
    });
  }

  /* Establish connection with backend */
  private flexConnect() {
    return this.flexconnect.firstConnection().subscribe(res => {
      if (res) {
        return this.flexconnect.secondConnection().then(resp => {
          /* Fetch seasons */
          this.getSeasons();
          this.isLoggedIn();
        });
      }
    });
  }

  /* Fetch all seasons */
  private getSeasons() {
    this.flexconnect.fetchSeasons().subscribe(res => {
      this.seasons = res.data;
    });
  }

  private isLoggedIn() {
    this.loggedIn = this.cookieService.check('user');
  }

  /* Navigate to season deals */
  public goToSeason(name) {
    this.router.navigate(['seasonal', name]);
  }

  public close() {
    this.sidenav.close();
  }

  /* Logout */
  public logout() {
    if (this.loggedIn) {
      this.cookieService.deleteAll('/');
      this.logoutAssist();
    }
  }

  private logoutAssist() {
    return this.flexconnect.firstConnection().subscribe(res => {
      if (res) {
        return this.flexconnect.secondConnection().then(resp => {
          this.global.loginStatusChange.emit();
          this.openSnackBar('Successfully logged out', 'Log in');
          this.isLoggedIn();
          if (this.router.url.substr(0, 16) === '/users/myaccount') {
            this.router.navigate(['/']);
          }
        });
      }
    });
  }

  /* Open snackbar */
  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
