import { Directive, HostListener, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appPanel]'
})
export class PanelDirective {
  public width;

  constructor(private el: ElementRef, private renderer: Renderer2) {
      this.onResize();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.width = window.innerWidth;
    this.addClass();
  }

  addClass() {
      if (this.width >= 600) {
          this.renderer.addClass(this.el.nativeElement, 'panel');
      } else {
          this.renderer.removeClass(this.el.nativeElement, 'panel');
      }
  }
}
