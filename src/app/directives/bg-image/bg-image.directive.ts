import { Directive, Input, ElementRef, Renderer2, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appBgImage]'
})
export class BgImageDirective implements AfterViewInit {

  @Input('appBgImage') path: String = '';

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngAfterViewInit() {
    this.renderer.setStyle(this.el.nativeElement, 'backgroundImage', `url(${this.path})`);
    this.renderer.setStyle(this.el.nativeElement, 'backgroundSize', 'cover');
  }

}
