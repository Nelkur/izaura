import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appAccordion]'
})
export class AccordionDirective {
  private accordions;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    this.accordions = this.el.nativeElement.querySelectorAll('.accordion-content');
  }

  @HostListener('click', ['$event.target']) toggle(ev) {
    if (ev.closest('.accordion-title')) {
      const content = ev.closest('.accordion-title').nextElementSibling;
      if (content.classList.contains('closed')) {
        this.renderer.removeClass(content, 'closed');
        this.renderer.addClass(ev.closest('.accordion-title'), 'active'); // Styling for the active accordion
      } else {
        this.renderer.addClass(content, 'closed');
        this.renderer.removeClass(ev.closest('.accordion-title'), 'active');
      }
    }
  }

  private closeAccordions() {
    for (let i = 0; i < this.accordions.length; i++) {
      this.renderer.addClass(this.accordions[i], 'closed');
    }
  }

}
