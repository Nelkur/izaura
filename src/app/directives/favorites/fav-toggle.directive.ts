import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appFavToggle]'
})
export class FavToggleDirective {
  private saved = this.el.nativeElement.classList.contains('saved');

  constructor( private el: ElementRef, private renderer: Renderer2 ) { }

  @HostListener('click', ['$event']) clickHandler() {
    if (this.el.nativeElement.classList.contains('saved')) {
      this.renderer.removeClass(this.el.nativeElement, 'saved');
    } else {
      this.renderer.addClass(this.el.nativeElement, 'saved');
    }

    // this.renderer[this.saved ? 'removeClass' : 'addClass'](this.el.nativeElement, 'saved');
  }

}
