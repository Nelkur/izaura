import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { FlexModel } from '../../models/flex.model';

interface OrbLet {
  orbLetUser: string;
  orbLetAuth: string;
  resType: string;
}

interface Orb {
  orbUser: string;
  orbAuth: string;
  resType: string;
}

const pass = '4212d2b98361d71ab84a1eb7540a519e78851c86';

@Injectable()
export class FlexconnectService {
  private flexUrl = '/API';

  private orblet: OrbLet = {
    orbLetUser: 'izauradb_izaura@xlock',
    orbLetAuth: 'pass@izaura',
    resType: 'json'
  };

  private orb: Orb = {
    orbUser: 'izauradb_izaura@xlock',
    orbAuth: pass,
    resType: 'json'
  };

  constructor(private http: HttpClient) { }

  /* OrbLet login */
  public firstConnection(): Observable<OrbLet> {
    return this.http.post<OrbLet>(this.flexUrl, this.orblet)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  /* Orb login */
  public secondConnection() {
    return this.http.post<Orb>(this.flexUrl, this.orb)
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
    .toPromise();
  }

  /* Get the seasons for the toolbar */
  public fetchSeasons() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM seasonal_holidays'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Get the deals */
  public fetchDeals() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM seasonal_destinations'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
