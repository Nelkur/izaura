import { TestBed } from '@angular/core/testing';

import { FlexconnectService } from './flexconnect.service';

describe('FlexconnectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlexconnectService = TestBed.get(FlexconnectService);
    expect(service).toBeTruthy();
  });
});
