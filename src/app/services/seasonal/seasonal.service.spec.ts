import { TestBed } from '@angular/core/testing';

import { SeasonalService } from './seasonal.service';

describe('SeasonalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SeasonalService = TestBed.get(SeasonalService);
    expect(service).toBeTruthy();
  });
});
