import { TestBed } from '@angular/core/testing';

import { SeasonaldetailsService } from './seasonaldetails.service';

describe('SeasonaldetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SeasonaldetailsService = TestBed.get(SeasonaldetailsService);
    expect(service).toBeTruthy();
  });
});
