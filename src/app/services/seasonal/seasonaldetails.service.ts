import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { FlexModel } from '../../models/flex.model';

@Injectable()
export class SeasonaldetailsService {
  private flexUrl = '/API';

  constructor(private http: HttpClient) { }

  /* Fetch package with a specific id */
  public fetchPackage(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM seasonal_packages WHERE id=${id}`
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch package Images */
  public fetchPackageImages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM seasonal_packageImages WHERE fk_package = ${id}`
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch package itinerary */
  public fetchItinerary(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM seasonal_packageItineraries WHERE fk_package = ${id}`
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch Excursions */
  public fetchExcursions(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM seasonal_packageExcursions WHERE fk_package=${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch transport packages */
  public fetchTransportPackages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM seasonal_transport WHERE fk_packageId=${id}`
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch accommodation packages */
  public fetchAntPackages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM seasonal_antPackages as packages
      INNER JOIN seasonal_hotels as hotels ON hotels.id = packages.fk_hotel
      LEFT JOIN seasonal_transport as transport ON transport.id = packages.fk_transport
      WHERE packages.fk_package =${id}`
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  public fetchHotelImages() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM seasonal_hotelImages`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public fetchGroupDepartures(id: string | number) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM seasonal_groupDepartures WHERE fk_package = ${id}`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Fetch tags */
  public fetchTags() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM tags`
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
