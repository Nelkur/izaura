import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { FlexModel } from '../../models/flex.model';

@Injectable()
export class InboundService {
  private flexUrl = '/API';

  constructor(private http: HttpClient) { }

  /* Fetch destinations */
  public fetchDestinations() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM inbound_destinations'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch types */
  public fetchTypes() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM inbound_types'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch destinationTypes */
  public fetchDestinationTypes() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM inbound_destinationTypes'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch packages */
  public fetchPackages() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM inbound_packages'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch locations */
  public fetchLocations() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM inbound_locations'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch location images */
  public fetchLocationImages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM inbound_locationImages WHERE fk_locationId=${id}`
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  public fetchCityTowns() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM inbound_cityTown'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  public fetchAccommodationPackages() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM inbound_accommodationPackages as packages
      INNER JOIN inbound_hotels as hotels ON hotels.id = packages.fk_hotel`,
      // test: 'SELECT * FROM inbound_accommodationPackages',
      resType: 'json'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  public fetchHotelImages() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM inbound_hotelImages'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  public fetchPickupTypes() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM inbound_pickupTypes'
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public fetchPickupPoints() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM inbound_pickupPoints'
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public fetchTransport() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM inbound_transport as transport INNER JOIN inbound_pickupPoints as pickups ON transport.fk_pickup = pickups.id',
      // test: 'SELECT * FROM inbound_pickupPoints',
      resType: 'json'
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public fetchExcursions(idString: string) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM inbound_packageExcursions as excursions WHERE fk_package IN (${idString})`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
