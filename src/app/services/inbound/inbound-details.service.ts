import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { FlexModel } from '../../models/flex.model';

@Injectable()
export class InboundDetailsService {
  private flexUrl = '/API';

  constructor(private http: HttpClient) { }

  /* Fetch package by id */
  public fetchPackage(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM inbound_packages WHERE id = ${id}`
    }).pipe (
      retry(2),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch package images */
  public fetchPackageImages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM inbound_packageImages WHERE fk_package = ${id} `
    }).pipe (
      retry(2),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch package itinereary */
  public fetchItinerary(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM inbound_packageItineraries WHERE fk_package = ${id}`
    }).pipe (
      retry(2),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch locations by id */
  public fetchLocation(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM inbound_locations WHERE id = ${id}`
    }).pipe (
      retry(2),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch location images */
  public fetchLocationImages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM inbound_locationImages WHERE fk_locationId = ${id}`
    }).pipe (
      retry(2),
      catchError(this.handleError)
    ).toPromise();
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
