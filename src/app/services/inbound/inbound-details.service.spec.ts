import { TestBed } from '@angular/core/testing';

import { InboundDetailsService } from './inbound-details.service';

describe('InboundDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InboundDetailsService = TestBed.get(InboundDetailsService);
    expect(service).toBeTruthy();
  });
});
