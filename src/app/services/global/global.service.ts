import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { FlexModel } from '../../models/flex.model';

interface Package {
  packageId: number;
  packageType: string;
  title: string;
  image: string;
  description: string;
  url: string;
  duration: string;
  price: number;
}

@Injectable()
export class GlobalService {
  private flexUrl = '/API';
  public favCountEvent = new EventEmitter();
  public loginStatusChange = new EventEmitter();

  constructor(private http: HttpClient) { }

  /* Dynamically fetch the packages */
  public fetchPackages(table: string) {
    let sql;
    if (table === 'weekends') {
      sql = 'SELECT * FROM weekend_events';
    } else {
      sql = `SELECT * FROM ${table}_packages`;
    }

    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: sql,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public getFavorites(userId: number) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT packageId, packageType FROM user_favorites WHERE fk_user = ${userId}`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Favorite a package - for both a logged in and a non logged in user */
  public addToFavorites(value: Array<number>) {
    localStorage.setItem('favorites', JSON.stringify(value));
  }

  public postToFavorites(userId: number, pkg: Package) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'put',
      pLoad: `INSERT INTO user_favorites (id, fk_user, packageId, packageType, title, image, description, url, duration, price)
      VALUES (NULL, '${userId}', '${pkg.packageId}', '${pkg.packageType}', '${pkg.title}', '${pkg.image}',
      '${pkg.description}', '${pkg.url}', '${pkg.duration}', '${pkg.price}')`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Delete one favorite - both for logged in and non logged in user */
  // Get the current list of favorites -- the new length is currentlength - 1
  // Remove from the list the id passed
  // Update the local storage
  public removeFavorite(userId: number, pkg) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'put',
      pLoad: `DELETE from user_favorites WHERE fk_user = '${userId}' AND packageId = '${pkg['packageId']}' AND
        packageType = '${pkg['packageType']}'`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public removeFav(userId: number, pkg) {
    console.log(`DELETE from user_favorites WHERE fk_user = '${userId}' AND packageId = '${pkg[2]}'
    AND packageType = '${pkg[3]}'`);
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'put',
      pLoad: `DELETE from user_favorites WHERE fk_user = '${userId}' AND packageId = '${pkg[2]}'
        AND packageType = '${pkg[3]}'`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Delete all favorites - both for logged in and non logged in user */
  public deleteAllFavorites() {
    localStorage.removeItem('favorites');
  }

  public deleteAllFavs(userId: number) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'put',
      test: `DELETE from user_favorites WHERE fk_user = ${userId}`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Transfer favorites from local Storage to database */

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
