import { TestBed } from '@angular/core/testing';

import { WeekendsService } from './weekends.service';

describe('WeekendsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WeekendsService = TestBed.get(WeekendsService);
    expect(service).toBeTruthy();
  });
});
