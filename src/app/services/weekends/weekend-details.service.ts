import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { FlexModel } from '../../models/flex.model';

@Injectable()
export class WeekendDetailsService {
  private flexUrl = '/API';

  constructor(private http: HttpClient) { }

  /* Fetch weekend event by id */
  public fetchEvent(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM weekend_events WHERE id=${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch weekend event images */
  public fetchEventImages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM weekend_eventImages WHERE fk_eventId=${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch event itinerary */
  public fetchEventItinerary(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM weekend_eventItineraries WHERE fk_event = ${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch event accommodation inner join hotel */
  public fetchAntPackages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM weekend_antPackages as packages
      INNER JOIN weekend_hotels as hotels ON hotels.id = packages.fk_hotel
      LEFT JOIN weekend_transport as transport ON transport.id = packages.fk_transport
      WHERE fk_event = ${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch weekend transport */
  public fetchEventTransport(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM weekend_transport WHERE fk_eventId = ${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch hotel images */
  public fetchHotelImages() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM weekend_hotelImages`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch all pickup locations */
  public fetchPickupLocations(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM weekend_pickupLocations WHERE fk_event = ${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch Excursions */
  public fetchExcursions(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM weekend_eventExcursions WHERE fk_package=${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error);
      console.log(error.error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
