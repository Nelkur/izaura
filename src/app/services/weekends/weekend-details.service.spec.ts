import { TestBed } from '@angular/core/testing';

import { WeekendDetailsService } from './weekend-details.service';

describe('WeekendDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WeekendDetailsService = TestBed.get(WeekendDetailsService);
    expect(service).toBeTruthy();
  });
});
