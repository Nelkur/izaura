import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { FlexModel } from '../../models/flex.model';

@Injectable()
export class WeekendsService {
  private flexUrl = '/API';

  constructor(private http: HttpClient) { }

  /* Fetch departure locations */
  public fetchDepartureLocations() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM weekend_departures'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch weekend destinations */
  public fetchDestinations() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM weekend_destinations'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch weekend events */
  public fetchEvents() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM weekend_events'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
