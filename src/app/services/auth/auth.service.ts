import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

import { FlexModel } from '../../models/flex.model';

import { Booking, Installments, BookingTotals } from '../../models/booking.model';
import { Transaction, WalletTotal } from '../../models/transaction.model';

interface Orb {
  orbUser: string;
  orbAuth: string;
  resType: string;
}

interface TotalPoints {
  totalPoints: number;
}

@Injectable()
export class AuthService {
  private flexUrl = '/API';
  private serviceUrl = 'https://api.izaurasafaris.com';
  private admin: string;

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  /* Login method */
  public login(email: string, password: string) {
    return this.http.post<Orb>(this.flexUrl, {
      orbUser: `izauradb_${email}`,
      orbAuth: password,
      resType: 'json'
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Check if user is logged in */
  get isLoggedIn() {
    return this.cookieService.check('user');
  }

  /* Store the admin cookie */
  public storeAdmin(sessid: string) {
    this.admin = sessid;
  }

  /* Retrieve the admin cookie */
  get adminCookie() {
    return this.admin;
  }

  /* Set admin cookie once user is logged out or if a login attempt fails */
  public setAdminCookie(cookie: string) {
    this.cookieService.set('PHPSESSID', this.adminCookie);
  }

  /* Fetch user */
  public getUserDetails(email: string) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT id, emailAddress, fullNames, phoneNo FROM users WHERE emailAddress = '${email}'`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Register and add user access */
  public register(value) {
    return this.hashPassword(value.password).then(res => {
      const fullNames = `${value.name.firstName.split(' ')[0].trim()} ${value.name.lastName.split(' ')[0].trim()}`;
      const phone = value.intlPhone ? value.intlPhone : value.phone;
      if (res.data) {
        return this.http.post<FlexModel>(this.flexUrl, {
          reqType: 'put',
          // tslint:disable-next-line:max-line-length
          pLoad: `INSERT INTO users (id, emailAddress, fullNames, phoneNo, passKey, status, dateLog) VALUES (NULL, '${value.email}', '${fullNames}', ${phone}, '${res.data[0][0]}', 'active', CURRENT_TIMESTAMP)`
        }).pipe(
          retry(3),
          catchError(this.handleError)
        ).toPromise();
      }
    });
  }

  /* Get the user's password hash value */
  public hashPassword(pass: string) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT sha('${pass}')`,
      resType: 'json'
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* User access */
  public setUserGroup(userId: number) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'put',
      pLoad: `INSERT INTO userAccess (accessGroup, userId, status)
                VALUES (2, ${userId}, 'active')`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  // Set user nationality info
  public setUserNationality(userId: number, data: Object, phone) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'put',
      pLoad: `INSERT INTO user_profiles (fk_user, nationality, iso2, phone, dialCode)
              VALUES (${userId}, '${data['name']}', '${data['iso2']}', '${phone}', '${data['dialCode']}')`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Fetch user account information */
  public fetchUserAccountInfo(userId: number) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT emailAddress, fullNames, phoneNo, dateLog FROM users WHERE id = '${userId}' AND status = 'active'`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch user profile */
  public fetchUserProfile(userId: number) {
    return this.http.post(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM user_profiles WHERE fk_user = ${userId} LIMIT 1`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Fetch user savings - to display the transaction logs */
  public fetchUserSavings(userId: number) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT totalSavings FROM user_savings WHERE fk_user = ${userId} LIMIT 1`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Fetch user bookings */
  public fetchUserBookings(userId: number) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM user_bookings WHERE fk_user = '${userId}' ORDER BY date DESC`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* NON FLEXORIGIN CALLS */

  public getBookings(userId: number): Observable<Booking[]> {
    return this.http.get<Booking[]>(`${this.serviceUrl}/bookings/users/${userId}`)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public getWalletTransactions(userId: number): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(`${this.serviceUrl}/transactions/${userId}`)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public getBookingTotals(userId: number): Observable<BookingTotals[]> {
    return this.http.get<BookingTotals[]>(`${this.serviceUrl}/bookings/totals/${userId}`)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public getWalletTotal(userId: number): Promise<WalletTotal[]> {
    return this.http.get<WalletTotal[]>(`${this.serviceUrl}/transactions/wallet/${userId}`, {})
      .pipe(
        retry(3),
        catchError(this.handleError)
      ).toPromise();
  }

  public getTotalPoints(userId: number): Promise<TotalPoints[]> {
    return this.http.post<TotalPoints[]>(`${this.serviceUrl}/bookings/points/${userId}`, {})
      .pipe(
        retry(3),
        catchError(this.handleError)
      ).toPromise();
  }

  public findNextInstallments(id: number): Observable<Installments[]> {
    return this.http.get<Installments[]>(`${this.serviceUrl}/bookings/installments/next/${id}`)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public updateInstallmentStatus(id: string, installmentId: number): Observable<Booking> {
    return this.http.put<any>(`${this.serviceUrl}/bookings/installments/next/${id}`, {
      id, installmentId
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public cancelBooking(id: string, points: number): Observable<Booking> {
    return this.http.post<any>(`${this.serviceUrl}/bookings/cancel`, {
      id,
      points
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Fetch user favorites */
  public fetchUserFavorites(userId: number) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM user_favorites WHERE fk_user = ${userId} ORDER BY date DESC`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Get user points specifically */
  public userTotalPoints(userId: number) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT totalPoints FROM user_bookings WHERE fk_user = '${userId}' ORDER BY date DESC LIMIT 1`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Get latest user savings record */
  public userTotalSavings(userId: number) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM user_savings WHERE fk_user = ${userId} ORDER BY date DESC LIMIT 1`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Add user national id */
  public addNationalId(userId: number, nationalId: any) {
    return this.http.post(this.flexUrl, {
      reqType: 'put',
      pLoad: `UPDATE user_profiles SET nationalId = '${nationalId}' WHERE fk_user = ${userId}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Add user's passport */
  public addPassport(userId: number, passport: object) {
    return this.http.post(this.flexUrl, {
      reqType: 'put',
      pLoad: `UPDATE user_profiles SET passport = '${passport['passport']}', issueCountry = '${passport['country']}'
      WHERE fk_user = ${userId}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Add user secondary email */
  public addSecondaryEmail(userId: number, email: string) {
    return this.http.post(this.flexUrl, {
      reqType: 'put',
      pLoad: `UPDATE user_profiles SET secondary_email = '${email}' WHERE fk_user = ${userId}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Add new user phone number */
  public addSecondaryPhone(userId: number, phone: object) {
    return this.http.post(this.flexUrl, {
      reqType: 'put',
      // tslint:disable-next-line: max-line-length
      pLoad: `UPDATE user_profiles SET secondary_phone = '${phone['phone']}', secondary_code = '${phone['dialCode']}', secondary_type = '${phone['type']}' WHERE fk_user = ${userId}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Post to user bookings - create interface for bookings */
  public addUserBooking(userId: number, payload: Object) {
    const departureType = payload['departureType'] ? payload['departureType'] : 'none';
    const pickup = payload['pickup'] ? payload['pickup'] : 'none';

    return this.http.post(this.flexUrl, {
      reqType: 'put',
      // tslint:disable-next-line: max-line-length
      pLoad: `INSERT INTO user_bookings (fk_user, userType, package, packageType, departure, departureType, pickup, transport, accommodation, excursions, redeemedPoints, earnedPoints, totalPoints, bookingStatus, totalCharge) VALUES ('${userId}', '${payload['userType']}', '${payload['package']}', '${payload['packageType']}', '${payload['departure']}', '${departureType}', '${pickup}', '${payload['transport']}', '${payload['accommodation']}', '${payload['excursions']}', '${payload['redeemedPoints']}', '${payload['earnedPoints']}', '${payload['totalPoints']}', '${payload['bookingStatus']}', '${payload['totalCharge']}')`,
      data: true
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Edit user's name */
  public editName(userId: number, newName: string) {
    return this.http.post(this.flexUrl, {
      reqType: 'put',
      pLoad: `UPDATE users SET fullNames = '${newName}' WHERE id = '${userId}'`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Edit user's national Id */
  public editNationalId(userId: number, nationalId: number) {
    return this.http.post(this.flexUrl, {
      reqType: 'put',
      pLoad: `UPDATE user_profiles SET nationalId = '${nationalId}' WHERE fk_user = '${userId}'`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Edit user's passport */
  public editPassport(userId: number, passport: string) {
    return this.http.post(this.flexUrl, {
      reqType: 'put',
      pLoad: `UPDATE user_profiles SET passport = '${passport}' WHERE fk_user = '${userId}'`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Edit user's email */
  public editPhone(userId: number, phone: number, column: string) {
    return this.http.post(this.flexUrl, {
      reqType: 'put',
      pLoad: `UPDATE user_profiles SET '${column}' = '${phone}' WHERE fk_user = '${userId}'`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }
  /* Add to user wish list - create interface for Favorites */
  public addUserFavorites(userId: String, payload: Object) {
    const duration = `${payload['days']} days and ${payload['nights']} nights`;
    return this.http.post(this.flexUrl, {
      reqType: 'put',
      pLoad: `INSERT INTO user_favorites (package_type, title, url, image, description, duration, price)
        VALUES ('${payload['packageType']}', '${payload['packageTitle']}', '${payload['packageUrl']}',
        '${payload['packageImage']}', '${payload['packageByline']}', '${duration}', '${payload['totalCharge']}')`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Error handler */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(`Error message is "${error.message}"`);
      console.log(error);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
