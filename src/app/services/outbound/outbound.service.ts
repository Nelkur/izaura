import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { FlexModel } from '../../models/flex.model';

@Injectable()
export class OutboundService {
  private flexUrl = '/API';

  constructor(private http: HttpClient) { }

  /* Fetch destinations */
  public fetchDestinations() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM outbound_destinations'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch locations */
  public fetchLocations() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM outbound_locations'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch location images */
  public fetchLocationImages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM outbound_locationImages WHERE fk_location=${id}`
    }).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  /* Fetch packages */
  public fetchPackages() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM outbound_packages'
    }).pipe(
      retry(2),
      catchError(this.handleError)
    )
    .toPromise();
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
