import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { FlexModel } from '../../models/flex.model';

@Injectable()
export class OutboundDetailsService {
  private flexUrl = '/API';
  private _userSelection = {}; // Will hold user selection so we can navigate back to checkout

  constructor(private http: HttpClient) { }

  set userSelection(data) {
    this._userSelection = data;
  }

  get userSelection() {
    return this._userSelection;
  }

  /* Fetch oubtound package by package id */
  public fetchPackage(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM outbound_packages WHERE id=${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch package images */
  public fetchPackageImages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM outbound_packageImages where fk_package=${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch package itinerary */
  public fetchItinerary(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM outbound_packageItineraries WHERE fk_package=${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch Excursions */
  public fetchExcursions(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM outbound_packageExcursions WHERE fk_package=${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch Includes */
  public fetchIncludes(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM outbound_packageIncludes WHERE fk_package=${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

    /* Fetch accommodation and transport packages including hotels and flights */
  public fetchAntPackages(id) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM outbound_antPackages as packages
      INNER JOIN outbound_hotels as hotels ON hotels.id = packages.fk_hotel
      INNER JOIN outbound_flights as flights ON flights.id = packages.fk_flight
      WHERE fk_package =${id}`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch hotel images */
  public fetchHotelImages() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM outbound_hotelImages`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }


  /* Fetch all hotels for a certain destination */
  public fetchHotels(destination: String) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM outbound_hotels WHERE fk_country='${destination}' ORDER BY fk_rating DESC`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  /* Fetch flights for a certain destination */
  public fetchFlights(destination: String) {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: `SELECT * FROM outbound_flights WHERE fk_destination='${destination}' ORDER BY price DESC`
    }).pipe(
      retry(3),
      catchError(this.handleError)
    ).toPromise();
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
