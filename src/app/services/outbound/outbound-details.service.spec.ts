import { TestBed } from '@angular/core/testing';

import { OutboundDetailsService } from './outbound-details.service';

describe('OutboundDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutboundDetailsService = TestBed.get(OutboundDetailsService);
    expect(service).toBeTruthy();
  });
});
