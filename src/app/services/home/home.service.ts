import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { FlexModel } from '../../models/flex.model';

@Injectable()
export class HomeService {
  private flexUrl = '/API';

  constructor(private http: HttpClient) { }

  /* Fetch all destinations where name = All */
  public fetchDestinations() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM seasonal_destinations WHERE fk_holiday="All"'
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Fetch packages */
  public fetchPackages() {
    return this.http.post<FlexModel>(this.flexUrl, {
      reqType: 'fetch',
      test: 'SELECT * FROM seasonal_packages'
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
