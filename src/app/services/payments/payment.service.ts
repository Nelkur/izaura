import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { IpayPayload } from '../../models/ipay-web.model';
import { IpayResponse } from '../../models/ipay-response.model';
import {
  StkResponse,
  StkConfirmation,
  PaybillResponse,
  PaybillConfirmation,
  B2CResponse,
  B2CConfirmation
} from '../../models/mpesa.model';
import { Transaction, WalletTotal } from '../../models/transaction.model';
import { Booking, PaymentModel } from '../../models/booking.model';

@Injectable()
export class PaymentService {
  private ipay = 'https://payments.ipayafrica.com/v3/ke';
  private serviceUrl = 'https://api.izaurasafaris.com';
  private secretKey = 'demoCHANGED';

  private _data: IpayPayload;
  private _userSelection: Booking;
  private _ipayResponse: IpayResponse;
  private orderNumberLen = 8;

  constructor(private http: HttpClient) { }

  /* Ipay Request */
  set data(d: IpayPayload) {
    this._data = d;
  }

  get data() {
    return this._data;
  }

  /* Ipay Response */
  set ipayResponse(data: IpayResponse) {
    this._ipayResponse = data;
  }

  get ipayResponse() {
    return this._ipayResponse;
  }

  /* User selection */
  set userSelection(data: Booking) {
    this._userSelection = data;
  }

  get userSelection() {
    return this._userSelection;
  }

  /* Get the hash id */
  public getHash(dataStr: string) {
    return this.http.post(`${this.serviceUrl}/ipay/hashid`, {
      data: dataStr,
      key: this.secretKey
    })
      .pipe(
        retry(3),
        catchError(this.handleError)
      ).toPromise();
  }

  /* Pay using web integration */
  public webPay(data: IpayPayload) {
    const build_query = () => {
      let query = '';
      Object.keys(data).forEach(param => {
        query += `${param}=${data[param]}&`;
      });

      return query.substring(-1, query.length - 1);
    };
    window.open(`${this.ipay}?${build_query()}`, '_blank');
  }

  /* Pay using mpesa */
  public mpesaStkPush(amount: number, phone: number): Observable<StkResponse> {
    return this.http.post<StkResponse>(`${this.serviceUrl}/mobipay/stkPush`, {
      ttl: amount,
      tel: phone
    })
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public mpesaC2BRegisterUrls(): Observable<PaybillResponse> {
    return this.http.post<any>(`${this.serviceUrl}/mobipay/paybill`, {})
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public mpesab2c(amount: number, phone: number): Observable<B2CResponse> {
    return this.http.post<any>(`${this.serviceUrl}/mpesa/withdraw`, {
      amount,
      phone
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Get payment status */
  public getPaymentStatus(url: string) {
    return this.http.get(url, { responseType: 'text' });
  }

  /* Generate unique order number */
  private _getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  public generateOrderNumber() {
    const ts = Date.now().toString();
    const parts = ts.split('').reverse();
    let id = '';

    for (let i = 0; i < this.orderNumberLen; i++) {
      const index = this._getRandomInt(0, parts.length - 1);
      id += parts[index];
    }
    return parseFloat(id);
  }

  /* POST TO BOOKINGS COLLECTION */
  public addBooking(booking: Booking): Observable<Booking> {
    return this.http.post<Booking>(`${this.serviceUrl}/bookings`, booking)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  /* POST TO TRANSACTION COLLECTION */
  public addTransaction(transaction: Transaction): Observable<Transaction> {
    return this.http.post<Transaction>(`${this.serviceUrl}/transactions`, transaction)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public getWalletBalance(userId: number): Promise<WalletTotal[]> {
    return this.http.get<WalletTotal[]>(`${this.serviceUrl}/transactions/wallet/${userId}`)
      .pipe(
        retry(3),
        catchError(this.handleError)
      ).toPromise();
  }

  public transferFunds(payload: PaymentModel, bookingId: string): Observable<Booking> {
    return this.http.put<Booking>(`${this.serviceUrl}/bookings/transfer/${bookingId}`, payload)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public payInstallment(payload: PaymentModel, bookingId: string, installmentId: number): Observable<Booking> {
    return this.http.put<Booking>(`${this.serviceUrl}/bookings/installments/${bookingId}`, {
      payment: payload,
      id: bookingId,
      installmentId
    })
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public confirmStk(transactionId: string): Observable<StkConfirmation> {
    return this.http.post<StkConfirmation>(`${this.serviceUrl}/payments/confirmStk`, {
      id: transactionId
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public confirmPaybill(transactionId: string): Observable<PaybillConfirmation> {
    return this.http.post<PaybillConfirmation>(`${this.serviceUrl}/payments/confirmPaybill`, {
      id: transactionId
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public confirmWithdrawal(transactionId: string): Observable<B2CConfirmation[]> {
    return this.http.post<B2CConfirmation[]>(`${this.serviceUrl}/payments/confirmWithdrawal`, {
      id: transactionId
    }).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  /* Error handler */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error}`);
      console.log(error);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
